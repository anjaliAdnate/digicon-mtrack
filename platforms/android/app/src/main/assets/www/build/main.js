webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__history_device_history_device__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sms__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__customers_modals_add_device_modal__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__update_device_update_device__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__live_single_device_live_single_device__ = __webpack_require__(316);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as io from 'socket.io-client';
// import * as moment from 'moment';
// import { LivePage } from '../live/live';



// import { Geocoder, GeocoderResult } from "@ionic-native/google-maps";


var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, sms, modalCtrl, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.sms = sms;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.allDevicesSearch = [];
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 5;
        this.searchQuery = '';
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.islogindealer = localStorage.getItem('isDealervalue');
        console.log("islogindealer=> " + this.islogindealer);
        if (navParams.get("label") && navParams.get("value")) {
            console.log("lebel=> ", navParams.get("label"));
            console.log("value=> ", navParams.get("value"));
            this.stausdevice = localStorage.getItem('status');
            console.log("stausdevice=> " + this.stausdevice);
        }
        else {
            // localStorage.removeItem("status");
            this.stausdevice = undefined;
        }
    }
    AddDevicesPage.prototype.ngOnInit = function () {
        this.getdevices();
        console.log(this.isDealer);
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getdevices();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        console.log("shareId=> " + data.shareId);
        console.log("d_data=>", d_data);
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var editdata = data;
            console.log(editdata);
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            // });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            that.apiCall.getdevicesApi(baseURLp)
                .subscribe(function (res) {
                that.ndata = res.devices;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.allDevices.push(that.ndata[i]);
                    // that.allDevicesSearch.push(that.ndata[i]);
                }
                that.allDevicesSearch = that.allDevices;
                console.log("all devices => " + that.allDevices);
            }, function (error) {
                console.log(error);
            });
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        // debugger;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        console.log("getdevices");
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            //this.initializeItems();
            // this.searchItems = this.allDevices.map(function (d) {
            //   return { Device_Name: d.Device_Name }
            // })
            console.log(" devices=> " + data.devices);
            console.log("data devices=> " + data.devices.length);
        }, function (err) {
            console.log("error=> ", err);
            _this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        localStorage.setItem("LiveDevice", "LiveDevice");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__live_single_device_live_single_device__["a" /* LiveSingleDevice */], {
            device: device
        });
        // this.navCtrl.push(LiveSingleDevice, {
        //   param: device
        // })
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__history_device_history_device__["a" /* HistoryDevicePage */], {
            device: device
        });
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var that = this;
        // debugger;
        // console.log("device address=> "+ JSON.stringify(device))
        that.userPermission = localStorage.getItem('condition_chk');
        that.option_switch = that.userPermission;
        console.log(that.option_switch);
        // console.log(device)
        console.log("abc " + device.last_location);
        that.allDevices[index].address = "N/A";
        if (!device.last_location) {
            // device.address = N/A
            that.allDevices[index].address = "N/A";
            console.log(that.allDevices[index]);
            console.log("inside undefined function");
        }
        else if (device.last_location) {
            console.log("aaa", device.last_location);
            console.log("inside latlong function");
            var lattitude = device.last_location.lat;
            var longitude = device.last_location.long;
            console.log(lattitude);
            console.log(longitude);
            var geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(lattitude, longitude);
            var request = {
                "latLng": latlng
            };
            geocoder.geocode(request, function (resp, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //console.log("add data=> " + JSON.stringify(resp))
                    if (resp[0] != null) {
                        console.log("address is: " + resp[0].formatted_address);
                        console.log("index=> ", index);
                        that.allDevices[index].address = resp[0].formatted_address;
                        console.log(that.allDevices[index]);
                    }
                    else {
                        console.log("No address available");
                    }
                }
                else {
                    // that.allDevices[index].address = device.last_location.lat + ' ,' + device.last_location.long
                    that.allDevices[index].address = 'N/A';
                }
            });
        }
        // console.log(JSON.stringify(this.allDevices))
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        this.messages = undefined;
        console.log("d=> " + d);
        this.dataEngine = d;
        console.log("dataEngine=> " + this.dataEngine);
        console.log("simnumber=> " + d.sim_number);
        console.log("device_type=> " + this.dataEngine.device_model.device_type);
        console.log("engine_status=> " + this.dataEngine.engine_status);
        var baseURLp = 'http://51.38.175.41/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DeviceConfigStatus = data;
            console.log("immoblizer_command=> " + _this.DeviceConfigStatus[0].immoblizer_command);
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
        if (this.dataEngine.ignitionLock == '1') {
            this.messages = 'Do you want to unlock the engine?';
        }
        else {
            if (this.dataEngine.ignitionLock == '0') {
                this.messages = 'Do you want to lock the engine?';
            }
        }
        var alert = this.alertCtrl.create({
            message: this.messages,
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        var devicedetail = {
                            "_id": _this.dataEngine._id,
                            "engine_status": !_this.dataEngine.engine_status
                        };
                        _this.apiCall.startLoading().present();
                        _this.apiCall.deviceupdateCall(devicedetail)
                            .subscribe(function (response) {
                            _this.apiCall.stopLoading();
                            _this.editdata = response;
                            console.log(_this.editdata);
                            var toast = _this.toastCtrl.create({
                                message: response.message,
                                duration: 2000,
                                position: 'top'
                            });
                            toast.present();
                            _this.responseMessage = "Edit successfully";
                            _this.getdevices();
                            console.log(_this.responseMessage);
                            var msg;
                            if (!_this.dataEngine.engine_status) {
                                msg = _this.DeviceConfigStatus[0].resume_command;
                            }
                            else {
                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                            }
                            // console.log(this.DeviceConfigStatus[0].immoblizer_command);
                            // Send a text message using default options
                            // this.sms.send('416123456', 'Hello world!');
                            _this.sms.send(d.sim_number, msg);
                            var toast1 = _this.toastCtrl.create({
                                message: 'SMS sent successfully',
                                duration: 2000,
                                position: 'bottom'
                            });
                            toast1.present();
                        }, function (error) {
                            _this.apiCall.stopLoading();
                            console.log(error);
                        });
                    }
                },
                {
                    text: 'No'
                }]
        });
        alert.present();
    };
    ;
    AddDevicesPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.allDevices);
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onCancel = function (ev) {
        // Reset the field
        this.getdevices();
        // ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__customers_modals_add_device_modal__["a" /* AddDeviceModalPage */]);
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            _this.getdevices();
        });
        profileModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-devices\add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Vehicle List</ion-title>\n    <!-- <ion-buttons end *ngIf="isDealer || islogindealer"> -->\n    <ion-buttons end *ngIf="isDealer">\n\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n  <!-- <ion-searchbar (ionInput)="getItems($event)" (ionCancel)="onCancel($event)"></ion-searchbar> -->\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <!-- <ion-list>\n    <ion-item *ngFor="let item of items">\n      {{ item }}\n    </ion-item>\n  </ion-list> -->\n\n  <ion-card *ngFor="let d of allDevicesSearch; let i=index;">\n\n    <ion-item>\n      <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null" (click)="livetrack(d)">\n        <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n      </ion-avatar>\n\n      <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null" (click)="livetrack(d)">\n        <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n      </ion-avatar>\n\n      <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null" (click)="livetrack(d)">\n        <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n        <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n        <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n        <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&( !d.last_ACC ))">\n      </ion-avatar>\n\n      <div>\n        <h2>{{d.Device_Name}}</h2>\n        <p style="color:#cf1c1c;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n          <span style="text-transform: uppercase;">{{d.status}} </span>\n          <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n        </p>\n      </div>\n\n      <button ion-button item-end clear (click)="presentPopover($event, d)" *ngIf="isDealer">\n\n        <ion-icon ios="ios-more" md="md-more"></ion-icon>\n      </button>\n\n      <button ion-button item-end clear (click)="shareVehicle(d)" *ngIf="!isDealer">\n        <ion-icon ios="ios-share" md="md-share"></ion-icon>\n      </button>\n\n      <div item-end *ngIf="d.currentFuel">\n        <ion-avatar class="ava">\n          <img src="assets/imgs/fuel.png">\n        </ion-avatar>\n        <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n      </div>\n    </ion-item>\n\n    <ion-row style="background-color: #fafafa;" padding-right>\n      <ion-col width-20 style="text-align:center" (click)="livetrack(d)">\n        <ion-icon name="pin" style="color:#ffc900;font-size: 12px;"></ion-icon>\n        <p style="color:#131212;font-size: 10px;font-weight: 400;">Live</p>\n      </ion-col>\n      <ion-col width-20 style="text-align:center" (click)="showHistoryDetail(d)">\n        <ion-icon name="clock" style="color:#d675ea;font-size: 12px;"></ion-icon>\n        <p style="color:#131212;font-size: 10px;font-weight: 400;">History</p>\n      </ion-col>\n      <ion-col width-20 style="text-align:center">\n        <ion-icon name="power" style="color:#ef473a;font-size: 12px;" *ngIf="d.last_ACC==\'0\'"></ion-icon>\n        <ion-icon name="power" style="color:#1de21d;font-size: 12px;" *ngIf="d.last_ACC==\'1\'"></ion-icon>\n        <ion-icon name="power" style="color:gray;font-size: 12px;" *ngIf="d.last_ACC==null"></ion-icon>\n        <p style="color:#131212;font-size: 10px;font-weight: 400;">Ignition</p>\n      </ion-col>\n      <ion-col width-20 style="text-align:center" (click)="IgnitionOnOff(d)">\n        <ion-icon name="switch" style="color:#ef473a;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n        <ion-icon name="switch" style="color:#1de21d;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n        <!-- <ion-icon name="switch" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==null"></ion-icon> -->\n        <p style="color:#131212;font-size: 10px;font-weight: 400;">Immobilize</p>\n      </ion-col>\n      <ion-col width-20 style="text-align:center" (click)="dialNumber(d.contact_number)">\n        <ion-icon name="call" style="color:#0000FF;font-size: 12px;"></ion-icon>\n        <p style="color:#131212;font-size: 10px;font-weight: 400;">Call Driver</p>\n      </ion-col>\n    </ion-row>\n\n    <ion-item item-start class="itemStyle" style="background:#696D74;">\n      <ion-row>\n\n        <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n\n          <div class="overme">\n            {{d.address}}\n          </div>\n        </ion-col>\n        <ion-col class="colSt1">\n          <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">Today\'s distance</p>\n          <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">{{d.today_odo | number : \'1.0-2\'}}Kms</p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-devices\add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () {
        // if (this.navParams.data) {
        //   this.contentEle = this.navParams.data.contentEle;
        //   this.textEle = this.navParams.data.textEle;
        // }
    };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        console.log("edit");
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__update_device_update_device__["a" /* UpdateDevicePage */], {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                // this.navCtrl.push(AddDevicesPage);
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var editdata = data;
            console.log(editdata);
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            // });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n  \n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n      <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n      <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';


var HistoryDevicePage = /** @class */ (function () {
    function HistoryDevicePage(events, navCtrl, navParams, alertCtrl, toastCtrl, apiCall) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.locations = [];
        this.SelectVehicle = 'Select Vehicle';
        this.allData = {};
        this.showZoom = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    HistoryDevicePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                // todo something
                // this.navController.pop();
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        // else {
        //   this.navCtrl.pop();
        // }
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        if (localStorage.getItem("MainHistory") != null) {
            console.log("coming soon");
            this.showDropDown = true;
            this.getdevices();
        }
        else {
            this.device = this.navParams.get('device');
            console.log("devices=> ", this.device);
            this.trackerId = this.device.Device_ID;
            this.trackerType = this.device.iconType;
            this.DeviceId = this.device._id;
            this.trackerName = this.device.Device_Name;
            this.btnClicked(this.datetimeStart, this.datetimeEnd);
        }
        this.hideplayback = false;
        this.target = 0;
    };
    HistoryDevicePage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        localStorage.removeItem("MainHistory");
    };
    HistoryDevicePage.prototype.changeformat = function (date) {
        console.log("date=> " + new Date(date).toISOString());
    };
    HistoryDevicePage.prototype.getdevices = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
            _this.devices1243 = [];
            _this.devices = data;
            _this.devices1243.push(data);
            localStorage.setItem('devices', _this.devices);
            _this.isdevice = localStorage.getItem('devices');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    HistoryDevicePage.prototype.onChangedSelect = function (item) {
        var that = this;
        that.trackerId = item.Device_ID;
        that.trackerType = item.iconType;
        that.DeviceId = item._id;
        that.trackerName = item.Device_Name;
        if (that.allData.map) {
            that.allData.map.clear();
            that.allData.map.remove();
        }
    };
    HistoryDevicePage.prototype.Playback = function () {
        var that = this;
        that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playing = !that.playing; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playing) {
            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                that.allData.map.addMarker({
                    icon: './assets/imgs/vehicles/runningcar.png',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](that.startPos[0], that.startPos[1]),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](that.startPos[0], that.startPos[1]));
        }
    };
    HistoryDevicePage.prototype.liveTrack = function (map, mark, coords, target, startPos, speed, delay) {
        var that = this;
        that.events.subscribe("SpeedValue:Updated", function (sdata) {
            speed = sdata;
        });
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    };
    HistoryDevicePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    HistoryDevicePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    HistoryDevicePage.prototype.inter = function (fastforwad) {
        // debugger
        var that = this;
        console.log("fastforwad=> " + fastforwad);
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
            console.log("speed fast=> " + that.speed);
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
                console.log("speed slow=> " + that.speed);
            }
            else {
                console.log("speed normal=> " + that.speed);
            }
        }
        else {
            that.speed = 200;
        }
        that.events.publish("SpeedValue:Updated", that.speed);
    };
    HistoryDevicePage.prototype.btnClicked = function (timeStart, timeEnd) {
        if (localStorage.getItem("MainHistory") != null) {
            if (this.selectedVehicle == undefined) {
                var alert_1 = this.alertCtrl.create({
                    message: "Please select the vehicle first!!",
                    buttons: ['OK']
                });
                alert_1.present();
            }
            else {
                this.maphistory(timeStart, timeEnd);
            }
        }
        else {
            this.maphistory(timeStart, timeEnd);
        }
    };
    HistoryDevicePage.prototype.maphistory = function (timeStart, timeEnd) {
        var _this = this;
        // this.online = false;
        // this.offline = true;
        // from1 = null;
        // this.latLongArr = [];
        // this.latLongArr3 = [];
        // to1 = null;
        var from1 = new Date(timeStart);
        this.fromtime = from1.toISOString();
        var to1 = new Date(timeEnd);
        this.totime = to1.toISOString();
        if (this.totime >= this.fromtime) {
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Select Correct Time',
                message: 'To time always greater than From Time',
                buttons: ['ok']
            });
            alert_2.present();
            return false;
        }
        // this.latLongArr2 = [];
        // this.Load2 = false;
        this.apiCall.startLoading().present();
        this.apiCall.getDistanceSpeedCall(this.trackerId, this.fromtime, this.totime)
            .subscribe(function (data3) {
            _this.data2 = data3;
            _this.latlongObjArr = data3;
            if (_this.data2["Average Speed"] == 'NaN') {
                _this.data2.AverageSpeed = 0;
            }
            else {
                _this.data2.AverageSpeed = _this.data2["Average Speed"];
            }
            _this.data2.IdleTime = _this.data2["Idle Time"];
            _this.hideplayback = true;
            // this.customTxt = "<html> <head><style> </style> </head><body>Total Distance - " + this.total_dis + " Km<br>Average Speed - " + this.avg_speed + " Km/hr</body> </html> "
            //////////////////////////////////
            _this.locations = [];
            _this.stoppages(timeStart, timeEnd);
            ////////////////////////////////
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log("error in getdistancespeed =>  ", error);
            var body = error._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.stoppages = function (timeStart, timeEnd) {
        var _this = this;
        var that = this;
        that.apiCall.stoppage_detail(this.islogin._id, new Date(timeStart).toISOString(), new Date(timeEnd).toISOString(), this.DeviceId)
            .subscribe(function (res) {
            var arr = [];
            for (var i = 0; i < res.length; i++) {
                arr.push({
                    lat: res[i].lat,
                    lng: res[i].long,
                    arrival_time: res[i].arrival_time,
                    departure_time: res[i].departure_time,
                    device: res[i].device,
                    address: res[i].address,
                    user: res[i].user
                });
                that.locations.push(arr);
            }
            that.callgpsFunc(that.fromtime, that.totime);
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.callgpsFunc = function (fromtime, totime) {
        var that = this;
        that.apiCall.gpsCall(this.trackerId, fromtime, totime)
            .subscribe(function (data3) {
            that.apiCall.stopLoading();
            if (data3.length > 0) {
                if (data3.length > 1) {
                    that.gps(data3.reverse());
                }
                else {
                    var alert_3 = that.alertCtrl.create({
                        message: 'No Data found for selected vehicle..',
                        buttons: [{
                                text: 'OK',
                                handler: function () {
                                    // that.datetimeStart = moment({ hours: 0 }).format();
                                    // console.log('start date', this.datetimeStart)
                                    // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                    // console.log('stop date', this.datetimeEnd);
                                    // that.selectedVehicle = undefined;
                                    that.hideplayback = false;
                                }
                            }]
                    });
                    alert_3.present();
                }
            }
            else {
                var alert_4 = that.alertCtrl.create({
                    message: 'No Data found for selected vehicle..',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                // that.datetimeStart = moment({ hours: 0 }).format();
                                // console.log('start date', this.datetimeStart)
                                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                // console.log('stop date', this.datetimeEnd);
                                // that.selectedVehicle = undefined;
                                that.hideplayback = false;
                            }
                        }]
                });
                alert_4.present();
            }
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = that.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.gps = function (data3) {
        var that = this;
        that.latlongObjArr = data3;
        that.dataArrayCoords = [];
        // for (var i = data3.length - 1; i > 0; i--) {
        //   if (data3[i].lat && data3[i].lng) {
        //     var arr = [];
        //     var startdatetime = new Date(data3[i].insertionTime);
        //     arr.push(data3[i].lat);
        //     arr.push(data3[i].lng);
        //     arr.push({ "time": startdatetime.toLocaleString() });
        //     arr.push({ "speed": data3[i].speed });
        //     that.dataArrayCoords.push(arr);
        //   }
        // }
        for (var i = 0; i < data3.length; i++) {
            if (data3[i].lat && data3[i].lng) {
                var arr = [];
                var startdatetime = new Date(data3[i].insertionTime);
                arr.push(data3[i].lat);
                arr.push(data3[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": data3[i].speed });
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data3.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        var bounds = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLngBounds */](that.mapData);
        var mapOptions = {
            gestures: {
                rotate: false,
                tilt: false
            }
        };
        that.allData.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
        that.allData.map.moveCamera({
            target: bounds
        });
        if (that.locations[0] != undefined) {
            for (var k = 0; k < that.locations[0].length; k++) {
                that.setStoppages(that.locations[0][k]);
            }
        }
        that.allData.map.addMarker({
            title: 'S',
            position: that.mapData[0],
            icon: 'red',
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then(function (marker) {
            marker.showInfoWindow();
            that.allData.map.addMarker({
                title: 'D',
                position: that.mapData[that.mapData.length - 1],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
            });
        });
        that.allData.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
    };
    HistoryDevicePage.prototype.setStoppages = function (pdata) {
        var that = this;
        ///////////////////////////////
        var htmlInfoWindow = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* HtmlInfoWindow */]();
        var frame = document.createElement('div');
        frame.innerHTML = [
            '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
            '<p style="font-size: 7px;">Arrival Time:- ' + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
            '<p style="font-size: 7px;">Departure Time:- ' + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
        ].join("");
        // frame.getElementsByTagName("img")[0].addEventListener("click", () => {
        //   htmlInfoWindow.setBackgroundColor('red');
        // });
        htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
        ///////////////////////////////////////////////////
        if (pdata != undefined)
            (function (data) {
                console.log("inside for data=> ", data);
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* LatLng */](centerMarker.lat, centerMarker.lng);
                var markerOptions = {
                    position: location,
                    icon: './assets/imgs/park.png'
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    console.log('centerMarker.ID' + centerMarker.ID);
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        console.log(e);
                        htmlInfoWindow.open(marker);
                    });
                });
            })(pdata);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], HistoryDevicePage.prototype, "navBar", void 0);
    HistoryDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history-device',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\history-device\history-device.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title *ngIf="device">{{device.Device_Name}}</ion-title>\n    <ion-title *ngIf="!device">View History</ion-title>\n    <ion-buttons end>\n      <div *ngIf="hideplayback">\n        <ion-icon color="light" name="rewind" style="font-size:19px;margin-top:11px;margin-right: 17px" (click)="inter(\'slow\')"></ion-icon>\n        <ion-icon color="light" name="arrow-dropright-circle" style="font-size:24px;margin-top:10px;margin-right: 15px" class="play"\n          *ngIf="!playing" (click)="Playback()"></ion-icon>\n        <ion-icon color="light" name="pause" style="font-size:24px;margin-top:10px;margin-right: 15px" class="pause" *ngIf="playing"\n          (click)="Playback()"></ion-icon>\n        <ion-icon color="light" name="fastforward" style="font-size:19px;margin-top:11px;margin-right: 17px" (click)="inter(\'fast\')"></ion-icon>\n\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item *ngIf="showDropDown">\n    <ion-label>{{SelectVehicle}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="onChangedSelect(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row>\n    <ion-col width-50 padding-left class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left">\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">From Date</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="changeformat(datetimeStart)"\n          style="font-size: 10px;"></ion-datetime>\n        <!-- <ion-input type="datetime-local" [(ngModel)]="datetimeStart" (ionChange)="changeformat(datetimeStart)" style="font-size: 10px;"></ion-input> -->\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left">\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">To Date</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="changeformat(datetimeEnd)"\n          style="font-size: 10px;"></ion-datetime>\n        <!-- <ion-input type="datetime-local" [(ngModel)]="datetimeEnd" (ionChange)="changeformat(datetimeStart)" style="font-size: 10px;"></ion-input> -->\n      </ion-label>\n    </ion-col>\n    <ion-col ion-text text-right padding-right>\n      <ion-icon ios="ios-search" md="md-search" style="font-size:30px;" (tap)="btnClicked(datetimeStart,datetimeEnd)"></ion-icon>\n      <!-- <button ion-button small color="light" (click)="btnClicked(datetimeStart,datetimeEnd)">Show History</button> -->\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <div id="map_canvas"></div>\n</ion-content>\n<ion-footer class="footSty">\n\n  <ion-row style="background-color: #dfdfdf; padding: 0px !important;">\n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px; text-align:center;">\n        <ion-icon name="time" style="color:#33cd5f;font-size:15px;"></ion-icon>&nbsp;\n        <span *ngIf="updatetimedate">{{updatetimedate}}&nbsp;</span>\n        <span *ngIf="!updatetimedate">0:0&nbsp;</span>\n      </p>\n    </ion-col>\n  \n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px;text-align:center;">\n        <ion-icon name="speedometer" style="color:#cd4343"></ion-icon>&nbsp;\n        <span *ngIf="speedMarker">{{speedMarker}}&nbsp;Km/hr</span>\n        <span *ngIf="!speedMarker">0&nbsp;Km/hr</span>\n      </p>\n    </ion-col>\n  </ion-row>\n\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center; border-right: 1px solid white; padding: 0px !important">\n        <p style="color: white; margin:0px; padding:0px" *ngIf="data2">{{data2.Distance}} km</p>\n        <p style="color: white; margin:0px; padding:0px" *ngIf="!data2">0 Km</p>\n        <p style="color: white; margin:0px; padding:0px">\n          Total Distance\n        </p>\n\n      </ion-col>\n  \n      <ion-col width-50 style="text-align: center; padding: 0px !important">\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="data2">{{data2.AverageSpeed}}</p>\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="!data2">0 (Km/h)</p>\n        <p style="color:#ffffff; margin:0px; padding:0px">Average Speed</p>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\history-device\history-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], HistoryDevicePage);
    return HistoryDevicePage;
}());

//# sourceMappingURL=history-device.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDeviceModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__group_modal_group_modal__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddDeviceModalPage = /** @class */ (function () {
    function AddDeviceModalPage(params, viewCtrl, formBuilder, apiCall, toastCtrl, alerCtrl, modalCtrl) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.devicedetails = {};
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> " + __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            driver: [''],
            contact_num: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]],
            sim_number: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            ExipreDate: [this.currentYear, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_type: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            name: [''],
            first_name: [''],
            brand: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
        if (params.get("custDet")) {
            this.islogin = params.get("custDet");
            console.log("custDel=> ", this.islogin);
        }
        else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + this.islogin);
        }
    }
    AddDeviceModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddDeviceModalPage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    };
    AddDeviceModalPage.prototype.getDrivers = function () {
        var _this = this;
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(function (data) {
            _this.driverList = data;
            console.log("driver list => " + _this.driverList);
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.openAddGroupModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__group_modal_group_modal__["a" /* GroupModalPage */]);
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
        });
        modal.present();
    };
    AddDeviceModalPage.prototype.adddevices = function () {
        var _this = this;
        // console.log(devicedetails);
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.groupstaus == undefined) {
                this.groupstaus_id = this.islogin._id;
            }
            else {
                this.groupstaus_id = this.groupstaus._id;
            }
            this.devicedetails = {
                "devicename": this.addvehicleForm.value.device_name,
                "deviceid": this.addvehicleForm.value.device_id,
                "driver_name": this.addvehicleForm.value.driver,
                "contact_number": this.addvehicleForm.value.contact_num,
                "typdev": "Tracker",
                "sim_number": this.addvehicleForm.value.sim_number,
                "user": this.islogin._id,
                "emailid": this.islogin.email,
                "iconType": this.TypeOf_Device,
                "vehicleGroup": this.groupstaus_id,
                "device_model": this.modeldata_id,
                "expiration_date": new Date(this.addvehicleForm.value.ExipreDate).toISOString()
            };
            if (this.vehType == undefined) {
                this.devicedetails;
            }
            else {
                this.devicedetails.vehicleType = this.vehType._id;
            }
            console.log("devicedetails=> " + this.devicedetails);
            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.devicesadd = data;
                console.log("devicesadd=> ", _this.devicesadd);
                var toast = _this.toastCtrl.create({
                    message: 'Vehicle was added successfully',
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.vehType);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.errmsg,
                    buttons: ['Try Again']
                });
                alert.present();
                console.log("err");
            });
        }
    };
    AddDeviceModalPage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = 'http://51.38.175.41/groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allGroup = data;
            _this.allGroup = data["group details"];
            // console.log("all group=> " + this.allGroup[0].name);
            for (var i = 0; i < _this.allGroup.length; i++) {
                _this.allGroupName = _this.allGroup[i].name;
                // console.log("allGroupName=> "+this.allGroupName);
            }
            console.log("allGroupName=> " + _this.allGroupName);
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    AddDeviceModalPage.prototype.getDeviceModel = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = 'http://51.38.175.41/deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(function (data) {
            _this.deviceModel = data;
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = 'http://51.38.175.41/users/getAllUsers?dealer=' + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(function (data) {
            _this.selectUser = data;
        }, function (error) {
            console.log(error);
        });
    };
    AddDeviceModalPage.prototype.getVehicleType = function () {
        var _this = this;
        console.log("get getVehicleType");
        var baseURLp = 'http://51.38.175.41/vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.allVehicle = data;
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDeviceModalPage.prototype.deviceModelata = function (deviceModel) {
        console.log("deviceModel" + deviceModel);
        if (deviceModel != undefined) {
            this.modeldata = deviceModel;
            this.modeldata_id = this.modeldata._id;
        }
        else {
            this.modeldata_id = null;
        }
        // console.log("modal data device_type=> " + this.modeldata.device_type);
    };
    AddDeviceModalPage.prototype.GroupStatusdata = function (status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    AddDeviceModalPage.prototype.userselectData = function (userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    };
    AddDeviceModalPage.prototype.vehicleTypeselectData = function (vehicletype) {
        console.log(vehicletype);
        this.vehType = vehicletype;
        console.log("vehType=> " + this.vehType._id);
    };
    AddDeviceModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\add-device-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Vehicle</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="addvehicleForm">\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Device ID(IMEI)*</ion-label>\n\n            <ion-input formControlName="device_id" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.device_id.valid && (addvehicleForm.controls.device_id.dirty || submitAttempt)">\n\n            <p>device id required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Registration Num.*</ion-label>\n\n            <ion-input formControlName="device_name" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.device_name.valid && (addvehicleForm.controls.device_name.dirty || submitAttempt)">\n\n            <p>device name required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">SIM Number*</ion-label>\n\n            <ion-input formControlName="sim_number" type="number" maxlength="10" minlength="10" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.sim_number.valid && (addvehicleForm.controls.sim_number.dirty || submitAttempt)">\n\n            <p>sim number required & should be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Driver Name</ion-label>\n\n            <ion-select formControlName="driver" style="min-width:50%;">\n\n                <ion-option *ngFor="let dr of driverList" [value]="dr.name" style="margin-left: -2px;">{{dr.name}}</ion-option>\n\n            </ion-select>\n\n            <!-- <ion-label fixed style="min-width: 50% !important;">Driver Name*</ion-label>\n\n            <ion-input formControlName="driver" type="text"></ion-input> -->\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.driver.valid && (addvehicleForm.controls.driver.dirty || submitAttempt)">\n\n            <p>driver name required!</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Driver Contact Num.</ion-label>\n\n            <ion-input formControlName="contact_num" type="number" maxlength="10" minlength="10" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.contact_num.valid && (addvehicleForm.controls.contact_num.dirty || submitAttempt)">\n\n            <p>contact number should be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Expire On*</ion-label>\n\n            <!-- <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" max="{{currentYear}}" formControlName="ExipreDate" style="margin-left:-2%;"></ion-datetime> -->\n\n            <!-- <ion-input type="date" formControlName="ExipreDate" style="margin-left: -2px;" min="2019-01-31" max="2019-01-31"></ion-input> -->\n\n            <ion-input type="date" formControlName="ExipreDate" style="margin-left: -2px;" min="{{minDate}}"></ion-input>\n\n\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.ExipreDate.valid && (addvehicleForm.controls.ExipreDate.dirty || submitAttempt)">\n\n            <p>date of expiry required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Device Model*</ion-label>\n\n            <ion-select formControlName="device_type" style="min-width:50%;">\n\n                <ion-option *ngFor="let name of deviceModel" [value]="name.device_type" (ionSelect)="deviceModelata(name)">{{name.device_type}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.device_type.valid && (addvehicleForm.controls.device_type.dirty || submitAttempt)">\n\n            <p>device model required!</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label>Group</ion-label>\n\n            <ion-select formControlName="name" style="min-width:50%;">\n\n                <ion-option *ngFor="let group of allGroup" [value]="group.name" (ionSelect)="GroupStatusdata(group)">{{group.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Select User</ion-label>\n\n            <ion-select formControlName="first_name" style="min-width:50%;">\n\n                <ion-option *ngFor="let user of selectUser" [value]="user.first_name" (ionSelect)="userselectData(user)">{{user.first_name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.first_name.valid && (addvehicleForm.controls.first_name.dirty || submitAttempt)">\n\n            <p>user required!</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label>Select Vehicle Type*</ion-label>\n\n            <ion-select formControlName="brand" style="min-width: 50%;">\n\n                <ion-option *ngFor="let veh of allVehicle" [value]="veh.brand" (ionSelect)="vehicleTypeselectData(veh)">{{veh.brand}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.brand.valid && (addvehicleForm.controls.brand.dirty || submitAttempt)">\n\n            <p>vehicle type is required required!</p>\n\n        </ion-item>\n\n\n\n\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="border-right: 1px solid white; text-align: center;">\n\n                <button ion-button clear color="light" (click)="openAddGroupModal()">ADD NEW GROUP</button>\n\n            </ion-col>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="adddevices()">ADD VEHICLE</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\add-device-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], AddDeviceModalPage);
    return AddDeviceModalPage;
}());

//# sourceMappingURL=add-device-modal.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupModalPage = /** @class */ (function () {
    function GroupModalPage(viewCtrl, formBuilder, apiCall, alerCtrl, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.alerCtrl = alerCtrl;
        this.toastCtrl = toastCtrl;
        this.GroupStatus = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        this.GroupStatus = [
            {
                name: "Active",
                value: true
            },
            {
                name: "InActive",
                value: false
            }
        ];
        this.GroupType = [
            {
                vehicle: "assets/imgs/car2.png",
                name: "car"
            },
            {
                vehicle: "assets/imgs/bike1.png",
                name: "bike"
            },
            {
                vehicle: "assets/imgs/truck2.png",
                name: "truck"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: [''],
            grouptype: [''],
            address: [''],
            emailid: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]]
        });
    }
    GroupModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    GroupModalPage.prototype.clickedGroupDiv = function (group, index) {
        this.TypeOf_Device = group.name,
            console.log(this.TypeOf_Device);
        console.log(index);
        var selected = [false, false, false];
        for (var i = 0; i < selected.length; i++)
            document.getElementById("" + i + "").className = "group daysDeselected";
        console.log(document.getElementById("" + i + "").className);
        document.getElementById(index).className = "group daysSelected";
        selected[index] = true;
        console.log(selected);
    };
    GroupModalPage.prototype.GroupStatusdata = function (status) {
        console.log("group status=> " + status);
        this.groupstaus = status;
        console.log("group status id=> " + this.groupstaus.value);
    };
    GroupModalPage.prototype.addGroup = function () {
        var that = this;
        that.submitAttempt = true;
        if (that.groupForm.valid) {
            // var devicedetails = {
            //     "name": this.groupForm.value.group_name,
            //     "status": this.groupstaus.name,
            //     "logopath": this.TypeOf_Device,
            //     "uid": this.islogin._id
            // }
            var devicedetails = {
                "name": that.groupForm.value.group_name,
                "status": that.groupstaus.value,
                "address": that.groupForm.value.address,
                "email": that.groupForm.value.email,
                "mobileNo": that.groupForm.value.mobno,
                "uid": that.islogin._id,
                "logopath": "car"
            };
            console.log(devicedetails);
            that.apiCall.startLoading().present();
            that.apiCall.addGroupCall(devicedetails)
                .subscribe(function (data) {
                that.apiCall.stopLoading();
                that.devicesadd = data;
                console.log("response from device=> " + that.devicesadd);
                var toast = that.toastCtrl.create({
                    message: 'Group was added successfully',
                    position: 'top',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    that.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                that.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = that.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    GroupModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-modal',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\group-modal\group-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Group</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="groupForm">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n            <ion-input formControlName="group_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!groupForm.controls.group_name.valid && (groupForm.controls.group_name.dirty || submitAttempt)">\n\n            <p>group name is required!</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label>Group Status</ion-label>\n\n            <ion-select formControlName="status" style="min-width:50%;">\n\n                <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Address</ion-label>\n\n            <ion-textarea formControlName="address"></ion-textarea>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Email Id</ion-label>\n\n            <ion-input type="email" formControlName="emailid"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!groupForm.controls.emailid.valid && (groupForm.controls.emailid.dirty || submitAttempt)">\n\n            <p>please enter valid email id!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n\n            <ion-input type="number" minlength="10" maxlength="10" formControlName="mobno"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!groupForm.controls.mobno.valid && (groupForm.controls.mobno.dirty || submitAttempt)">\n\n            <p>mobile number should be 10 digits!</p>\n\n        </ion-item>\n\n    </form>\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="addGroup()">ADD GROUP</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\group-modal\group-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], GroupModalPage);
    return GroupModalPage;
}());

//# sourceMappingURL=group-modal.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__geofence_show_geofence_show__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_geofence_add_geofence__ = __webpack_require__(347);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GeofencePage = /** @class */ (function () {
    function GeofencePage(navCtrl, navParams, apiCall, toastCtrl, alerCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
        // var login = this.islogin
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isdevice = localStorage.getItem('cordinates');
        console.log("isdevice=> " + this.isdevice);
        console.log("setsmsforotp=> " + this.setsmsforotp);
    }
    GeofencePage.prototype.ngOnInit = function () {
        this.getgeofence();
    };
    GeofencePage.prototype.addgeofence = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__add_geofence_add_geofence__["a" /* AddGeofencePage */]);
    };
    GeofencePage.prototype.getgeofence = function () {
        var _this = this;
        console.log("getgeofence shape");
        var baseURLp = 'http://51.38.175.41/geofencing/getallgeofence?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getallgeofenceCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            console.log("devices=> ", _this.devices);
            localStorage.setItem('devices', _this.devices);
            _this.isdevice = localStorage.getItem('devices');
            console.log("isdevices=> ", _this.isdevice);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
        });
    };
    GeofencePage.prototype.deleteGeo = function (_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        var link = 'http://13.126.36.205:3000/geofencing/deletegeofence?id=' + _id;
        this.apiCall.deleteGeoCall(link).
            subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            var toast = _this.toastCtrl.create({
                message: 'Deleted Geofence Area successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getgeofence();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alerCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GeofencePage.prototype.DelateGeofence = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this geofence area ?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteGeo(_id);
                    }
                }]
        });
        alert.present();
    };
    GeofencePage.prototype.DisplayDataOnMap = function (item) {
        var _this = this;
        console.log(item);
        var baseURLp = 'http://51.38.175.41/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
        this.apiCall.geofencestatusCall(baseURLp)
            .subscribe(function (data) {
            _this.statusofgeofence = data;
            console.log(_this.statusofgeofence);
        }, function (err) {
            console.log(err);
        });
    };
    GeofencePage.prototype.geofenceShow = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__geofence_show_geofence_show__["a" /* GeofenceShowPage */], {
            param: item
        });
    };
    GeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\geofence.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Geofences</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="addgeofence()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let item of devices">\n\n    <ion-item (click)="geofenceShow(item)">\n\n      <h2>{{item.geoname}}</h2>\n\n      <p style="margin-top: 3%;">Number Of Vehicles - {{item.devicesWithin.length}}</p>\n\n      <ion-icon item-end name="trash" color="danger" (tap)="DelateGeofence(item._id)"> </ion-icon>\n\n    </ion-item>\n\n    <!-- <ion-toggle color="danger"></ion-toggle> -->\n\n    <ion-row>\n\n      <ion-col width-33>\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n\n\n        <!-- <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left:11%;"></ion-toggle> -->\n\n        <p style="color:#e618af;font-size:10px;margin-left: 10px;">Display on map</p>\n\n      </ion-col>\n\n      <ion-col width-33 style="text-align: center;">\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n        <!-- <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n        <p style="color:green;font-size: 10px;margin-left: -8%;">Notification Entering</p>\n\n      </ion-col>\n\n      <ion-col width-33>\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n        <!-- <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n        <p style="color:green;font-size: 10px; margin-left: 11%;">Notification Exiting</p>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], GeofencePage);
    return GeofencePage;
}());

//# sourceMappingURL=geofence.js.map

/***/ }),

/***/ 144:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 144;

/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about-us/about-us.module": [
		187
	],
	"../pages/add-devices/add-devices.module": [
		188
	],
	"../pages/add-geofence/add-geofence.module": [
		329
	],
	"../pages/all-notifications/all-notifications.module": [
		330
	],
	"../pages/contact-us/contact-us.module": [
		333
	],
	"../pages/customers/customers.module": [
		334
	],
	"../pages/daily-report/daily-report.module": [
		348
	],
	"../pages/dashboard/dashboard.module": [
		349
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		350
	],
	"../pages/distance-report/distance-report.module": [
		351
	],
	"../pages/feedback/feedback.module": [
		352
	],
	"../pages/fuel-report/fuel-report.module": [
		354
	],
	"../pages/geofence-report/geofence-report.module": [
		355
	],
	"../pages/geofence/geofence.module": [
		356
	],
	"../pages/groups/groups.module": [
		357
	],
	"../pages/history-device/history-device.module": [
		359
	],
	"../pages/ignition-report/ignition-report.module": [
		361
	],
	"../pages/live/live.module": [
		362
	],
	"../pages/login/login.module": [
		364
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		368
	],
	"../pages/profile/profile.module": [
		369
	],
	"../pages/route-voilations/route-voilations.module": [
		372
	],
	"../pages/route/route.module": [
		373
	],
	"../pages/show-geofence/show-geofence.module": [
		375
	],
	"../pages/signup/signup.module": [
		376
	],
	"../pages/speed-repo/speed-repo.module": [
		377
	],
	"../pages/splash/splash.module": [
		378
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		379
	],
	"../pages/support/support.module": [
		380
	],
	"../pages/trip-report/trip-report.module": [
		381
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 186;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPageModule", function() { return AboutUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_us__ = __webpack_require__(472);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AboutUsPageModule = /** @class */ (function () {
    function AboutUsPageModule() {
    }
    AboutUsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__about_us__["a" /* AboutUsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__about_us__["a" /* AboutUsPage */]),
            ],
        })
    ], AboutUsPageModule);
    return AboutUsPageModule;
}());

//# sourceMappingURL=about-us.module.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDevicesPageModule", function() { return AddDevicesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_devices__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dummy_directive__ = __webpack_require__(505);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer/modules/ion-bottom-drawer/ion-bottom-drawer.module';
var AddDevicesPageModule = /** @class */ (function () {
    function AddDevicesPageModule() {
    }
    AddDevicesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_devices__["a" /* AddDevicesPage */],
                __WEBPACK_IMPORTED_MODULE_4__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_devices__["a" /* AddDevicesPage */])
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__dummy_directive__["a" /* OnCreate */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__["a" /* SMS */]
            ]
        })
    ], AddDevicesPageModule);
    return AddDevicesPageModule;
}());

//# sourceMappingURL=add-devices.module.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateDevicePage = /** @class */ (function () {
    function UpdateDevicePage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.devicedetail = {};
        this.vehData = navPar.get("vehData");
        console.log("vehicle data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        if (this.vehData.expiration_date == null) {
            this.eDate = this.currentYear;
        }
        else {
            this.eDate = __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format('YYYY-MM-DD');
        }
        var d_type, d_user, v_type, g_name;
        if (this.vehData.device_model != undefined) {
            d_type = this.vehData.device_model['device_type'];
        }
        else {
            d_type = '';
        }
        if (this.vehData.user != undefined) {
            d_user = this.vehData.user['first_name'];
        }
        else {
            d_user = '';
        }
        if (this.vehData.vehicleType != undefined) {
            v_type = this.vehData.vehicleType['brand'];
        }
        else {
            v_type = '';
        }
        if (this.vehData.vehicleGroup != undefined) {
            g_name = this.vehData.vehicleGroup['name'];
        }
        else {
            g_name = '';
        }
        // console.log("device model=> ", this.vehData.device_model['device_type'])
        // console.log("user => ", this.vehData.user['first_name'])
        // console.log("brand => ", this.vehData.vehicleType['brand'])
        // console.log("device model=> ", this.vehData.device_model['device_type'])
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.updatevehForm = formBuilder.group({
            device_name: [this.vehData.Device_Name, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: [this.vehData.Device_ID],
            driver: [this.vehData.driver_name],
            sim_number: [this.vehData.sim_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            contact_number: [this.vehData.contact_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]],
            SpeedLimit: [this.vehData.SpeedLimit, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            ExipreDate: [this.eDate, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            gName: [g_name],
            device_type: [d_type],
            first_name: [d_user],
            brand: [v_type]
        });
        console.log("date format=> " + __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format('YYYY-MM-DD'));
        // this.updatevehForm.patchValue({
        //     ExipreDate: moment(this.vehData.expiration_date).format('YYYY-MM-DD')
        // })
    }
    UpdateDevicePage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
    };
    UpdateDevicePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateDevicePage.prototype.updateDevices = function () {
        var that = this;
        that.submitAttempt = true;
        if (that.updatevehForm.valid) {
            ///////////////////////////
            // debugger;
            console.log("form valuess=> ", that.updatevehForm.value);
            if (that.updatevehForm.value.device_type != '') {
                if (that.vehData.device_model != null || that.vehData.device_model != undefined) {
                    if (that.updatevehForm.value.device_type == that.vehData.device_model['device_type']) {
                        that.modeldata_id = that.vehData.device_model['_id'];
                    }
                    else {
                        that.modeldata_id = that.modeldata._id;
                    }
                }
                else {
                    if (that.modeldata._id != undefined) {
                        that.modeldata_id = that.modeldata._id;
                    }
                    else {
                        that.modeldata_id = null;
                    }
                }
            }
            else {
                that.modeldata_id = null;
            }
            if (that.updatevehForm.value.gName != '') {
                if (that.vehData.vehicleGroup != null || that.vehData.vehicleGroup != undefined) {
                    if (that.updatevehForm.value.gName == that.vehData.vehicleGroup['name']) {
                        that.groupstaus_id = that.vehData.vehicleGroup['_id'];
                    }
                    else {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                }
                else {
                    if (that.groupstaus._id != undefined) {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                    else {
                        that.groupstaus_id = null;
                    }
                }
            }
            else {
                that.groupstaus_id = null;
            }
            if (that.updatevehForm.value.first_name != '') {
                if (that.vehData.user != null || that.vehData.user != undefined) {
                    if (that.updatevehForm.value.first_name == that.vehData.user['first_name']) {
                        that.userdata_id = that.vehData.user['_id'];
                    }
                    else {
                        that.userdata_id = that.userdata._id;
                    }
                }
                else {
                    if (that.userdata._id != undefined) {
                        that.userdata_id = that.userdata._id;
                    }
                    else {
                        that.userdata_id = null;
                    }
                }
            }
            else {
                that.userdata_id = null;
            }
            if (that.updatevehForm.value.brand != '') {
                if (that.vehData.vehicleType != null || that.vehData.vehicleType != undefined) {
                    if (that.updatevehForm.value.brand == that.vehData.vehicleType['brand']) {
                        that.vehicleType_id = that.vehData.vehicleType['_id'];
                    }
                    else {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                }
                else {
                    if (that.vehicleType._id != undefined) {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                    else {
                        that.vehicleType_id = null;
                    }
                }
            }
            else {
                that.vehicleType_id = null;
            }
            ///////////////////////////
            that.devicedetail = {
                "_id": that.vehData._id,
                "devicename": that.updatevehForm.value.device_name,
                "drname": that.updatevehForm.value.driver,
                "sim": that.updatevehForm.value.sim_number,
                "iconType": null,
                "dphone": that.updatevehForm.value.contact_number,
                "speed": that.updatevehForm.value.SpeedLimit,
                "vehicleGroup": that.groupstaus_id,
                "device_model": that.modeldata_id,
                "expiration_date": new Date(that.updatevehForm.value.ExipreDate).toISOString(),
                "user": that.userdata_id,
                "vehicleType": that.vehicleType_id
            };
            console.log("all details=> " + that.devicedetail);
            that.apiCall.startLoading().present();
            that.apiCall.deviceupdateInCall(that.devicedetail)
                .subscribe(function (data) {
                that.apiCall.stopLoading();
                var editdata = data;
                console.log("editdata=> " + editdata);
                var toast = that.toastCtrl.create({
                    message: editdata.message,
                    position: 'top',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    that.viewCtrl.dismiss(editdata);
                });
                toast.present();
            }, function (err) {
                that.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = that.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    UpdateDevicePage.prototype.deviceModelata = function (deviceModel) {
        console.log("deviceModel" + deviceModel);
        this.modeldata = deviceModel;
        console.log("modal data device_type=> " + this.modeldata.device_type);
    };
    UpdateDevicePage.prototype.GroupStatusdata = function (status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    UpdateDevicePage.prototype.userselectData = function (userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    };
    UpdateDevicePage.prototype.vehicleTypeselectData = function (vehicletype) {
        console.log(vehicletype);
        this.vehicleType = vehicletype;
        console.log("vehType=> " + this.vehicleType._id);
    };
    UpdateDevicePage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = 'http://51.38.175.41/groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allGroup = data;
            _this.allGroup = data["group details"];
            // console.log("all group=> " + this.allGroup[0].name);
            for (var i = 0; i < _this.allGroup.length; i++) {
                _this.allGroupName = _this.allGroup[i].name;
                // console.log("allGroupName=> "+this.allGroupName);
            }
            console.log("allGroupName=> " + _this.allGroupName);
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage.prototype.getDeviceModel = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = 'http://51.38.175.41/deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(function (data) {
            _this.deviceModel = data;
            console.log("selected user=> ", _this.deviceModel);
        }, function (err) {
            console.log(err);
        });
    };
    UpdateDevicePage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = 'http://51.38.175.41/users/getAllUsers?dealer=' + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(function (data) {
            _this.selectUser = data;
            console.log("selected user=> ", _this.selectUser);
        }, function (error) {
            console.log(error);
        });
    };
    UpdateDevicePage.prototype.getVehicleType = function () {
        var _this = this;
        console.log("get getVehicleType");
        var baseURLp = 'http://51.38.175.41/vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.allVehicle = data;
            console.log("all vehicles=> ", _this.allVehicle);
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-device',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-devices\update-device\update-device.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Update Vehicle Details</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button ion-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="updatevehForm">\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Registration Number*</ion-label>\n\n            <ion-input formControlName="device_name" type="text" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.device_name.valid && (updatevehForm.controls.device_name.dirty || submitAttempt)">\n\n            <p>registration number is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">IMEI ID*</ion-label>\n\n            <ion-input formControlName="device_id" type="text" style="margin-left: 2px;" readonly></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!updatevehForm.controls.device_id.valid && (updatevehForm.controls.device_id.dirty || submitAttempt)">\n\n            <p>imei id is required!</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Driver Name</ion-label>\n\n            <ion-input formControlName="driver" type="text" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!updatevehForm.controls.driver.valid && (updatevehForm.controls.driver.dirty || submitAttempt)">\n\n            <p>driver name is required!</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Sim Number*</ion-label>\n\n            <ion-input formControlName="sim_number" type="number" maxlength="10" minlength="10" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.sim_number.valid && (updatevehForm.controls.sim_number.dirty || submitAttempt)">\n\n            <p>sim number is required & sould be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n\n            <ion-input formControlName="contact_number" type="number" maxlength="10" minlength="10" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.contact_number.valid && (updatevehForm.controls.contact_number.dirty || submitAttempt)">\n\n            <p>mobile number sould be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Speed Limit*</ion-label>\n\n            <ion-input formControlName="SpeedLimit" type="number" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.SpeedLimit.valid && (updatevehForm.controls.SpeedLimit.dirty || submitAttempt)">\n\n            <p>speed limit is required!</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Expire On*</ion-label>\n\n            <ion-input type="date" formControlName="ExipreDate" min="{{minDate}}" style="margin-left: 2px;"></ion-input>\n\n            <!-- <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" formControlName="ExipreDate" style="margin-left: -7px;"></ion-datetime> -->\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.ExipreDate.valid && (updatevehForm.controls.ExipreDate.dirty || submitAttempt)">\n\n            <p>exipre date is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Device Model</ion-label>\n\n            <ion-select formControlName="device_type" style="min-width:49%;">\n\n                <ion-option *ngFor="let deviceModelname of deviceModel" [value]="deviceModelname.device_type" (ionSelect)="deviceModelata(deviceModelname)">{{deviceModelname.device_type}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Group</ion-label>\n\n            <ion-select formControlName="gName" style="min-width:49%;">\n\n                <ion-option *ngFor="let groupname of allGroup" [value]="groupname.name" (ionSelect)="GroupStatusdata(groupname)">{{groupname.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Select User</ion-label>\n\n            <ion-select formControlName="first_name" style="min-width:49%;">\n\n                <ion-option *ngFor="let user of selectUser" [value]="user.first_name" (ionSelect)="userselectData(user)">{{user.first_name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Vehicle Type</ion-label>\n\n            <ion-select formControlName="brand" style="min-width:49%;">\n\n                <ion-option *ngFor="let veh of allVehicle" [value]="veh.brand" (ionSelect)="vehicleTypeselectData(veh)">{{veh.brand}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </form>\n\n\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="updateDevices()">UPDATE VEHICLE DETAILS</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-devices\update-device\update-device.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], UpdateDevicePage);
    return UpdateDevicePage;
}());

//# sourceMappingURL=update-device.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { SelectSearchableComponent } from 'ionic-select-searchable';


var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, 
        // private geolocation: Geolocation,
        modalCtrl, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.drawerHidden = true;
        this.drawerHidden1 = false;
        this.shouldBounce = true;
        this.dockedHeight = 150;
        this.bounceThreshold = 500;
        this.distanceTop = 56;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon
        };
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device")) {
            console.log("device details=> ", navParams.get("device"));
            // console.log("stringify device details=> ", JSON.stringify(navParams.get("device")))
        }
    }
    LiveSingleDevice.prototype.newMap = function () {
        // let mapOptions = {
        // controls: {
        // compass: true,
        // myLocationButton: false,
        // myLocation: true,
        // indoorPicker: true,
        // zoom: true,
        // mapToolbar: true
        // },
        // mapTypeControlOptions: {
        // mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
        // }
        // }
        // let map = GoogleMaps.create('map_canvas', mapOptions);
        // map.animateCamera({
        // target: { lat: 20.5937, lng: 78.9629 },
        // zoom: 10,
        // duration: 2000,
        // padding: 0, // default = 20px
        // })
        // this.geolocation.getCurrentPosition().then((position) => {
        // var pos = {
        // lat: position.coords.latitude,
        // lng: position.coords.longitude
        // };
        // map.setCameraTarget(pos);
        // }).catch((error) => {
        // console.log('Error getting location', error);
        // });
        var mapOptions = {
            camera: { zoom: 10 }
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
        return map;
    };
    LiveSingleDevice.prototype.ngOnInit = function () {
        // let that = this;
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"]('http://51.38.175.41/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        console.log(paramData);
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () {
        var _this = this;
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        this._io.on('disconnect', function () {
            _this._io.open();
        });
    };
    LiveSingleDevice.prototype.chooseMapType = function (event) {
        var that = this;
        var actionSheet = that.actionSheetCtrl.create({
            title: 'Map Type',
            buttons: [
                {
                    text: 'Normal',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].NORMAL);
                    }
                },
                {
                    text: 'Satellite',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].SATELLITE);
                    }
                },
                {
                    text: 'Hybrid',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].HYBRID);
                    }
                },
                {
                    text: 'Terrain',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].TERRAIN);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        // console.log("email=> ", this.email);
        // console.log("time=> ", this.time);
        // console.log("tempName=> ", this.tempName)
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response=> ", data);
            _this.resToken = data.t;
            // this.sendToken(this.resToken);
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        // let profileModal = this.modalCtrl.create(LiveShareModal, { liveData: that.liveDataShare });
        // profileModal.present();
        var link = "https://51.38.175.41/share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "trackdigi- Live Trip", "", link);
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        var _this = this;
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            console.log(d4, " => d4");
            var that = _this;
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        console.log("data undefined");
                        return;
                    }
                    // console.log( that.socketSwitch);
                    if (data._id != undefined && data.last_location != undefined) {
                        // debugger;
                        var key = data._id;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            console.log(data.Device_ID + " has no iconType", data.iconType);
                            return;
                        }
                        ic_1.path = null;
                        // ic.url = 'https://www.oneqlik.in/images/' + data.status.toLowerCase() + data.iconType + '.png#' + data._id;
                        ic_1.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        console.log("url icon=> ", ic_1.url);
                        console.log(data.Device_Name, "_", data.status, "_", data.last_speed);
                        that.vehicle_speed = data.last_speed;
                        console.log("vehicle speed=> ", that.vehicle_speed);
                        that.todays_odo = data.today_odo;
                        console.log("today's odo => ", that.todays_odo);
                        that.total_odo = data.total_odo;
                        console.log("total odo=> ", that.total_odo);
                        that.fuel = data.currentFuel;
                        console.log("fuel=> ", that.fuel);
                        that.last_ping_on = data.last_ping_on;
                        if (data.lastStoppedAt != null) {
                            if (!isNaN(data.lastStoppedAt)) {
                                var fd = new Date(data.lastStoppedAt).getTime();
                                var td = new Date().getTime();
                                var time_difference = td - fd;
                                var total_min = time_difference / 60000;
                                var hours = total_min / 60;
                                var rhours = Math.floor(hours);
                                var minutes = (hours - rhours) * 60;
                                var rminutes = Math.round(minutes);
                                that.lastStoppedAt = rhours + ':' + rminutes;
                            }
                            else {
                                that.lastStoppedAt = '00' + ':' + '00';
                            }
                        }
                        else {
                            that.lastStoppedAt = '00' + ':' + '00';
                        }
                        console.log("last stopped at=> ", that.lastStoppedAt);
                        console.log("from srvice last stopped at=> ", data.lastStoppedAt);
                        that.distFromLastStop = data.distFromLastStop;
                        console.log("distance from last stop=> ", that.distFromLastStop);
                        if (!isNaN(data.timeAtLastStop)) {
                            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        }
                        else {
                            that.timeAtLastStop = '00:00:00';
                        }
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                        console.log("timeAtLastStop=> ", data.timeAtLastStop);
                        console.log("today_stopped=> ", data.today_stopped);
                        console.log("today_running=> ", data.today_running);
                        that.last_ACC = data.last_ACC;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;
                        // debugger;
                        if (that.allData[key]) {
                            console.log("if vehicle running");
                            that.socketSwitch[key] = data;
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                        }
                        else {
                            console.log("else for initializing");
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            }
                            else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    // position: { lat: data.last_location.lat, lng: data.last_location.long },
                                    icon: ic_1,
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        // alert(e.address);
                                        console.log(e);
                                        that.liveVehicleName = data.Device_Name;
                                        that.drawerHidden = false;
                                        that.onClickShow = true;
                                    });
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                });
                            }
                        }
                        var geocoder = new google.maps.Geocoder;
                        var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);
                        var request = {
                            "latLng": latlng
                        };
                        geocoder.geocode(request, function (resp, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                //console.log("add data=> " + JSON.stringify(resp))
                                if (resp[0] != null) {
                                    console.log("address is: " + resp[0].formatted_address);
                                    // console.log("index=> ", index)
                                    that.address = resp[0].formatted_address;
                                    console.log(that.address);
                                }
                                else {
                                    console.log("No address available");
                                }
                            }
                            else {
                                // that.allDevices[index].address = device.last_location.lat + ' ,' + device.last_location.long
                                that.address = 'N/A';
                            }
                        });
                        // Geocoder.geocode({
                        // "position": {
                        // lat: data.last_location['lat'],
                        // lng: data.last_location['long']
                        // }
                        // }).then((results: GeocoderResult[]) => {
                        // if (results.length == 0) {
                        // //not found
                        // return null;
                        // }
                        // console.log("address test=> ", results[0])
                        // let address: any = [
                        // results[0].subThoroughfare || "",
                        // results[0].thoroughfare || "",
                        // results[0].locality || "",
                        // results[0].adminArea || "",
                        // results[0].postalCode || "",
                        // results[0].country || ""
                        // ].join(", ");
                        // that.address = address;
                        // });
                    }
                })(d4);
        });
    };
    // liveTrack(map, mark, icons, coords, speed, delay, id) {
    LiveSingleDevice.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        console.log("center=> " + coords[0]);
        if (center) {
            map.setCameraTarget(coords[0]);
            // map.moveCamera(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                mark.setIcon(icons);
                // map.setCameraBearing(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (localStorage.getItem("LiveDevice") != null) {
                    // map.setCameraTarget({ lat: lat, lng: lng })
                    // // map.moveCamera({ lat: lat, lng: lng })
                    // }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (localStorage.getItem("LiveDevice") != null) {
                    // map.setCameraTarget(dest);
                    // // map.moveCamera(dest)
                    // }
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.drawerHidden = true;
        that.onClickShow = false;
        // if (that.allData.map != undefined) {
        // console.log("map is not undefined..!")
        // // that.allData.map.clear();
        // that.allData.map.remove();
        // }
        console.log("on select change data=> " + JSON.stringify(data));
        // for (var i = 0; i < that.socketChnl.length; i++)
        // that._io.removeAllListeners(that.socketChnl[i]);
        // that.allData = {};
        // that.socketChnl = [];
        // that.socketSwitch = {};
        if (data) {
            if (data.last_location) {
                console.log("if last location");
                // that.allData.map = GoogleMaps.create('map_canvas', {
                // camera: {
                // target: {
                // lat: data.last_location['lat'],
                // lng: data.last_location['long']
                // },
                // zoom: 15,
                // tilt: 30
                // }
                // });
                var mapOptions = {
                    backgroundColor: 'white',
                    controls: {
                        compass: true,
                        myLocationButton: false,
                        myLocation: true,
                        indoorPicker: true,
                        zoom: true,
                        mapToolbar: true
                    }
                };
                var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                map.animateCamera({
                    target: { lat: 20.5937, lng: 78.9629 },
                    zoom: 15,
                    // zoom: 17,
                    // tilt: 10,
                    // bearing: 140,
                    duration: 1000,
                    padding: 0 // default = 20px
                });
                map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                that.allData.map = map;
                // that.allData.map.setTrafficEnabled(true);
                that.socketInit(data);
            }
            else {
                console.log("else last location");
                that.allData.map = that.newMap();
                // that.allData.map.setTrafficEnabled(true);
                that.socketInit(data);
            }
        }
        // if (localStorage.getItem("LiveDevice") == null) {
        // that.showBtn = true;
        // that.SelectVehicle = "Selected Vehicle";
        // }
    };
    LiveSingleDevice.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LiveSingleDevice.prototype.show = function (data, key) {
        if (data != undefined) {
            if (key == 'last_ping_on') {
                return __WEBPACK_IMPORTED_MODULE_5_moment__(data[key]).format('DD/MM/YYYY, h:mm:ss a');
            }
            else {
                return data[key];
            }
        }
    };
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\live-single-device\live-single-device.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Live For {{titleText}}\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div id="map_canvas_single_device">\n\n    <ion-buttons end>\n\n      <button ion-button icon-only color="light" (click)="chooseMapType($event)" style="border-radius: 50%;">\n\n        <ion-icon name="map" style="font-size: 22px;"></ion-icon>\n\n      </button>\n\n      <br/>\n\n      <button ion-button icon-only color="light" (click)="trafficFunc()" style="border-radius: 50%;">\n\n        <ion-icon name="walk"></ion-icon>\n\n        <!-- <img src="assets/maps/196787.png" height="10" width="10"> -->\n\n      </button>\n\n      <br/>\n\n      <button ion-button icon-only color="light" *ngIf="showShareBtn" (click)="shareLive($event)" style="border-radius: 50%;">\n\n        <ion-icon name="share" style="font-size: 22px;"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n\n  <ion-bottom-drawer [(hidden)]="drawerHidden1" [dockedHeight]="dockedHeight" [bounceThreshold]="bounceThreshold" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop">\n\n    <div class="drawer-content">\n\n        <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">Last Updated On &mdash; {{last_ping_on | date:\'medium\'}}</p>\n\n\n\n\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{address}}</p>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC==\'0\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC==\'1\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC==null" width="20" height="20">\n\n          <p>IGN</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" width="20" height="20">\n\n          <p>AC</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20">\n\n          <p>FUEL</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power==null" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power==\'0\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power==\'1\'" width="20" height="20">\n\n          <p>POWER</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking==null" width="30" height="20">\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking==\'0\'" width="30" height="20">\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking==\'1\'" width="30" height="20">\n\n          <p>GPS</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!total_odo">N/A</p>\n\n          <p style="font-size: 14px;" *ngIf="total_odo">{{total_odo | number : \'1.0-2\'}}</p>\n\n          <p style="font-size: 13px">Odometer</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!vehicle_speed">0 km/h</p>\n\n          <p style="font-size: 14px;" *ngIf="vehicle_speed">{{vehicle_speed}} km/h</p>\n\n          <p style="font-size: 13px">\n\n            Speed\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="fuel">{{fuel}}</p>\n\n          <p style="font-size: 14px;" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">Fuel</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <!-- <p style="font-size: 14px;" *ngIf="!distFromLastStop">N/A</p> -->\n\n          <p style="font-size: 14px;" *ngIf="!distFromLastStop">0 Km</p>\n\n          <p style="font-size: 14px;" *ngIf="distFromLastStop">{{distFromLastStop | number : \'1.0-2\'}} Km</p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!todays_odo">0 Km</p>\n\n          <!-- <p style="font-size: 14px;" *ngIf="!todays_odo">N/A</p> -->\n\n          <p style="font-size: 14px;" *ngIf="todays_odo">{{todays_odo | number : \'1.0-2\'}} km</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!timeAtLastStop">N/A</p>\n\n          <p style="font-size: 14px;" *ngIf="timeAtLastStop">{{timeAtLastStop}}</p>\n\n          <p style="font-size: 13px">At Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!today_stopped">N/A</p>\n\n          <p style="font-size: 14px;" *ngIf="today_stopped">{{today_stopped}}</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!lastStoppedAt">N/A</p>\n\n          <p style="font-size: 14px;" *ngIf="lastStoppedAt">{{lastStoppedAt}}</p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px;" *ngIf="!today_running">N/A</p>\n\n          <p style="font-size: 14px;" *ngIf="today_running">{{today_running}}</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\live-single-device\live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGeofencePageModule", function() { return AddGeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_geofence__ = __webpack_require__(506);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddGeofencePageModule = /** @class */ (function () {
    function AddGeofencePageModule() {
    }
    AddGeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */]),
            ],
        })
    ], AddGeofencePageModule);
    return AddGeofencePageModule;
}());

//# sourceMappingURL=add-geofence.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllNotificationsPageModule", function() { return AllNotificationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_notifications__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AllNotificationsPageModule = /** @class */ (function () {
    function AllNotificationsPageModule() {
    }
    AllNotificationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], AllNotificationsPageModule);
    return AllNotificationsPageModule;
}());

//# sourceMappingURL=all-notifications.module.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllNotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__filter_filter__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as moment from 'moment';
 // with ES6 import

// import { OrderByPipe } from 'fuel-ui/fuel-ui';
var AllNotificationsPage = /** @class */ (function () {
    function AllNotificationsPage(navCtrl, navParams, apiCall, events, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.events = events;
        this.popoverCtrl = popoverCtrl;
        this.NotifyData = [];
        this.MassArray1 = [];
        this.page = 1;
        this.items = [];
        this.limit = 15;
        this.keyData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.events.publish('cart:updated', 0);
        for (var i = 0; i < 30; i++) {
            this.items.push(this.items.length);
        }
    }
    AllNotificationsPage.prototype.ngOnInit = function () {
        var _this = this;
        localStorage.removeItem("filterByType");
        localStorage.removeItem("filterByDate");
        this.getUsersOnScroll();
        this.getVehicleList();
        this.socket = __WEBPACK_IMPORTED_MODULE_3_socket_io_client__('https://www.oneqlik.in/sbNotifIO', {
            transports: ['websocket', 'polling']
        });
        this.socket.on('connect', function () {
            console.log('IO Connected page');
            console.log("socket connected page ", _this.socket.connected);
        });
        this.socket.on(this.islogin._id, function (msg) {
            // console.log("tab push notify msg=> " + JSON.stringify(msg))
            _this.NotifyData.push(msg);
            // this.events.publish('cart:updated', ++this.count);
            console.log("tab notice data=> " + _this.NotifyData);
        });
    };
    AllNotificationsPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getUsersOnScroll();
        refresher.complete();
    };
    AllNotificationsPage.prototype.filterby = function (ev) {
        var _this = this;
        // console.log("populated=> " + JSON.stringify(data))
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__filter_filter__["a" /* FilterPage */], {
            cssClass: 'iosPop-popover'
        });
        popover.present({
            ev: ev
        });
        popover.onDidDismiss(function (data) {
            // console.log("from popover=> " + JSON.stringify(data))
            if (localStorage.getItem("types") != null) {
                var typeArr = [];
                if (data != null) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            typeArr.push(data[i].filterValue);
                        }
                    }
                    _this.keyData = typeArr;
                    _this.filterByType();
                }
            }
            else {
                if (localStorage.getItem("dates") != null) {
                    // console.log("dates=> ", data)
                    _this.dates = data;
                    _this.filterByDate();
                }
            }
        });
    };
    AllNotificationsPage.prototype.filterByType = function () {
        var that = this;
        console.log(that.keyData);
        localStorage.setItem("filterByType", "filterByType");
        that.apiCall.startLoading().present();
        that.apiCall.filterByType(that.islogin._id, that.page, that.limit, that.keyData)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            that.MassArray1 = that.ndata;
            localStorage.removeItem("types");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.filterByDate = function () {
        var that = this;
        localStorage.setItem("filterByDate", "filterByDate");
        that.apiCall.startLoading().present();
        that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            that.MassArray1 = that.ndata;
            localStorage.removeItem("dates");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.temp = function (key) {
        var _this = this;
        console.log(key);
        this.apiCall.startLoading().present();
        this.apiCall.getFilteredcall(this.islogin._id, this.page, this.limit, key.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data;
            _this.MassArray1 = _this.ndata;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.getUsersOnScroll = function () {
        var _this = this;
        // this.apiCall.startLoading().present();
        this.apiCall.getDataOnScroll(this.islogin._id, this.page, this.limit)
            .subscribe(function (res) {
            // this.apiCall.stopLoading();
            // console.log("res=> " + res)
            _this.ndata = res;
            _this.MassArray1 = _this.ndata;
        }, function (error) {
            // this.apiCall.stopLoading();
            console.log(error);
        });
    };
    AllNotificationsPage.prototype.getVehicleList = function () {
        var that = this;
        // that.apiCall.startLoading().present();
        that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
            .subscribe(function (data) {
            // that.apiCall.stopLoading();
            that.portstemp = data.devices;
            // console.log("vehicle list=> " + that.portstemp)
        }, function (err) {
            // that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            if (localStorage.getItem("filterByType") != null) {
                that.filterByType();
            }
            else {
                if (localStorage.getItem("filterByDate") != null) {
                    that.filterByDate();
                }
                else {
                    if (that.selectedVehicle != undefined) {
                        that.temp(that.selectedVehicle);
                    }
                    else {
                        that.apiCall.getDataOnScroll(that.islogin._id, that.page, that.limit)
                            .subscribe(function (res) {
                            that.ndata = res;
                            for (var i = 0; i < that.ndata.length; i++) {
                                that.MassArray1.push(that.ndata[i]);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 1000);
    };
    AllNotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-all-notifications',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\all-notifications\all-notifications.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Notifications</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="filterby($event)">\n        <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item *ngIf="!titleText" class="itemStyle">\n    <ion-label>Select Vehicle</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="temp(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <div *ngIf="MassArray1.length == 0">\n    <p padding-left>Oops.. No data found for selected vehicle..</p>\n  </div>\n  <div *ngIf="MassArray1.length != 0">\n    <ion-list>\n      <ion-item *ngFor="let notif of MassArray1; let i= index;">\n\n        <ion-avatar item-start>\n          <img src="assets/imgs/speedlimit.png" *ngIf="notif.type == \'overspeed\' ">\n          <img src="assets/imgs/geofence.jpg" *ngIf="notif.type == \'Geo-Fence\' ">\n          <img src="assets/imgs/ignition-switch-icon.jpg" *ngIf="notif.type == \'IGN\' ">\n          <img src="assets/imgs/car2.png" *ngIf="notif.type == \'route-poi\' ">\n          <img src="assets/imgs/petrolpump4.jpg" *ngIf="notif.type == \'Fuel\' ">\n          <img src="assets/imgs/system_status-noun_63767_cc.png" *ngIf="notif.type == \'status\'">\n          <img src="assets/imgs/AccurateMapping.jpg" *ngIf="notif.type == \'Route\'">\n          <img src="assets/imgs/397px-Snow_flake.svg.png" *ngIf="notif.type == \'AC\' ">\n          <img src="assets/imgs/power-png-icon-6.png" *ngIf="notif.type == \'power\'">\n        </ion-avatar>\n        <h2>{{notif.type}}</h2>\n        <p ion-text text-wrap>{{notif.item[\'sentence\']}}</p>\n        <ion-row item-end>\n          <ion-col width-40>\n            <div style="margin-top:19%;">\n              <p style="text-align: right;font-size: 12px;">{{notif.timestamp | date: \'mediumDate\'}}</p>\n            </div>\n            <div style="margin-top:19%;">\n              <p style="text-align: right;font-size: 12px;">{{notif.timestamp | date:\'shortTime\'}}</p>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-list>\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\all-notifications\all-notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AllNotificationsPage);
    return AllNotificationsPage;
}());

//# sourceMappingURL=all-notifications.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';
// import * as moment from 'moment';
var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: "Overspeed", filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: "Ignition", filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: "Geofence", filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: "Stoppage", filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: "Fuel", filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: "AC", filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: "Power", filterValue: 'power', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
    };
    // dateOpen() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }
    // changeDTformat(fromDT) {
    //     let that = this;
    //     that.modal.fromDate = moment(new Date(fromDT), "YYYY-MM-DD").format('dd/mm/yyyy');
    //     console.log("moment=> ", that.modal.fromDate);
    // }
    FilterPage.prototype.applyFilter = function () {
        console.log(this.modal);
        console.log(this.selectedArray);
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\all-notifications\filter\filter.html"*/'<ion-list>\n\n    <!-- <button ion-item (click)="close()">Close</button> -->\n\n    <ion-list-header style="text-align: center;">\n\n        Filter By\n\n    </ion-list-header>\n\n    <hr>\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">Type</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">Time</ion-item-divider>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb2">From</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.fromDate"/>\n\n        </div>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb1">To</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.toDate"/>\n\n        </div>\n\n    </ion-item-group>\n\n\n\n\n\n\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center;">\n\n        <button ion-button clear small color="light" (click)="cancel()">Cancel</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">Apply Filter</button>\n\n    </ion-col>\n\n</ion-row>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\all-notifications\filter\filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactUsPageModule = /** @class */ (function () {
    function ContactUsPageModule() {
    }
    ContactUsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"]
            ],
        })
    ], ContactUsPageModule);
    return ContactUsPageModule;
}());

//# sourceMappingURL=contact-us.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageModule", function() { return CustomersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__customers__ = __webpack_require__(508);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { AddDeviceModalPage } from './modals/add-device-modal';
var CustomersPageModule = /** @class */ (function () {
    function CustomersPageModule() {
    }
    CustomersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */]),
            ],
            exports: []
        })
    ], CustomersPageModule);
    return CustomersPageModule;
}());

//# sourceMappingURL=customers.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateCustModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateCustModalPage = /** @class */ (function () {
    function UpdateCustModalPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.devicedetail = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("_id=> " + this.islogin._id);
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.customer = navPar.get("param");
        // console.log("customer=> " + JSON.stringify(this.customer))
        // console.log("cust stat=> ", moment(this.customer.date, 'dd/mm/yyyy').toDate().toISOString());
        // console.log("one year later date=> " + this.customer.date)
        if (this.customer.date == null) {
            var tempdate = new Date();
            tempdate.setDate(tempdate.getDate() + 365);
            // console.log("current year=> ", new Date(tempdate).toISOString())
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
            // console.log("current year if=> ", this.yearLater)
        }
        else {
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(this.customer.date, 'DD/MM/YYYY').format('YYYY-MM-DD');
            // console.log("current year else=> ", this.yearLater)
        }
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this.customer.first_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this.customer.last_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [this.customer.email, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            phone: [this.customer.phone, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            address: [this.customer.address, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            date: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        });
    }
    UpdateCustModalPage.prototype.ngOnInit = function () {
        this.getAllDealers();
    };
    UpdateCustModalPage.prototype.getAllDealers = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = 'http://51.38.175.41/users/getAllDealerVehicles';
        this.apiCall.getAllDealerVehiclesCall(baseURLp)
            .subscribe(function (data) {
            _this.getAllDealersData = data;
        }, function (err) {
            console.log(err);
        });
    };
    UpdateCustModalPage.prototype.DealerselectData = function (dealerselect) {
        console.log(dealerselect);
        this.dealerdata = dealerselect;
        console.log(this.dealerdata.dealer_id);
    };
    UpdateCustModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateCustModalPage.prototype.updateCustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            console.log(this.updatecustForm.value);
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                // "expire_date": this.updatecustForm.value.date,
                "expire_date": new Date(this.updatecustForm.value.date).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid
            };
            if (this.vehType == undefined) {
                this.devicedetail;
            }
            else {
                this.devicedetail.vehicleType = this.vehType._id;
                // this.vehType._id
            }
            console.log(this.devicedetail);
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.editdata = data;
                // console.log("editdata=>"+ this.editdata);
                var toast = _this.toastCtrl.create({
                    message: "data updated successfully!",
                    position: 'bottom',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.editdata);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    UpdateCustModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\update-cust\update-cust.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Edit Customer Details</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button ion-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="updatecustForm">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n\n            <ion-input formControlName="userid" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.userid.valid && (updatecustForm.controls.userid.dirty || submitAttempt)">\n\n            <p>user id is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n\n            <ion-input formControlName="first_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.first_name.valid && (updatecustForm.controls.first_name.dirty || submitAttempt)">\n\n            <p>first name is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n\n            <ion-input formControlName="last_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.last_name.valid && (updatecustForm.controls.last_name.dirty || submitAttempt)">\n\n            <p>last name is required!</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Email ID*</ion-label>\n\n            <ion-input formControlName="email" type="email"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.email.valid && (updatecustForm.controls.email.dirty || submitAttempt)">\n\n            <p>email id is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n\n            <ion-input formControlName="phone" type="number" maxlength="10" minlength="10"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.phone.valid && (updatecustForm.controls.phone.dirty || submitAttempt)">\n\n            <p>mobile number is required and should be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Address</ion-label>\n\n            <ion-input formControlName="address" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.address.valid && (updatecustForm.controls.address.dirty || submitAttempt)">\n\n            <p>address is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Expire On</ion-label>\n\n            <ion-input type="date" formControlName="date" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n            <!-- <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" formControlName="date" style="margin-left: -7px;"></ion-datetime> -->\n\n        </ion-item>\n\n        \n\n        <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>Dealers</ion-label>\n\n            <ion-select formControlName="dealer_firstname">\n\n                <ion-option *ngFor="let dealer of getAllDealersData" [value]="dealer.dealer_firstname" (ionSelect)="DealerselectData(dealer)">{{dealer.dealer_firstname}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </form>\n\n    <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="updateCustomer()">UPDATE CUSTOMER DETAILS</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\update-cust\update-cust.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"]])
    ], UpdateCustModalPage);
    return UpdateCustModalPage;
}());

//# sourceMappingURL=update-cust.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCustomerModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddCustomerModal = /** @class */ (function () {
    // isDealer: any;
    function AddCustomerModal(navCtrl, navParams, formBuilder, apicallCustomer, alerCtrl, viewCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apicallCustomer = apicallCustomer;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.customerdata = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        console.log("isDealer=> " + this.isSuperAdminStatus);
        this.addcustomerform = formBuilder.group({
            userId: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            Firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            LastName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            emailid: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            contact_num: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            confpassword: [''],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            dealer_firstname: ['']
        });
    }
    AddCustomerModal.prototype.ngOnInit = function () {
        this.getAllDealers();
    };
    AddCustomerModal.prototype.dealerOnChnage = function (dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    };
    AddCustomerModal.prototype.addcustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {
            this.customerdata = {
                "first_name": this.addcustomerform.value.Firstname,
                "last_name": this.addcustomerform.value.LastName,
                "email": this.addcustomerform.value.emailid,
                "phone": this.addcustomerform.value.contact_num,
                "password": this.addcustomerform.value.password,
                "isDealer": false,
                "custumer": true,
                "status": true,
                "user_id": this.addcustomerform.value.userId,
                "address": this.addcustomerform.value.address,
            };
            if (this.dealerdata != undefined) {
                this.customerdata.Dealer = this.dealerdata.dealer_id;
            }
            else {
                this.customerdata.Dealer = this.islogin._id;
            }
            this.apicallCustomer.startLoading().present();
            this.apicallCustomer.addcustomerCall(this.customerdata)
                .subscribe(function (data) {
                _this.apicallCustomer.stopLoading();
                _this.Customeradd = data;
                console.log("devicesadd=> ", _this.Customeradd);
                var toast = _this.toastCtrl.create({
                    message: 'Customer was added successfully',
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                _this.apicallCustomer.stopLoading();
                var body = err._body;
                console.log(body);
                var msg = JSON.parse(body);
                console.log(msg);
                var namepass = [];
                namepass = msg.split(":");
                var name = namepass[1];
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: name,
                    buttons: ['OK']
                });
                alert.present();
                console.log(err);
            });
        }
    };
    AddCustomerModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddCustomerModal.prototype.getAllDealers = function () {
        var _this = this;
        console.log("get dealer");
        var baseURLp = 'http://51.38.175.41/users/getAllDealerVehicles';
        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.selectDealer = data;
            console.log(_this.selectDealer);
        }, function (error) {
            console.log(error);
        });
    };
    AddCustomerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-customer-model',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\add-customer-modal\add-customer-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Customer</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="addcustomerform">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n\n            <ion-input formControlName="userId" type="text"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.userId.valid && (addcustomerform.controls.userId.dirty || submitAttempt)">\n\n            <p>user id required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n\n            <ion-input formControlName="Firstname" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.Firstname.valid && (addcustomerform.controls.Firstname.dirty || submitAttempt)">\n\n            <p>first name required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n\n            <ion-input formControlName="LastName" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.LastName.valid && (addcustomerform.controls.LastName.dirty || submitAttempt)">\n\n            <p>last name required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Email ID *</ion-label>\n\n            <ion-input formControlName="emailid" type="email"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.emailid.valid && (addcustomerform.controls.emailid.dirty || submitAttempt)">\n\n            <p>email id required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Mobile Number*</ion-label>\n\n            <ion-input formControlName="contact_num" type="number" maxlength="10" minlength="10"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.contact_num.valid && (addcustomerform.controls.contact_num.dirty || submitAttempt)">\n\n            <p>mobile number required and should be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Password*</ion-label>\n\n            <ion-input formControlName="password" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.password.valid && (addcustomerform.controls.password.dirty || submitAttempt)">\n\n            <p>Password required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Address*</ion-label>\n\n            <ion-input formControlName="address" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!addcustomerform.controls.address.valid && (addcustomerform.controls.address.dirty || submitAttempt)">\n\n            <p>Address required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>Dealers</ion-label>\n\n            <ion-select formControlName="dealer_firstname" style="min-width:49%;">\n\n                <ion-option *ngFor="let dealer of selectDealer" [value]="dealer.dealer_firstname" (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n    </form>\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="addcustomer()">ADD CUSTOMER</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\modals\add-customer-modal\add-customer-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], AddCustomerModal);
    return AddCustomerModal;
}());

//# sourceMappingURL=add-customer-modal.js.map

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(345);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { SelectSearchableComponent } from 'ionic-select-searchable';



// import { LiveSingleDevice } from '../live-single-device/live-single-device';
// import * as $ from 'jquery';
// class Port {
//   public id: number;
//   public name: string;
// }
var LivePage = /** @class */ (function () {
    function LivePage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, geolocation, modalCtrl, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.geolocation = geolocation;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.drawerHidden = true;
        this.drawerHidden1 = false;
        this.shouldBounce = true;
        this.dockedHeight = 150;
        this.bounceThreshold = 500;
        this.distanceTop = 56;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon
        };
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device")) {
            console.log("device details=> ", navParams.get("device"));
            // console.log("stringify device details=> ", JSON.stringify(navParams.get("device")))
        }
    }
    LivePage.prototype.newMap = function () {
        // let mapOptions = {
        //   controls: {
        //     // compass: true,
        //     // myLocationButton: false,
        //     // myLocation: true,
        //     // indoorPicker: true,
        //     zoom: true,
        //     // mapToolbar: true
        //   },
        //   mapTypeControlOptions: {
        //     mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
        //   }
        // }
        // let map = GoogleMaps.create('map_canvas', mapOptions);
        // map.animateCamera({
        //   target: { lat: 20.5937, lng: 78.9629 },
        //   zoom: 10,
        //   duration: 2000,
        //   padding: 0,  // default = 20px
        // })
        // this.geolocation.getCurrentPosition().then((position) => {
        //   var pos = {
        //     lat: position.coords.latitude,
        //     lng: position.coords.longitude
        //   };
        //   map.setCameraTarget(pos);
        // }).catch((error) => {
        //   console.log('Error getting location', error);
        // });
        // let mapOptions: GoogleMapOptions =
        // {
        //   camera:
        //     { zoom: 10 }
        // };
        // let map = GoogleMaps.create('map_canvas', mapOptions);
        var mapOptions = {
            camera: { target: { lat: 20.5937, lng: 78.9629 },
                zoom: 4 }
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
        return map;
    };
    LivePage.prototype.ngOnInit = function () {
        // let that = this;
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"]('http://51.38.175.41/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = false;
        // if (localStorage.getItem("LiveDevice") != null) {
        //   var paramData = this.navParams.get("device");
        //   console.log(paramData);
        //   this.titleText = paramData.Device_Name;
        //   this.temp(paramData);
        //   this.showActionSheet = true;
        // }
        // else {
        this.drawerHidden = true;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
        this.userDevices();
        // }
    };
    LivePage.prototype.ngOnDestroy = function () {
        var _this = this;
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        this._io.on('disconnect', function () {
            _this._io.open();
        });
    };
    LivePage.prototype.chooseMapType = function (event) {
        var that = this;
        var actionSheet = that.actionSheetCtrl.create({
            title: 'Map Type',
            buttons: [
                {
                    text: 'Normal',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].NORMAL);
                    }
                },
                {
                    text: 'Satellite',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].SATELLITE);
                    }
                },
                {
                    text: 'Hybrid',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].HYBRID);
                    }
                },
                {
                    text: 'Terrain',
                    handler: function () {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].TERRAIN);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    LivePage.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LivePage.prototype.shareLive = function () {
        // console.log("email=> ", this.email);
        // console.log("time=> ", this.time);
        // console.log("tempName=> ", this.tempName)
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response=> ", data);
            _this.resToken = data.t;
            // this.sendToken(this.resToken);
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.liveShare = function () {
        var that = this;
        // let profileModal = this.modalCtrl.create(LiveShareModal, { liveData: that.liveDataShare });
        // profileModal.present();
        var link = "https://51.38.175.41/share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "trackdigi- Live Trip", "", link);
    };
    LivePage.prototype.userDevices = function () {
        var that = this;
        ////////////// Native work starts ///////////////////
        console.log("map data=> ", that.allData.map);
        if (that.allData.map != undefined) {
            // that.allData.map.clear();
            that.allData.map.remove();
            that.allData.map = that.newMap();
            // that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map = that.newMap();
            // that.allData.map.setTrafficEnabled(true);
        }
        ////////////// Native work ends ///////////////////
        var link = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        that.apiCall.startLoading().present();
        that.apiCall.getdevicesApi(link)
            .subscribe(function (resp) {
            that.apiCall.stopLoading();
            console.log("resp1=> ", resp);
            that.portstemp = resp.devices;
            // that.mapData = [];
            // that.mapData = that.portstemp.map(function (d) {
            //   return { lat: d.last_location['lat'], lng: d.last_location['long'] };
            // });
            // let bounds: LatLngBounds = new LatLngBounds(that.mapData);
            // var center = bounds.getCenter();
            // console.log("bounds=> ", bounds.getCenter())
            // console.log("lat long=> ", JSON.stringify(that.mapData));
            // that.allData.map.setCameraTarget(center);
            for (var i = 0; i < resp.devices.length; i++) {
                // this.initMarkers(resp.devices[i]);
                that.socketInit(resp.devices[i]);
            }
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LivePage.prototype.socketInit = function (pdata, center) {
        var _this = this;
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            console.log(d4, " => d4");
            var that = _this;
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        console.log("data undefined");
                        return;
                    }
                    //  console.log( that.socketSwitch);
                    if (data._id != undefined && data.last_location != undefined) {
                        // debugger;
                        var key = data._id;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            console.log(data.Device_ID + " has no iconType", data.iconType);
                            return;
                        }
                        ic_1.path = null;
                        // ic.url = 'https://www.oneqlik.in/images/' + data.status.toLowerCase() + data.iconType + '.png#' + data._id;
                        ic_1.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        console.log("url icon=> ", ic_1.url);
                        console.log(data.Device_Name, "_", data.status, "_", data.last_speed);
                        that.vehicle_speed = data.last_speed;
                        console.log("vehicle speed=> ", that.vehicle_speed);
                        that.todays_odo = data.today_odo;
                        console.log("today's odo => ", that.todays_odo);
                        that.total_odo = data.total_odo;
                        console.log("total odo=> ", that.total_odo);
                        that.fuel = data.currentFuel;
                        console.log("fuel=> ", that.fuel);
                        if (data.lastStoppedAt != null) {
                            if (!isNaN(data.lastStoppedAt)) {
                                var fd = new Date(data.lastStoppedAt).getTime();
                                var td = new Date().getTime();
                                var time_difference = td - fd;
                                var total_min = time_difference / 60000;
                                var hours = total_min / 60;
                                var rhours = Math.floor(hours);
                                var minutes = (hours - rhours) * 60;
                                var rminutes = Math.round(minutes);
                                that.lastStoppedAt = rhours + ':' + rminutes;
                            }
                            else {
                                that.lastStoppedAt = '00' + ':' + '00';
                            }
                        }
                        else {
                            that.lastStoppedAt = '00' + ':' + '00';
                        }
                        console.log("last stopped at=> ", that.lastStoppedAt);
                        console.log("from srvice last stopped at=> ", data.lastStoppedAt);
                        that.distFromLastStop = data.distFromLastStop;
                        console.log("distance from last stop=> ", that.distFromLastStop);
                        if (!isNaN(data.timeAtLastStop)) {
                            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        }
                        else {
                            that.timeAtLastStop = '00:00:00';
                        }
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                        console.log("timeAtLastStop=> ", data.timeAtLastStop);
                        console.log("today_stopped=> ", data.today_stopped);
                        console.log("today_running=> ", data.today_running);
                        that.last_ACC = data.last_ACC;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;
                        // debugger;
                        if (that.allData[key]) {
                            console.log("if vehicle running");
                            that.socketSwitch[key] = data;
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                        }
                        else {
                            console.log("else for initializing");
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            }
                            else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    // position: { lat: data.last_location.lat, lng: data.last_location.long },
                                    icon: ic_1,
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    if (that.selectedVehicle == undefined) {
                                        // if (that.selectedVehicle == undefined && localStorage.getItem("LiveDevice") == null) {
                                        marker.showInfoWindow();
                                    }
                                    // if (that.selectedVehicle != undefined || localStorage.getItem("LiveDevice") != null) {
                                    if (that.selectedVehicle != undefined) {
                                        marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK)
                                            .subscribe(function (e) {
                                            // alert(e.address);
                                            console.log(e);
                                            that.liveVehicleName = data.Device_Name;
                                            that.drawerHidden = false;
                                            that.onClickShow = true;
                                        });
                                    }
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                });
                            }
                        }
                        var geocoder = new google.maps.Geocoder;
                        var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);
                        var request = {
                            "latLng": latlng
                        };
                        geocoder.geocode(request, function (resp, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                //console.log("add data=> " + JSON.stringify(resp))
                                if (resp[0] != null) {
                                    console.log("address is: " + resp[0].formatted_address);
                                    // console.log("index=> ", index)
                                    that.address = resp[0].formatted_address;
                                    console.log(that.address);
                                }
                                else {
                                    console.log("No address available");
                                }
                            }
                            else {
                                // that.allDevices[index].address = device.last_location.lat + ' ,' + device.last_location.long
                                that.address = 'N/A';
                            }
                        });
                        // Geocoder.geocode({
                        //   "position": {
                        //     lat: data.last_location['lat'],
                        //     lng: data.last_location['long']
                        //   }
                        // }).then((results: GeocoderResult[]) => {
                        //   if (results.length == 0) {
                        //     //not found
                        //     return null;
                        //   }
                        //   console.log("address test=> ", results[0])
                        //   let address: any = [
                        //     results[0].subThoroughfare || "",
                        //     results[0].thoroughfare || "",
                        //     results[0].locality || "",
                        //     results[0].adminArea || "",
                        //     results[0].postalCode || "",
                        //     results[0].country || ""
                        //   ].join(", ");
                        //   that.address = address;
                        // });
                    }
                })(d4);
        });
    };
    // liveTrack(map, mark, icons, coords, speed, delay, id) {
    LivePage.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        console.log("center=> " + coords[0]);
        if (center) {
            map.setCameraTarget(coords[0]);
            // map.moveCamera(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                mark.setIcon(icons);
                // map.setCameraBearing(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (localStorage.getItem("LiveDevice") != null) {
                    //   map.setCameraTarget({ lat: lat, lng: lng })
                    //   // map.moveCamera({ lat: lat, lng: lng })
                    // }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget({ lat: lat, lng: lng });
                        // map.moveCamera({ lat: lat, lng: lng })
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (localStorage.getItem("LiveDevice") != null) {
                    //   map.setCameraTarget(dest);
                    //   // map.moveCamera(dest)
                    // }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget(dest);
                        // map.moveCamera(dest)
                    }
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.drawerHidden = true;
        that.onClickShow = false;
        if (that.allData.map != undefined) {
            console.log("map is not undefined..!");
            // that.allData.map.clear();
            that.allData.map.remove();
        }
        console.log("on select change data=> " + JSON.stringify(data));
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        if (data) {
            if (data.last_location) {
                console.log("if last location");
                // that.allData.map = GoogleMaps.create('map_canvas', {
                //   camera: {
                //     target: {
                //       lat: data.last_location['lat'],
                //       lng: data.last_location['long']
                //     },
                //     zoom: 15,
                //     tilt: 30
                //   }
                // });
                var mapOptions = {
                    backgroundColor: 'white',
                    controls: {
                        compass: true,
                        myLocationButton: false,
                        myLocation: true,
                        indoorPicker: true,
                        zoom: true,
                        mapToolbar: true
                    }
                };
                var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
                map.animateCamera({
                    target: { lat: 20.5937, lng: 78.9629 },
                    zoom: 15,
                    // zoom: 17,
                    // tilt: 10,
                    // bearing: 140,
                    duration: 1000,
                    padding: 0 // default = 20px
                });
                map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                that.allData.map = map;
                // that.allData.map.setTrafficEnabled(true);
                that.socketInit(data);
            }
            else {
                console.log("else last location");
                that.allData.map = that.newMap();
                // that.allData.map.setTrafficEnabled(true);
                that.socketInit(data);
            }
            if (that.selectedVehicle != undefined) {
                that.drawerHidden = false;
                that.onClickShow = true;
            }
        }
        // if (localStorage.getItem("LiveDevice") == null) {
        //   that.showBtn = true;
        //   that.SelectVehicle = "Selected Vehicle";
        // }
        that.showBtn = true;
    };
    LivePage.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LivePage.prototype.show = function (data, key) {
        if (data != undefined) {
            if (key == 'last_ping_on') {
                return __WEBPACK_IMPORTED_MODULE_5_moment__(data[key]).format('DD/MM/YYYY, h:mm:ss a');
            }
            else {
                return data[key];
            }
        }
    };
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\live\live.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Live Tracking\n      <span *ngIf="titleText">For {{titleText}}</span>\n    </ion-title>\n    <ion-buttons end *ngIf="showBtn">\n      <button ion-button (click)="ngOnInit()">All Vehicles</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item *ngIf="!titleText">\n    <ion-label>{{SelectVehicle}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="temp(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <div id="map_canvas">\n    <ion-buttons end>\n      <button ion-button icon-only color="light" (click)="chooseMapType($event)" style="border-radius: 50%;">\n        <ion-icon name="map" style="font-size: 22px;"></ion-icon>\n      </button>\n      <br/>\n      <button ion-button icon-only color="light" (click)="trafficFunc()" style="border-radius: 50%;">\n        <ion-icon name="walk"></ion-icon>\n        <!-- <img src="assets/maps/196787.png" height="10" width="10"> -->\n      </button>\n      <br/>\n      <button ion-button icon-only color="light" *ngIf="showShareBtn" (click)="shareLive($event)" style="border-radius: 50%;">\n        <ion-icon name="share" style="font-size: 22px;"></ion-icon>\n      </button>\n    </ion-buttons>\n  </div>\n</ion-content>\n<div *ngIf="onClickShow" class="divPlan">\n  <ion-bottom-drawer [(hidden)]="drawerHidden" [dockedHeight]="dockedHeight" [bounceThreshold]="bounceThreshold" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop">\n    <div class="drawer-content">\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n      <p padding-left style="font-size: 13px" *ngIf="address">{{address}}</p>\n      <hr>\n      <ion-row>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC==\'0\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC==\'1\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC==null" width="20" height="20">\n          <p>IGN</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ac_na.png" width="20" height="20">\n          <p>AC</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20">\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20">\n          <p>FUEL</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power==null" width="20" height="20">\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power==\'0\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power==\'1\'" width="20" height="20">\n          <p>POWER</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking==null" width="30" height="20">\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking==\'0\'" width="30" height="20">\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking==\'1\'" width="30" height="20">\n          <p>GPS</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!total_odo">N/A</p>\n          <p style="font-size: 14px;" *ngIf="total_odo">{{total_odo | number : \'1.0-2\'}}</p>\n          <p style="font-size: 13px">Odometer</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!vehicle_speed">0 km/h</p>\n          <p style="font-size: 14px;" *ngIf="vehicle_speed">{{vehicle_speed}} km/h</p>\n          <p style="font-size: 13px">\n            Speed\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="fuel">{{fuel}}</p>\n          <p style="font-size: 14px;" *ngIf="!fuel">N/A</p>\n          <p style="font-size: 13px">Fuel</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!distFromLastStop">0 Km</p>\n          <p style="font-size: 14px;" *ngIf="distFromLastStop">{{distFromLastStop | number : \'1.0-2\'}} Km</p>\n          <p style="font-size: 13px">From Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!todays_odo">0 Km</p>\n          <p style="font-size: 14px;" *ngIf="todays_odo">{{todays_odo | number : \'1.0-2\'}} Km</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!timeAtLastStop">N/A</p>\n          <p style="font-size: 14px;" *ngIf="timeAtLastStop">{{timeAtLastStop}}</p>\n          <p style="font-size: 13px">At Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!today_stopped">N/A</p>\n          <p style="font-size: 14px;" *ngIf="today_stopped">{{today_stopped}}</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!lastStoppedAt">N/A</p>\n          <p style="font-size: 14px;" *ngIf="lastStoppedAt">{{lastStoppedAt}}</p>\n          <p style="font-size: 13px">From Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!today_running">N/A</p>\n          <p style="font-size: 14px;" *ngIf="today_running">{{today_running}}</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n<div *ngIf="showActionSheet" class="divPlan">\n  <ion-bottom-drawer [(hidden)]="drawerHidden1" [dockedHeight]="dockedHeight" [bounceThreshold]="bounceThreshold" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop">\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n      <p padding-left style="font-size: 13px" *ngIf="address">{{address}}</p>\n      <hr>\n      <ion-row>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC==\'0\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC==\'1\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC==null" width="20" height="20">\n          <p>IGN</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ac_na.png" width="20" height="20">\n          <p>AC</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20">\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20">\n          <p>FUEL</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power==null" width="20" height="20">\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power==\'0\'" width="20" height="20">\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power==\'1\'" width="20" height="20">\n          <p>POWER</p>\n        </ion-col>\n        <ion-col width-10 style="text-align:center">\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking==null" width="30" height="20">\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking==\'0\'" width="30" height="20">\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking==\'1\'" width="30" height="20">\n          <p>GPS</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!total_odo">N/A</p>\n          <p style="font-size: 14px;" *ngIf="total_odo">{{total_odo | number : \'1.0-2\'}}</p>\n          <p style="font-size: 13px">Odometer</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!vehicle_speed">0 km/h</p>\n          <p style="font-size: 14px;" *ngIf="vehicle_speed">{{vehicle_speed}} km/h</p>\n          <p style="font-size: 13px">\n            Speed\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="fuel">{{fuel}}</p>\n          <p style="font-size: 14px;" *ngIf="!fuel">N/A</p>\n          <p style="font-size: 13px">Fuel</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!distFromLastStop">0 Km</p>\n          <p style="font-size: 14px;" *ngIf="distFromLastStop">{{distFromLastStop | number : \'1.0-2\'}} Km</p>\n          <p style="font-size: 13px">From Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!todays_odo">0 Km</p>\n          <p style="font-size: 14px;" *ngIf="todays_odo">{{todays_odo | number : \'1.0-2\'}} km</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!timeAtLastStop">N/A</p>\n          <p style="font-size: 14px;" *ngIf="timeAtLastStop">{{timeAtLastStop}}</p>\n          <p style="font-size: 13px">At Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!today_stopped">N/A</p>\n          <p style="font-size: 14px;" *ngIf="today_stopped">{{today_stopped}}</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n      <hr>\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!lastStoppedAt">N/A</p>\n          <p style="font-size: 14px;" *ngIf="lastStoppedAt">{{lastStoppedAt}}</p>\n          <p style="font-size: 13px">From Last Stop</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%">\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px;" *ngIf="!today_running">N/A</p>\n          <p style="font-size: 14px;" *ngIf="today_running">{{today_running}}</p>\n          <p style="font-size: 13px">Total</p>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\live\live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceShowPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeofenceShowPage = /** @class */ (function () {
    function GeofenceShowPage(navParam, apiCall) {
        this.apiCall = apiCall;
        this.geoshape = {};
        this.items = navParam.get("param");
        this.gefence = this.items._id;
        this.gefencename = this.items.geoname;
        console.log(this.gefencename);
    }
    GeofenceShowPage.prototype.ngOnInit = function () {
        this.getgeofence();
    };
    GeofenceShowPage.prototype.getgeofence = function () {
        var _this = this;
        console.log("getgeofence");
        var baseURLp = 'http://51.38.175.41/geofencing/getdevicegeofence?gid=' + this.gefence;
        this.apiCall.startLoading().present();
        this.apiCall.getdevicegeofenceCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.geofenceShowdata = data;
            _this.geofenceShowdata = data[0].geofence.coordinates[0];
            console.log(_this.geofenceShowdata);
            _this.geoshape = [];
            _this.geoshape = _this.geofenceShowdata.map(function (p) {
                return {
                    lat: p[1], lng: p[0],
                };
            });
            // console.log("geoshape1=> " + this.geoshape);
            // console.log("geoshape2=> " + JSON.stringify(this.geoshape));
            var temp = JSON.stringify(_this.geoshape);
            var temp2 = JSON.parse(temp);
            console.log(temp2);
            _this.draw(temp2);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    GeofenceShowPage.prototype.draw = function (zzz) {
        var map;
        var infoWindow;
        this.GeoFencCoords = zzz;
        var GeoName = "yyy";
        function initMap(GeoFencCoords) {
            map = new google.maps.Map(document.getElementById('mapShowGeofence'), {
                zoom: 17,
                center: { lat: GeoFencCoords[0].lat, lng: GeoFencCoords[0].lng },
                mapTypeId: 'terrain'
            });
            // console.log("polygon => " + JSON.stringify(GeoFencCoords))
            var bermudaTriangle = new google.maps.Polygon({
                paths: GeoFencCoords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });
            bermudaTriangle.setMap(map);
            // Add a listener for the click event.
            bermudaTriangle.addListener('click', showArrays);
            infoWindow = new google.maps.InfoWindow;
        }
        /** @this {google.maps.Polygon} */
        function showArrays(event) {
            // var vertices = this.getPath();
            var contentString = '<b>Geo Fence</b><br>' +
                'Name: ' + GeoName;
            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);
            infoWindow.open(map);
        }
        initMap(this.GeoFencCoords);
    };
    GeofenceShowPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\geofence-show\geofence-show.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{items.geoname}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div id="mapShowGeofence"></div>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\geofence-show\geofence-show.html"*/,
            styles: ["\n    #mapShowGeofence{\n        height: 100%;\n         width: 100%;\n     }"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], GeofenceShowPage);
    return GeofenceShowPage;
}());

//# sourceMappingURL=geofence-show.js.map

/***/ }),

/***/ 347:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__geofence__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddGeofencePage = /** @class */ (function () {
    function AddGeofencePage(apiCall, alertCtrl, toastCtrl, fb, navCtrl) {
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        this.geofenceForm = fb.group({
            geofence_name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            check: [false],
            check1: [false]
        });
    }
    AddGeofencePage.prototype.ngOnInit = function () {
        this.runGeofenceMaps();
    };
    AddGeofencePage.prototype.enteringFunc = function () {
        console.log(this.geofenceForm.value.check);
        this.entering = this.geofenceForm.value.check;
    };
    AddGeofencePage.prototype.exitingFunc = function () {
        console.log(this.geofenceForm.value.check1);
        this.exiting = this.geofenceForm.value.check1;
    };
    AddGeofencePage.prototype.disableTap = function () {
        // var container = document.getElementsByClassName('pac-container');
        // angular.element(container).attr('data-tap-disabled', 'true');
        // var backdrop = document.getElementsByClassName('backdrop');
        // angular.element(backdrop).attr('data-tap-disabled', 'true');
        // angular.element(container).on("click", function () {
        //     document.getElementById('pac-input').blur();
        // });
    };
    AddGeofencePage.prototype.creategoefence = function () {
        var _this = this;
        debugger;
        this.submitAttempt = true;
        if (this.geofenceForm.valid) {
            if (this.entering || this.exiting) {
                if (this.geofenceForm.value.geofence_name && this.finalcordinate.length) {
                    console.log("add device click");
                    console.log(this.islogin._id);
                    var data = {
                        "uid": this.islogin._id,
                        "geoname": this.geofenceForm.value.geofence_name,
                        "entering": this.entering,
                        "exiting": this.exiting,
                        "geofence": this.finalcordinate
                    };
                    this.apiCall.startLoading().present();
                    this.apiCall.addgeofenceCall(data)
                        .subscribe(function (data) {
                        _this.apiCall.stopLoading();
                        _this.devicesadd = data;
                        console.log(_this.devicesadd);
                        var toast = _this.toastCtrl.create({
                            message: 'Created geofence successfully',
                            position: 'bottom',
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            console.log('Dismissed toast');
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__geofence__["a" /* GeofencePage */]);
                        });
                        toast.present();
                    }, function (err) {
                        _this.apiCall.stopLoading();
                    });
                }
                else {
                    var toast = this.toastCtrl.create({
                        message: 'Select Geofence On Map!',
                        position: 'top',
                        duration: 2000
                    });
                    toast.present();
                }
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    message: 'All fields are required!',
                    buttons: ['Try Again']
                });
                alert_1.present();
            }
        }
    };
    AddGeofencePage.prototype.runGeofenceMaps = function () {
        var that = this;
        console.log("get location");
        console.log("Running GEO");
        // navigator.geolocation.getCurrentPosition(onWeatherSuccess1, onWeatherError, { enableHighAccuracy: true });
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        function success(pos) {
            var crd = pos.coords;
            console.log(crd);
            console.log('Your current position is:');
            console.log("Latitude : " + crd.latitude);
            console.log("Longitude: " + crd.longitude);
            console.log("More or less " + crd.accuracy + " meters.");
            var latLng = new google.maps.LatLng(crd.latitude, crd.longitude);
            var mapOptions = {
                center: latLng,
                disableDefaultUI: true,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("mapGeofence"), mapOptions);
            var input = (document.getElementById('pac-input'));
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                infowindow.close();
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                }
                else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                // Set the position of the marker using the place ID and location.
                marker.setPlace(/** @type {!google.maps.Place} */ ({
                    placeId: place.place_id,
                    location: place.geometry.location
                }));
                marker.setVisible(true);
            });
            // var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var all_overlays = [];
            var selectedShape;
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT,
                    drawingModes: [
                        // google.maps.drawing.OverlayType.MARKER,
                        // google.maps.drawing.OverlayType.CIRCLE,
                        google.maps.drawing.OverlayType.POLYGON,
                    ]
                },
                // markerOptions: {
                //     icon:image
                // },
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 0.2,
                    strokeWeight: 3,
                    clickable: false,
                    editable: true,
                    zIndex: 1
                },
                polygonOptions: {
                    clickable: true,
                    draggable: true,
                    editable: true,
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                },
                rectangleOptions: {
                    clickable: true,
                    draggable: true,
                    editable: true,
                    fillColor: '#ffff00',
                    fillOpacity: 0.5,
                }
            });
            function clearSelection() {
                if (selectedShape) {
                    selectedShape.setEditable(false);
                    selectedShape = null;
                }
            }
            function setSelection(shape) {
                clearSelection();
                selectedShape = shape;
                shape.setEditable(true);
                google.maps.event.addListener(selectedShape.getPath(), 'set_at', getPolygonCoords(shape));
            }
            // function deleteSelectedShape() {
            //     if (selectedShape) {
            //         selectedShape.setMap(null);
            //         that.finalcordinate = [];
            //     }
            // }
            // function deleteAllShape() {
            //     for (var i = 0; i < all_overlays.length; i++) {
            //         all_overlays[i].overlay.setMap(null);
            //     }
            //     all_overlays = [];
            // }
            // function CenterControl(controlDiv, map) {
            //     // Set CSS for the control border.
            //     var controlUI = document.createElement('div');
            //     controlUI.style.backgroundColor = '#fff';
            //     controlUI.style.border = '2px solid #fff';
            //     controlUI.style.borderRadius = '3px';
            //     controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            //     controlUI.style.cursor = 'pointer';
            //     controlUI.style.marginBottom = '22px';
            //     controlUI.style.textAlign = 'center';
            //     controlUI.title = 'Select to delete the shape';
            //     controlDiv.appendChild(controlUI);
            //     // Set CSS for the control interior.
            //     var controlText = document.createElement('div');
            //     controlText.style.color = 'rgb(25,25,25)';
            //     controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            //     controlText.style.fontSize = '16px';
            //     controlText.style.lineHeight = '38px';
            //     controlText.style.paddingLeft = '5px';
            //     controlText.style.paddingRight = '5px';
            //     controlText.innerHTML = 'Delete Selected Area';
            //     controlUI.appendChild(controlText);
            //     // Setup the click event listeners: simply set the map to Chicago.
            //     controlUI.addEventListener('click', function () {
            //         deleteSelectedShape();
            //         localStorage.removeItem("cordinates");
            //     });
            // }
            drawingManager.setMap(map);
            function getPolygonCoords(newShape) {
                //  console.log("We are one");
                var len = newShape.getPath().getLength();
                that.cord = [];
                that.finalcordinate = [];
                for (var i = 0; i < len; i++) {
                    //  console.log(newShape.getPath().getAt(i).toUrlValue(6));
                    //  console.log(cor)
                    var b = newShape.getPath().getAt(i).toUrlValue(6);
                    console.log(b);
                    var a = [];
                    a.push(parseFloat(b.split(",")[1]));
                    a.push(parseFloat(b.split(",")[0]));
                    that.cord.push(a);
                }
                if (that.cord.length)
                    that.finalcordinate.push(that.cord);
                console.log(that.cord);
                console.log(that.finalcordinate);
                var p = that.cord[0];
                var finalcord = [0];
                that.cord.push(p);
                finalcord.push(that.cord);
                console.log(finalcord);
                var val = JSON.stringify(finalcord);
                //   console.log(val);
                localStorage.setItem('cordinates', val);
                that.isdevice = localStorage.getItem('cordinates');
                /*localStorage.setItem("cordinates", val);*/
            }
            ;
            google.maps.event.addListener(drawingManager, 'polygoncomplete', function (event) {
                event.getPath().getLength();
                google.maps.event.addListener(event.getPath(), 'insert_at', function () {
                    var len = event.getPath().getLength();
                    for (var i = 0; i < len; i++) {
                        // console.log(event.getPath().getAt(i).toUrlValue(5));
                    }
                });
                google.maps.event.addListener(event.getPath(), 'set_at', function () {
                    var len = event.getPath().getLength();
                    for (var i = 0; i < len; i++) {
                        //  console.log(event.getPath().getAt(i).toUrlValue(5));
                    }
                });
            });
            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                all_overlays.push(event);
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    drawingManager.setDrawingMode(null);
                    //Write code to select the newly selected object.
                    var newShape = event.overlay;
                    newShape.type = event.type;
                    google.maps.event.addListener(newShape, 'click', function () {
                        setSelection(newShape);
                    });
                    setSelection(newShape);
                }
            });
            var centerControlDiv = document.createElement('div');
            // var centerControl = new CenterControl(centerControlDiv, map);
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
        }
        function error(err) {
            alert("ERROR(" + err.code + "): " + err.message);
        }
        navigator.geolocation.getCurrentPosition(success, error, options);
    };
    ;
    AddGeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-add-geofence',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\add-geofence\add-geofence.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Geofence</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-item class="item item-input controls">\n\n        <ion-icon name="search" item-start></ion-icon>\n\n        <input item-end id="pac-input" type="text" placeholder="Search Location" data-tap-disabled="true" (ionChange)=\'disableTap()\'\n\n            [(ngModel)]="search"> \n\n   </ion-item>\n\n    <div id="mapGeofence" data-tap-disabled="true"></div>\n\n\n\n    <form [formGroup]="geofenceForm">\n\n        <ion-item class="logitem">\n\n            <ion-input formControlName="geofence_name" type="text" placeholder="GeoFence Name *"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!geofenceForm.controls.geofence_name.valid && (geofenceForm.controls.geofence_name.dirty || submitAttempt)">\n\n            <p>geofence_name required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Entering Geo Fence</ion-label>\n\n            <ion-checkbox color="dark" formControlName="check" (ionChange)="enteringFunc()"></ion-checkbox>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Exiting Geo Fence</ion-label>\n\n            <ion-checkbox color="dark" formControlName="check1" (ionChange)="exitingFunc()"></ion-checkbox>\n\n        </ion-item>\n\n\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center">\n\n                <button ion-button clear color="light" (click)="creategoefence()">Create Geofence</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence\add-geofence\add-geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], AddGeofencePage);
    return AddGeofencePage;
}());

//# sourceMappingURL=add-geofence.js.map

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyReportPageModule", function() { return DailyReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daily_report__ = __webpack_require__(554);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DailyReportPageModule = /** @class */ (function () {
    function DailyReportPageModule() {
    }
    DailyReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__daily_report__["a" /* DailyReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__daily_report__["a" /* DailyReportPage */]),
            ],
        })
    ], DailyReportPageModule);
    return DailyReportPageModule;
}());

//# sourceMappingURL=daily-report.module.js.map

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { ContentDrawerComponent } from '../../components/content-drawer/content-drawer';
// import { ComponentsModule } from '../../components/components.module';
var DashboardPageModule = /** @class */ (function () {
    function DashboardPageModule() {
    }
    DashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]),
            ],
            exports: []
        })
    ], DashboardPageModule);
    return DashboardPageModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSummaryRepoPageModule", function() { return DeviceSummaryRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__ = __webpack_require__(555);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DeviceSummaryRepoPageModule = /** @class */ (function () {
    function DeviceSummaryRepoPageModule() {
    }
    DeviceSummaryRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DeviceSummaryRepoPageModule);
    return DeviceSummaryRepoPageModule;
}());

//# sourceMappingURL=device-summary-repo.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistanceReportPageModule", function() { return DistanceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__distance_report__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DistanceReportPageModule = /** @class */ (function () {
    function DistanceReportPageModule() {
    }
    DistanceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DistanceReportPageModule);
    return DistanceReportPageModule;
}());

//# sourceMappingURL=distance-report.module.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackPageModule", function() { return FeedbackPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feedback__ = __webpack_require__(557);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__ = __webpack_require__(558);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FeedbackPageModule = /** @class */ (function () {
    function FeedbackPageModule() {
    }
    FeedbackPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__["a" /* Ionic2RatingModule */]
            ],
        })
    ], FeedbackPageModule);
    return FeedbackPageModule;
}());

//# sourceMappingURL=feedback.module.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelReportPageModule", function() { return FuelReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_report__ = __webpack_require__(560);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FuelReportPageModule = /** @class */ (function () {
    function FuelReportPageModule() {
    }
    FuelReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_report__["a" /* FuelReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_report__["a" /* FuelReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], FuelReportPageModule);
    return FuelReportPageModule;
}());

//# sourceMappingURL=fuel-report.module.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofenceReportPageModule", function() { return GeofenceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence_report__ = __webpack_require__(561);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GeofenceReportPageModule = /** @class */ (function () {
    function GeofenceReportPageModule() {
    }
    GeofenceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], GeofenceReportPageModule);
    return GeofenceReportPageModule;
}());

//# sourceMappingURL=geofence-report.module.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofencePageModule", function() { return GeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GeofencePageModule = /** @class */ (function () {
    function GeofencePageModule() {
    }
    GeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */]),
            ],
        })
    ], GeofencePageModule);
    return GeofencePageModule;
}());

//# sourceMappingURL=geofence.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPageModule", function() { return GroupsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groups__ = __webpack_require__(562);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupsPageModule = /** @class */ (function () {
    function GroupsPageModule() {
    }
    GroupsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */]),
            ],
        })
    ], GroupsPageModule);
    return GroupsPageModule;
}());

//# sourceMappingURL=groups.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\update-group\update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\update-group\update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDevicePageModule", function() { return HistoryDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history_device__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { DatePickerModule } from 'ion-datepicker';

var HistoryDevicePageModule = /** @class */ (function () {
    function HistoryDevicePageModule() {
    }
    HistoryDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]),
                // DatePickerModule,
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__["a" /* DatePicker */]]
        })
    ], HistoryDevicePageModule);
    return HistoryDevicePageModule;
}());

//# sourceMappingURL=history-device.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IgnitionReportPageModule", function() { return IgnitionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ignition_report__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var IgnitionReportPageModule = /** @class */ (function () {
    function IgnitionReportPageModule() {
    }
    IgnitionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], IgnitionReportPageModule);
    return IgnitionReportPageModule;
}());

//# sourceMappingURL=ignition-report.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */]
            ],
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signupOtp__ = __webpack_require__(366);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, formBuilder, apiService, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apiService = apiService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.signupForm = formBuilder.group({
            firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            lastname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mobile: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            confirmpassword: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            check: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.doLogin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    SignupPage.prototype.IsDealer = function (check) {
        this.isdealer = check;
    };
    SignupPage.prototype.getotp = function () {
        var _this = this;
        this.isdealer = false;
        this.submitAttempt = true;
        if (this.signupForm.valid) {
            console.log(this.signupForm.value);
            this.usersignupdetails = this.signupForm.value;
            console.log(this.usersignupdetails);
            localStorage.setItem('usersignupdetails', this.usersignupdetails);
            // $rootScope.signupDetails = store.get('usersignupdetails');
            this.signupDetails = localStorage.getItem("usersignupdetails");
            if (this.signupForm.value.confirmpassword && this.signupForm.value.email && this.signupForm.value.mobile && this.signupForm.value.firstname && this.signupForm.value.lastname && this.signupForm.value.password) {
                if (this.signupForm.value.password == this.signupForm.value.confirmpassword) {
                    var usersignupdata = {
                        "first_name": this.signupForm.value.firstname,
                        "last_name": this.signupForm.value.lastname,
                        "email": this.signupForm.value.email,
                        "password": this.signupForm.value.password,
                        "confirmpass": this.signupForm.value.confirmpassword,
                        "org_name": this.signupForm.value.orgname,
                        "phone": String(this.signupForm.value.mobile),
                        "dealer": this.isdealer
                    };
                    this.apiService.startLoading();
                    this.apiService.signupApi(usersignupdata)
                        .subscribe(function (response) {
                        _this.apiService.stopLoading();
                        _this.signupUseradd = response;
                        console.log(_this.signupUseradd);
                        var toast = _this.toastCtrl.create({
                            message: response.message,
                            duration: 2500,
                            position: 'top'
                        });
                        toast.onDidDismiss(function () {
                            console.log('Dismissed toast');
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__signupOtp__["a" /* SignupOtp */]);
                        });
                        toast.present();
                    }, function (error) {
                        _this.apiService.stopLoading();
                        if (!error.message) {
                            var s = JSON.stringify(error);
                            //var s="Error: Duplicate EmailID or Mobile Number"
                            if (s.indexOf("Error:") > -1) {
                                var alertPopup = _this.alertCtrl.create({
                                    title: 'Signup failed!',
                                    message: s.split(":")[1],
                                    buttons: ['Ok']
                                });
                                alertPopup.present();
                            }
                        }
                        else {
                            var alertPopup = _this.alertCtrl.create({
                                title: 'Signup failed!',
                                message: error.message
                            });
                            alertPopup.present();
                        }
                        _this.responseMessage = "Something Wrong";
                    });
                }
                else {
                    var alertPopup = this.alertCtrl.create({
                        title: 'Warning!',
                        message: "Password and Confirm Password Not Matched",
                        buttons: ['OK']
                    });
                    alertPopup.present();
                }
            }
        }
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\signup\signup.html"*/'<ion-header>\n\n  <ion-navbar color="gpsc">\n    <ion-title>Sign Up</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="signup-page" padding>\n  <form [formGroup]="signupForm">\n    <ion-list no-lines>\n      <ion-item class="itemSty">\n        <ion-label color="light">First Name*</ion-label>\n        <ion-input formControlName="firstname" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Last Name*</ion-label>\n        <ion-input formControlName="lastname" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Email*</ion-label>\n        <ion-input formControlName="email" type="email"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Mobile Number*</ion-label>\n        <ion-input formControlName="mobile" type="number"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Password*</ion-label>\n        <ion-input formControlName="password" type="password"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Confirm Password*</ion-label>\n        <ion-input formControlName="confirmpassword"  type="password"></ion-input>\n      </ion-item>\n      <ion-item class="itemSty">\n        <ion-label color="light">Are you Dealer?</ion-label>\n        <ion-checkbox color="gpsc" formControlName="check" (ionChange)="IsDealer(check)"></ion-checkbox>\n      </ion-item>\n    </ion-list>\n  </form>\n  <div class="btnDiv">\n    <button ion-button block color="gpsc" (tap)="getotp()">NEXT</button>\n    <ion-row>\n      <p ion-text color="light">Already having an account?&nbsp;&nbsp;\n        <span ion-text color="gpsc" (tap)="doLogin()">LOGIN</span>\n      </p>\n    </ion-row>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupOtp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SignupOtp = /** @class */ (function () {
    function SignupOtp() {
    }
    SignupOtp.prototype.signupUser = function () {
    };
    SignupOtp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\signup\signupOtp.html"*/''/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\signup\signupOtp.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SignupOtp);
    return SignupOtp;
}());

//# sourceMappingURL=signupOtp.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MobileVerify; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MobileVerify = /** @class */ (function () {
    function MobileVerify() {
    }
    MobileVerify = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\login\mobil-verify.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Mobile Verification</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding>\n\n  \n\n  </ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\login\mobil-verify.html"*/
        })
    ], MobileVerify);
    return MobileVerify;
}());

//# sourceMappingURL=mobile-verify.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverSpeedRepoPageModule", function() { return OverSpeedRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__ = __webpack_require__(564);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var OverSpeedRepoPageModule = /** @class */ (function () {
    function OverSpeedRepoPageModule() {
    }
    OverSpeedRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], OverSpeedRepoPageModule);
    return OverSpeedRepoPageModule;
}());

//# sourceMappingURL=over-speed-repo.module.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(371);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": this.islogin._id,
            "token": this.token,
            "os": "android"
        };
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        // localStorage.clear();
                        // localStorage.setItem('count', null)
                        // // this.menuCtrl.close();
                        // this.navCtrl.setRoot(LoginPage);
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            // this.menuCtrl.close();
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Me</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="assets/imgs/dummy-user-img.png">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>Account:{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      Service Provider\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      Password\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-icon name="cog" item-start></ion-icon>\n      Settings\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      Logout\n    </ion-item>\n  </ion-item-group>\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">Version {{aVer}}</div>\n  <!-- <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">Powered by </span>&nbsp;\n    <a href="http://www.oneqlik.in/telematics/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.jpg" width="55" height="19">\n    </a>\n  </div> -->\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = this.navParam.get("param");
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Provider Info</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/2.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Name</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{uData.Dealer_ID.first_name}}&nbsp;{{uData.Dealer_ID.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Contacts</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{uData.Dealer_ID.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Mobile</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{uData.Dealer_ID.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>E-mail</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{uData.Dealer_ID.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Address</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.passData = this.navParam.get("param");
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_1 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_2 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_2.present();
            }
            else {
                var toast = this.toastCtrl.create({
                    message: 'Password Updated successfully',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    _this.oldP = "";
                    _this.newP = "";
                    _this.cnewP = "";
                });
                toast.present();
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Change Password</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                Save\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Old Password" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="New Password" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Confirm New Password" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\profile\update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteVoilationsPageModule", function() { return RouteVoilationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__route_voilations__ = __webpack_require__(565);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RouteVoilationsPageModule = /** @class */ (function () {
    function RouteVoilationsPageModule() {
    }
    RouteVoilationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__route_voilations__["a" /* RouteVoilationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__route_voilations__["a" /* RouteVoilationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], RouteVoilationsPageModule);
    return RouteVoilationsPageModule;
}());

//# sourceMappingURL=route-voilations.module.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutePageModule", function() { return RoutePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__route__ = __webpack_require__(566);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { CreateRoutePage } from '../create-route/create-route';
var RoutePageModule = /** @class */ (function () {
    function RoutePageModule() {
    }
    RoutePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__route__["a" /* RoutePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__route__["a" /* RoutePage */]),
            ],
            exports: []
        })
    ], RoutePageModule);
    return RoutePageModule;
}());

//# sourceMappingURL=route.module.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditRouteDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditRouteDetailsPage = /** @class */ (function () {
    // polyArray: any = [];
    function EditRouteDetailsPage(navParam, viewCtrl, toastCtrl, fb, apiCall, alertCtrl) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.fb = fb;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.params = navParam.get("param");
        console.log("params=> ", this.params);
        this.editrouteForm = fb.group({
            source: [this.params.source, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            destination: [this.params.destination, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            route: [this.params.name, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
    }
    EditRouteDetailsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EditRouteDetailsPage.prototype.getRouteEdit = function () {
        // $rootScope.routeid = dvDistance;
        // console.log($rootScope.routeid);
        var renderer = new google.maps.DirectionsRenderer({
            suppressPolylines: true
        });
        // var im = 'http://www.robotwoods.com/dev/misc/bluecircle.png';
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        function error(err) {
            alert("ERROR(" + err.code + "): " + err.message);
        }
        function success(pos) {
            var crd = pos.coords;
            // var image = 'https://maps.google.com/mapfiles/ms/micons/blue.png';
            var myLatLng = new google.maps.LatLng(crd.latitude, crd.longitude);
            var map = new google.maps.Map(document.getElementById('dvMap'), {
                center: myLatLng,
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true
            });
            var directionsDisplay = new google.maps.DirectionsRenderer(renderer);
            var directionsService = new google.maps.DirectionsService();
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));
            // var userMarker = new google.maps.Marker({
            //     position: myLatLng,
            //     map: map,
            //     icon: im
            // });
            //*********DIRECTIONS AND ROUTE**********************//
            var source = document.getElementById("txtSource").value;
            var destination = document.getElementById("txtDestination").value;
            console.log("source=> ", source);
            console.log("destination=> ", destination);
            var polyArray = [];
            directionsService.route({
                origin: source,
                destination: destination,
                travelMode: 'DRIVING',
                provideRouteAlternatives: true
            }, function (response, status) {
                console.log(response);
                if (status === 'OK') {
                    renderer.setDirections(response);
                    renderer.setMap(map);
                    renderer.getRouteIndex();
                    for (var j = 0; j < response.routes.length; j++) {
                        var path = new google.maps.MVCArray();
                        polyArray.push(new google.maps.Polyline({
                            map: map,
                            strokeColor: "#615959",
                            strokeOpacity: 0.3,
                            strokeWeight: 5
                        }));
                        if (j == 0)
                            polyArray[0].setOptions({
                                strokeColor: '#1E90FF',
                                strokeOpacity: 1.0
                            });
                        polyArray[polyArray.length - 1].setPath(path);
                        polyArray[polyArray.length - 1].getPath();
                        google.maps.event.addListener(polyArray[polyArray.length - 1], 'click', function (e) {
                            for (var i = 0; i < polyArray.length; i++) {
                                polyArray[i].setOptions({
                                    strokeOpacity: 0.3,
                                    strokeColor: "#615959"
                                });
                            }
                            this.setOptions({
                                strokeOpacity: 1.0,
                                strokeColor: "#1E90FF"
                            });
                            var s = this.getPath().getArray().toString();
                            var arr = [];
                            var arrNew = [];
                            var sNew = s.split("),(");
                            for (var k = 0; k < sNew.length; k++) {
                                if (k % 2 == 1) {
                                    arrNew.push(sNew[k].split(","));
                                    arr.length = 0;
                                }
                            }
                            console.log(arrNew);
                            this.finlCoords = [];
                            for (var m = 0; m < arrNew.length; m++) {
                                var lat = arrNew[m][0];
                                var latitude = parseFloat(lat);
                                var lang = arrNew[m][1];
                                var longitude = parseFloat(lang);
                                this.finlCoords.push([longitude, latitude]);
                            }
                            console.log(this.finlCoords);
                        });
                        for (var i = 0, len = response.routes[j].overview_path.length; i < len; i++) {
                            path.push(response.routes[j].overview_path[i]);
                        }
                    }
                }
                else {
                    console.log('Directions request failed due to ' + status);
                }
            });
            //*********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: true,
                avoidTolls: true
            }, function (response, status) {
                console.log(response);
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS" && status === google.maps.DirectionsStatus.OK) {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    console.log(distance);
                    console.log(duration);
                }
                else {
                    alert("Unable to find the distance via road.");
                }
            });
        }
        navigator.geolocation.getCurrentPosition(success, error, options);
    };
    EditRouteDetailsPage.prototype.editRoute = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.editrouteForm.valid) {
            console.log(this.editrouteForm.value);
            // console.log("edit form id=> "+ this.editrouteForm.value._id);
            this.coordinate = this.finlCoords;
            /* console.log($rootScope.routesdetail);*/
            var devicedetail = {
                "source": this.editrouteForm.value.source,
                "destination": this.editrouteForm.value.destination,
                "name": this.editrouteForm.value.name,
                "user": this.islogin._id,
                "loc": this.coordinate,
                "_id": this.params._id
            };
            // var link = 'http://13.126.36.205:3000/trackRoute/' + this.editrouteForm.value._id;
            this.apiCall.startLoading().present();
            this.apiCall.gettrackRouteCall(this.params._id, devicedetail)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.editdata = data;
                console.log("editdata=> " + _this.editdata);
                var toast = _this.toastCtrl.create({
                    message: 'Route was updated successfully',
                    position: 'bottom',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.editdata);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error in route update=> ", err);
                // var body = err._body;
                // var msg = JSON.parse(body);
                // let alert = this.alertCtrl.create({
                //     title: 'Oops!',
                //     message: msg.message,
                //     buttons: ['OK']
                // });
                // alert.present();
            });
        }
    };
    EditRouteDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-edit-route-details',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\route\edit-route-details\edit-route-details.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            Edit Route Details\n\n        </ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="editrouteForm">\n\n        <ion-item class="logitem">\n\n            <ion-label>Source Name* </ion-label>\n\n            <ion-input id="txtSource" formControlName="source" type="text" placeholder="Source Name* "></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!editrouteForm.controls.source.valid && (editrouteForm.controls.source.dirty || submitAttempt)">\n\n            <p>source required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item class="logitem">\n\n            <ion-label>Destination Name* </ion-label>\n\n            <ion-input id="txtDestination" formControlName="destination" type="text" placeholder="Destination Name* "></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!editrouteForm.controls.destination.valid && (editrouteForm.controls.destination.dirty || submitAttempt)">\n\n            <p>Destination required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item class="logitem">\n\n            <ion-label>Route Name </ion-label>\n\n            <ion-input formControlName="route" type="text" placeholder="Route Name  "></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!editrouteForm.controls.route.valid && (editrouteForm.controls.route.dirty || submitAttempt)">\n\n            <p>route required!</p>\n\n        </ion-item>\n\n    </form>\n\n    <div id="dvMap" style="width:100%; height:67%;"></div>\n\n    <div id="dvPanel" style="width: 500px; height: 500px"></div>\n\n</ion-content>\n\n<!-- <ion-footer>\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col width-50>\n\n                <button ion-button block (click)="getRouteEdit()">\n\n                    <ion-icon name="navigate"></ion-icon>&nbsp;&nbsp;Get Route</button>\n\n            </ion-col>\n\n            <ion-col width-50>\n\n                <button ion-button block (click)="editRoute()">\n\n                    <ion-icon name="create"></ion-icon>&nbsp;&nbsp;Update Route\n\n                </button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer> -->\n\n\n\n<ion-footer class="footSty">\n\n\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="border-right: 1px solid white; text-align: center;">\n\n                <button ion-button clear color="light" (click)="getRouteEdit()">Get Route</button>\n\n            </ion-col>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="editRoute()">Update Route</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\route\edit-route-details\edit-route-details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["AlertController"]])
    ], EditRouteDetailsPage);
    return EditRouteDetailsPage;
}());

//# sourceMappingURL=edit-route-details.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowGeofencePageModule", function() { return ShowGeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__show_geofence__ = __webpack_require__(567);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ShowGeofencePageModule = /** @class */ (function () {
    function ShowGeofencePageModule() {
    }
    ShowGeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__show_geofence__["a" /* ShowGeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__show_geofence__["a" /* ShowGeofencePage */]),
            ],
        })
    ], ShowGeofencePageModule);
    return ShowGeofencePageModule;
}());

//# sourceMappingURL=show-geofence.module.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(365);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpeedRepoPageModule", function() { return SpeedRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__speed_repo__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SpeedRepoPageModule = /** @class */ (function () {
    function SpeedRepoPageModule() {
    }
    SpeedRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__speed_repo__["a" /* SpeedRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__speed_repo__["a" /* SpeedRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], SpeedRepoPageModule);
    return SpeedRepoPageModule;
}());

//# sourceMappingURL=speed-repo.module.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashPageModule", function() { return SplashPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__splash__ = __webpack_require__(569);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SplashPageModule = /** @class */ (function () {
    function SplashPageModule() {
    }
    SplashPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__splash__["a" /* SplashPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__splash__["a" /* SplashPage */]),
            ],
        })
    ], SplashPageModule);
    return SplashPageModule;
}());

//# sourceMappingURL=splash.module.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoppagesRepoPageModule", function() { return StoppagesRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__ = __webpack_require__(570);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var StoppagesRepoPageModule = /** @class */ (function () {
    function StoppagesRepoPageModule() {
    }
    StoppagesRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], StoppagesRepoPageModule);
    return StoppagesRepoPageModule;
}());

//# sourceMappingURL=stoppages-repo.module.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportPageModule", function() { return SupportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__support__ = __webpack_require__(571);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SupportPageModule = /** @class */ (function () {
    function SupportPageModule() {
    }
    SupportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__support__["a" /* SupportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__support__["a" /* SupportPage */]),
            ],
        })
    ], SupportPageModule);
    return SupportPageModule;
}());

//# sourceMappingURL=support.module.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripReportPageModule", function() { return TripReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trip_report__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TripReportPageModule = /** @class */ (function () {
    function TripReportPageModule() {
    }
    TripReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], TripReportPageModule);
    return TripReportPageModule;
}());

//# sourceMappingURL=trip-report.module.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(591);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Online) {
                _this.setStatus(ConnectionStatusEnum.Offline);
                _this.eventCtrl.publish('network:offline');
            }
            // if (this.previousStatus === ConnectionStatusEnum.Online) {
            //   this.eventCtrl.publish('network:offline');
            // }
            // this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Offline) {
                _this.setStatus(ConnectionStatusEnum.Online);
                _this.eventCtrl.publish('network:online');
            }
            // setTimeout(() => {
            //   if (this.previousStatus === ConnectionStatusEnum.Offline) {
            //     this.eventCtrl.publish('network:online');
            //   }
            //   this.previousStatus = ConnectionStatusEnum.Online;
            // }, 3000);
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this._status.asObservable();
    };
    NetworkProvider.prototype.setStatus = function (status) {
        this.status = status;
        this._status.next(this.status);
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }, {
                title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
            },
            {
                title: 'Reports',
                subPages: [{
                        title: 'Daily Report',
                        component: 'DailyReportPage',
                    }, {
                        title: 'Speed Variation Report',
                        component: 'SpeedRepoPage',
                    }, {
                        title: 'Summary Report',
                        component: 'DeviceSummaryRepoPage',
                    }, {
                        title: 'Geofenceing Report',
                        component: 'GeofenceReportPage',
                    },
                    {
                        title: 'Overspeed Report',
                        component: 'OverSpeedRepoPage',
                    }, {
                        title: 'Ignition Report',
                        component: 'IgnitionReportPage',
                    },
                    {
                        title: 'Route Violation Report',
                        component: 'RouteVoilationsPage',
                    }, {
                        title: 'Stoppage Report',
                        component: 'StoppagesRepoPage',
                    },
                    {
                        title: 'Fuel Report',
                        component: 'FuelReportPage',
                    }, {
                        title: 'Distnace Report',
                        component: 'DistanceReportPage',
                    },
                    {
                        title: 'Trip Report',
                        component: 'TripReportPage',
                    }],
                icon: 'clipboard'
            },
            {
                title: 'Routes',
                subPages: [{
                        title: 'Routes',
                        component: 'RoutePage',
                    }, {
                        title: 'Route Maping',
                        component: 'RouteMapPage',
                    }],
                icon: 'map'
            }, {
                title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
            }, {
                title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
            }, {
                title: 'Support', component: 'SupportPage', icon: 'call'
            }, {
                title: 'About Us', component: 'AboutUsPage', icon: 'alert'
            },];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(592);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\shared\side-menu-content\side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n        <!-- It is a simple option -->\n        <ng-template [ngIf]="!option.suboptionsCount">\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                (tap)="select(option)" tappable>\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n                {{ option.displayText }}\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n            </ion-item>\n        </ng-template>\n\n        <!-- It has nested options -->\n        <ng-template [ngIf]="option.suboptionsCount">\n\n            <ion-list no-margin class="accordion-menu">\n\n                <!-- Header -->\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                    (tap)="toggleItemOptions(option)" tappable>\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n                        item-left></ion-icon>\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n                    {{ option.displayText }}\n                </ion-item>\n\n                <!-- Sub items -->\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n                            tappable (tap)="select(item)">\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n                            {{ item.displayText }}\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n                        </ion-item>\n                    </ng-template>\n                </div>\n            </ion-list>\n\n        </ng-template>\n\n    </ng-template>\n</ion-list>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\shared\side-menu-content\side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(447);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_about_us_about_us_module__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_trip_report_trip_report_module__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_support_support_module__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_stoppages_repo_stoppages_repo_module__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_speed_repo_speed_repo_module__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_show_geofence_show_geofence_module__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_route_voilations_route_voilations_module__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_over_speed_repo_over_speed_repo_module__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_login_login_module__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_add_geofence_add_geofence_module__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_all_notifications_all_notifications_module__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_contact_us_contact_us_module__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_customers_customers_module__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_daily_report_daily_report_module__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_dashboard_dashboard_module__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_device_summary_repo_device_summary_repo_module__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_distance_report_distance_report_module__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_feedback_feedback_module__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_fuel_report_fuel_report_module__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_geofence_geofence_module__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_geofence_report_geofence_report_module__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_groups_groups_module__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_ignition_report_ignition_report_module__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_history_device_history_device_module__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_live_live_module__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_signup_signup_module__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_network_network__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_login_mobile_verify__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_signup_signupOtp__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_add_devices_add_devices_module__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_customers_modals_add_device_modal__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_customers_modals_group_modal_group_modal__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_customers_modals_update_cust_update_cust__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_geofence_geofence_show_geofence_show__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_geofence_add_geofence_add_geofence__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_menu_menu__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_route_route_module__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_route_edit_route_details_edit_route_details__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_google_maps__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_add_devices_add_devices__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__node_modules_ion_bottom_drawer__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_add_devices_update_device_update_device__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_splash_splash_module__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_push__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__ionic_native_date_picker__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_groups_group_model_group_model__ = __webpack_require__(603);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_customers_modals_add_customer_modal_add_customer_modal__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_groups_update_group_update_group__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_all_notifications_filter_filter__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_live_single_device__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__ionic_native_geolocation__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_profile_profile_module__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_profile_profile__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__ionic_native_app_version__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__ionic_native_social_sharing__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











// import { TripReportShowPageModule } from '../pages/trip-report-show/trip-report-show.module';

// import { TripPageModule } from '../pages/trip/trip.module';



// import { ShowTripPageModule } from '../pages/show-trip/show-trip.module';
// import { ShowRoutePageModule } from '../pages/show-route/show-route.module';
// import { ShowMapNearmePageModule } from '../pages/show-map-nearme/show-map-nearme.module';


// import { RouteMapShowPageModule } from '../pages/route-map-show/route-map-show.module';
// import { RouteMapPageModule } from '../pages/route-map/route-map.module';

// import { OdoPageModule } from '../pages/odo/odo.module';

// import { AcReportPageModule } from '../pages/ac-report/ac-report.module';
// import { AddCustomerPageModule } from '../pages/add-customer/add-customer.module';



// import { CreateRoutePageModule } from '../pages/create-route/create-route.module';




// import { DistanceRepoPageModule } from '../pages/distance-repo/distance-repo.module';

// import { DriversRepPageModule } from '../pages/drivers-rep/drivers-rep.module';





// import { IgnitionTabsPageModule } from '../pages/ignition-tabs/ignition-tabs.module';

// import { IgnitionRepoPageModule } from '../pages/ignition-repo/ignition-repo.module';
// import { IdlingRepoPageModule } from '../pages/idling-repo/idling-repo.module';
// import { IdletimeReportPageModule } from '../pages/idletime-report/idletime-report.module';















// import { CreateRoutePage } from '../pages/route/create-route/create-route';

// import { SMS } from '@ionic-native/sms';











// Custom components







// import { ComponentsModule } from '../components/components.module';
// import { ContentDrawerComponent } from '../components/content-drawer/content-drawer';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_60__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_38__pages_login_mobile_verify__["a" /* MobileVerify */],
                __WEBPACK_IMPORTED_MODULE_39__pages_signup_signupOtp__["a" /* SignupOtp */],
                __WEBPACK_IMPORTED_MODULE_41__pages_customers_modals_add_device_modal__["a" /* AddDeviceModalPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_customers_modals_group_modal_group_modal__["a" /* GroupModalPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_customers_modals_update_cust_update_cust__["a" /* UpdateCustModalPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_geofence_geofence_show_geofence_show__["a" /* GeofenceShowPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_geofence_add_geofence_add_geofence__["a" /* AddGeofencePage */],
                // CreateRoutePage,
                __WEBPACK_IMPORTED_MODULE_48__pages_route_edit_route_details_edit_route_details__["a" /* EditRouteDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_add_devices_update_device_update_device__["a" /* UpdateDevicePage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_groups_group_model_group_model__["a" /* GroupModel */],
                __WEBPACK_IMPORTED_MODULE_57__pages_customers_modals_add_customer_modal_add_customer_modal__["a" /* AddCustomerModal */],
                __WEBPACK_IMPORTED_MODULE_58__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_59__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_live_single_device__["a" /* LiveSingleDevice */],
                __WEBPACK_IMPORTED_MODULE_64__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_profile_profile__["c" /* UpdatePasswordPage */]
                // ContentDrawerComponent
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/about-us/about-us.module#AboutUsPageModule', name: 'AboutUsPage', segment: 'about-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel-report/fuel-report.module#FuelReportPageModule', name: 'FuelReportPage', segment: 'fuel-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-geofence/show-geofence.module#ShowGeofencePageModule', name: 'ShowGeofencePage', segment: 'show-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash/splash.module#SplashPageModule', name: 'SplashPage', segment: 'splash', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/support/support.module#SupportPageModule', name: 'SupportPage', segment: 'support', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] }
                    ]
                }),
                // ComponentsModule,
                __WEBPACK_IMPORTED_MODULE_51__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_53__pages_splash_splash_module__["SplashPageModule"],
                __WEBPACK_IMPORTED_MODULE_10__pages_about_us_about_us_module__["AboutUsPageModule"],
                // AcReportPageModule,
                // AddCustomerPageModule,
                __WEBPACK_IMPORTED_MODULE_19__pages_add_geofence_add_geofence_module__["AddGeofencePageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_all_notifications_all_notifications_module__["AllNotificationsPageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_contact_us_contact_us_module__["ContactUsPageModule"],
                // CreateRoutePageModule,
                __WEBPACK_IMPORTED_MODULE_22__pages_customers_customers_module__["CustomersPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__pages_daily_report_daily_report_module__["DailyReportPageModule"],
                __WEBPACK_IMPORTED_MODULE_24__pages_dashboard_dashboard_module__["DashboardPageModule"],
                __WEBPACK_IMPORTED_MODULE_25__pages_device_summary_repo_device_summary_repo_module__["DeviceSummaryRepoPageModule"],
                // DistanceRepoPageModule,
                __WEBPACK_IMPORTED_MODULE_26__pages_distance_report_distance_report_module__["DistanceReportPageModule"],
                // DriversRepPageModule,
                __WEBPACK_IMPORTED_MODULE_27__pages_feedback_feedback_module__["FeedbackPageModule"],
                __WEBPACK_IMPORTED_MODULE_28__pages_fuel_report_fuel_report_module__["FuelReportPageModule"],
                __WEBPACK_IMPORTED_MODULE_29__pages_geofence_geofence_module__["GeofencePageModule"],
                __WEBPACK_IMPORTED_MODULE_30__pages_geofence_report_geofence_report_module__["GeofenceReportPageModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_groups_groups_module__["GroupsPageModule"],
                __WEBPACK_IMPORTED_MODULE_33__pages_history_device_history_device_module__["HistoryDevicePageModule"],
                // IdletimeReportPageModule,
                // IdlingRepoPageModule,
                // IgnitionRepoPageModule,
                __WEBPACK_IMPORTED_MODULE_32__pages_ignition_report_ignition_report_module__["IgnitionReportPageModule"],
                // IgnitionTabsPageModule,
                __WEBPACK_IMPORTED_MODULE_34__pages_live_live_module__["LivePageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_login_login_module__["LoginPageModule"],
                // OdoPageModule,
                __WEBPACK_IMPORTED_MODULE_17__pages_over_speed_repo_over_speed_repo_module__["OverSpeedRepoPageModule"],
                // RouteMapPageModule,
                // RouteMapShowPageModule,
                __WEBPACK_IMPORTED_MODULE_16__pages_route_voilations_route_voilations_module__["RouteVoilationsPageModule"],
                __WEBPACK_IMPORTED_MODULE_15__pages_show_geofence_show_geofence_module__["ShowGeofencePageModule"],
                // ShowMapNearmePageModule,
                // ShowRoutePageModule,
                // ShowTripPageModule,
                __WEBPACK_IMPORTED_MODULE_14__pages_speed_repo_speed_repo_module__["SpeedRepoPageModule"],
                __WEBPACK_IMPORTED_MODULE_13__pages_stoppages_repo_stoppages_repo_module__["StoppagesRepoPageModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_signup_signup_module__["SignupPageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_support_support_module__["SupportPageModule"],
                // TripPageModule,
                __WEBPACK_IMPORTED_MODULE_11__pages_trip_report_trip_report_module__["TripReportPageModule"],
                // TripReportShowPageModule,
                __WEBPACK_IMPORTED_MODULE_40__pages_add_devices_add_devices_module__["AddDevicesPageModule"],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_47__pages_route_route_module__["RoutePageModule"],
                __WEBPACK_IMPORTED_MODULE_63__pages_profile_profile_module__["ProfilePageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_38__pages_login_mobile_verify__["a" /* MobileVerify */],
                __WEBPACK_IMPORTED_MODULE_39__pages_signup_signupOtp__["a" /* SignupOtp */],
                __WEBPACK_IMPORTED_MODULE_41__pages_customers_modals_add_device_modal__["a" /* AddDeviceModalPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_customers_modals_group_modal_group_modal__["a" /* GroupModalPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_customers_modals_update_cust_update_cust__["a" /* UpdateCustModalPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_geofence_geofence_show_geofence_show__["a" /* GeofenceShowPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_geofence_add_geofence_add_geofence__["a" /* AddGeofencePage */],
                // CreateRoutePage,
                __WEBPACK_IMPORTED_MODULE_48__pages_route_edit_route_details_edit_route_details__["a" /* EditRouteDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_add_devices_update_device_update_device__["a" /* UpdateDevicePage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_groups_group_model_group_model__["a" /* GroupModel */],
                __WEBPACK_IMPORTED_MODULE_57__pages_customers_modals_add_customer_modal_add_customer_modal__["a" /* AddCustomerModal */],
                __WEBPACK_IMPORTED_MODULE_58__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_59__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_live_single_device__["a" /* LiveSingleDevice */],
                __WEBPACK_IMPORTED_MODULE_64__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_profile_profile__["c" /* UpdatePasswordPage */]
                // ContentDrawerComponent
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_36__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_37__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_46__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_62__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_google_maps__["g" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_54__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_55__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_65__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_66__ionic_native_social_sharing__["a" /* SocialSharing */]
                // SMS
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutUsPage = /** @class */ (function () {
    function AboutUsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutUsPage');
    };
    AboutUsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-about-us',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\about-us\about-us.html"*/'<ion-header>\n\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>About Us</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card class="card">\n        <img src="assets/imgs/1.jpg" />\n        <div class="card-title">Digicon Technologies</div>\n        <div class="card-subtitle">trackdigi</div>\n    </ion-card>\n\n    <ion-card padding-bottom>\n        <!-- <ion-card-content padding-bottom>\n            <div style="width: 10%;float: left; text-align: center; margin-top: 0%;">\n                <ion-icon name="pin" color="gpsc" style="font-size: 30px;"></ion-icon>\n            </div>\n            <div style="font-size: 16px; width: 90%;float: right; padding-left: 15px; text-align: left">\n                <p>404,purple pride square,kalewadi phata Kalewadi, Pune, Maharashtra 411057.</p>\n            </div>\n        </ion-card-content>\n        <br/> -->\n        <ion-card-content>\n            <div style="width: 10%;float: left; text-align: center; margin-top: 0%;">\n                <ion-icon name="globe" color="primary" style="font-size: 28px;"></ion-icon>\n            </div>\n            <div style="font-size: 16px; width: 90%;float: right; padding-left: 15px; text-align: left; margin-top: 2%;">\n                <p>\n                    <a href="http://digiconsecure.com" style="color:  rgb(63,81,181);">http://digiconsecure.com</a>\n                </p>\n            </div>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <ion-card-header style="background: rgb(42, 50, 165); color: white;">\n            For business co-operation\n        </ion-card-header>\n        <ion-card-content padding-top>\n            <span style="color:#f1bd13;font-size: 16px;">M Anshuman</span>\n            <br/>\n            <ion-icon ios="ios-call" md="md-call" style="font-size: 16px;color:#33cd5f;" (click)="dialSupportNumber()"></ion-icon>\n            &nbsp;&nbsp;\n            <a href="tel:+91-9534561100">\n                <span ion-text style="font-size: 16px">+91 9534561100</span>\n            </a>\n            <br/>\n\n            <ion-icon ios="ios-mail" md="md-mail" style="font-size: 16px;color:red;"></ion-icon>\n            &nbsp;&nbsp;\n            <a href="mailto:sales@digiconsecure.com">\n                <span ion-text style="font-size: 16px;">sales@digiconsecure.com</span>\n            </a>\n        </ion-card-content>\n    </ion-card>\n\n    <ion-card>\n        <ion-card-header style="background: brown; color: white;">\n            Customer support contacts\n        </ion-card-header>\n        <ion-card-content padding-top>\n\n            <ion-icon ios="ios-call" md="md-call" style="color:#33cd5f;" (click)="dialSupportNumber()"></ion-icon>\n            &nbsp;&nbsp;\n            <a href="tel:+91-9430420078">\n                <span ion-text style="font-size: 16px;">+91 9430420078</span>\n            </a>\n            <br/>\n\n            <ion-icon ios="ios-mail" md="md-mail" style="color:red;" (click)="dialSupportNumber()"></ion-icon>&nbsp;&nbsp;\n            <a href="mailto:sales@digiconsecure.com">\n                <span ion-text style="font-size: 16px;">sales@digiconsecure.com</span>\n            </a>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\about-us\about-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AboutUsPage);
    return AboutUsPage;
}());

//# sourceMappingURL=about-us.js.map

/***/ }),

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 189,
	"./af.js": 189,
	"./ar": 190,
	"./ar-dz": 191,
	"./ar-dz.js": 191,
	"./ar-kw": 192,
	"./ar-kw.js": 192,
	"./ar-ly": 193,
	"./ar-ly.js": 193,
	"./ar-ma": 194,
	"./ar-ma.js": 194,
	"./ar-sa": 195,
	"./ar-sa.js": 195,
	"./ar-tn": 196,
	"./ar-tn.js": 196,
	"./ar.js": 190,
	"./az": 197,
	"./az.js": 197,
	"./be": 198,
	"./be.js": 198,
	"./bg": 199,
	"./bg.js": 199,
	"./bm": 200,
	"./bm.js": 200,
	"./bn": 201,
	"./bn.js": 201,
	"./bo": 202,
	"./bo.js": 202,
	"./br": 203,
	"./br.js": 203,
	"./bs": 204,
	"./bs.js": 204,
	"./ca": 205,
	"./ca.js": 205,
	"./cs": 206,
	"./cs.js": 206,
	"./cv": 207,
	"./cv.js": 207,
	"./cy": 208,
	"./cy.js": 208,
	"./da": 209,
	"./da.js": 209,
	"./de": 210,
	"./de-at": 211,
	"./de-at.js": 211,
	"./de-ch": 212,
	"./de-ch.js": 212,
	"./de.js": 210,
	"./dv": 213,
	"./dv.js": 213,
	"./el": 214,
	"./el.js": 214,
	"./en-au": 215,
	"./en-au.js": 215,
	"./en-ca": 216,
	"./en-ca.js": 216,
	"./en-gb": 217,
	"./en-gb.js": 217,
	"./en-ie": 218,
	"./en-ie.js": 218,
	"./en-il": 219,
	"./en-il.js": 219,
	"./en-nz": 220,
	"./en-nz.js": 220,
	"./eo": 221,
	"./eo.js": 221,
	"./es": 222,
	"./es-do": 223,
	"./es-do.js": 223,
	"./es-us": 224,
	"./es-us.js": 224,
	"./es.js": 222,
	"./et": 225,
	"./et.js": 225,
	"./eu": 226,
	"./eu.js": 226,
	"./fa": 227,
	"./fa.js": 227,
	"./fi": 228,
	"./fi.js": 228,
	"./fo": 229,
	"./fo.js": 229,
	"./fr": 230,
	"./fr-ca": 231,
	"./fr-ca.js": 231,
	"./fr-ch": 232,
	"./fr-ch.js": 232,
	"./fr.js": 230,
	"./fy": 233,
	"./fy.js": 233,
	"./gd": 234,
	"./gd.js": 234,
	"./gl": 235,
	"./gl.js": 235,
	"./gom-latn": 236,
	"./gom-latn.js": 236,
	"./gu": 237,
	"./gu.js": 237,
	"./he": 238,
	"./he.js": 238,
	"./hi": 239,
	"./hi.js": 239,
	"./hr": 240,
	"./hr.js": 240,
	"./hu": 241,
	"./hu.js": 241,
	"./hy-am": 242,
	"./hy-am.js": 242,
	"./id": 243,
	"./id.js": 243,
	"./is": 244,
	"./is.js": 244,
	"./it": 245,
	"./it.js": 245,
	"./ja": 246,
	"./ja.js": 246,
	"./jv": 247,
	"./jv.js": 247,
	"./ka": 248,
	"./ka.js": 248,
	"./kk": 249,
	"./kk.js": 249,
	"./km": 250,
	"./km.js": 250,
	"./kn": 251,
	"./kn.js": 251,
	"./ko": 252,
	"./ko.js": 252,
	"./ky": 253,
	"./ky.js": 253,
	"./lb": 254,
	"./lb.js": 254,
	"./lo": 255,
	"./lo.js": 255,
	"./lt": 256,
	"./lt.js": 256,
	"./lv": 257,
	"./lv.js": 257,
	"./me": 258,
	"./me.js": 258,
	"./mi": 259,
	"./mi.js": 259,
	"./mk": 260,
	"./mk.js": 260,
	"./ml": 261,
	"./ml.js": 261,
	"./mn": 262,
	"./mn.js": 262,
	"./mr": 263,
	"./mr.js": 263,
	"./ms": 264,
	"./ms-my": 265,
	"./ms-my.js": 265,
	"./ms.js": 264,
	"./mt": 266,
	"./mt.js": 266,
	"./my": 267,
	"./my.js": 267,
	"./nb": 268,
	"./nb.js": 268,
	"./ne": 269,
	"./ne.js": 269,
	"./nl": 270,
	"./nl-be": 271,
	"./nl-be.js": 271,
	"./nl.js": 270,
	"./nn": 272,
	"./nn.js": 272,
	"./pa-in": 273,
	"./pa-in.js": 273,
	"./pl": 274,
	"./pl.js": 274,
	"./pt": 275,
	"./pt-br": 276,
	"./pt-br.js": 276,
	"./pt.js": 275,
	"./ro": 277,
	"./ro.js": 277,
	"./ru": 278,
	"./ru.js": 278,
	"./sd": 279,
	"./sd.js": 279,
	"./se": 280,
	"./se.js": 280,
	"./si": 281,
	"./si.js": 281,
	"./sk": 282,
	"./sk.js": 282,
	"./sl": 283,
	"./sl.js": 283,
	"./sq": 284,
	"./sq.js": 284,
	"./sr": 285,
	"./sr-cyrl": 286,
	"./sr-cyrl.js": 286,
	"./sr.js": 285,
	"./ss": 287,
	"./ss.js": 287,
	"./sv": 288,
	"./sv.js": 288,
	"./sw": 289,
	"./sw.js": 289,
	"./ta": 290,
	"./ta.js": 290,
	"./te": 291,
	"./te.js": 291,
	"./tet": 292,
	"./tet.js": 292,
	"./tg": 293,
	"./tg.js": 293,
	"./th": 294,
	"./th.js": 294,
	"./tl-ph": 295,
	"./tl-ph.js": 295,
	"./tlh": 296,
	"./tlh.js": 296,
	"./tr": 297,
	"./tr.js": 297,
	"./tzl": 298,
	"./tzl.js": 298,
	"./tzm": 299,
	"./tzm-latn": 300,
	"./tzm-latn.js": 300,
	"./tzm.js": 299,
	"./ug-cn": 301,
	"./ug-cn.js": 301,
	"./uk": 302,
	"./uk.js": 302,
	"./ur": 303,
	"./ur.js": 303,
	"./uz": 304,
	"./uz-latn": 305,
	"./uz-latn.js": 305,
	"./uz.js": 304,
	"./vi": 306,
	"./vi.js": 306,
	"./x-pseudo": 307,
	"./x-pseudo.js": 307,
	"./yo": 308,
	"./yo.js": 308,
	"./zh-cn": 309,
	"./zh-cn.js": 309,
	"./zh-hk": 310,
	"./zh-hk.js": 310,
	"./zh-tw": 311,
	"./zh-tw.js": 311
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 474;

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.link = "http://51.38.175.41/users/";
        console.log('Hello ApiServiceProvider Provider');
    }
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("http://51.38.175.41/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (_id, skip, limit, key) {
        return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&type=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_id, email) {
        return this.http.get('http://51.38.175.41/devices/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post("http://51.38.175.41/trackRoute", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        return this.http.post('http://51.38.175.41/trackRoute/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        return this.http.delete('http://51.38.175.41/trackRoute/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get("http://51.38.175.41/stoppage/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get("http://51.38.175.41/notifs/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        // http://51.38.175.41/notifs/overSpeedReport?from_date=2018-09-30T20:01:26.055Z&to_date=2018-10-01T12:01:26.055Z&_u=5a7009c9031fc508983b458a
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get("http://51.38.175.41/notifs/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get("http://51.38.175.41/notifs/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        return this.http.get("http://51.38.175.41/summary/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (email, _id, from, to, skip, limit) {
        // http://51.38.175.41/gps/getGpsReport?email=sanjay.diwedi@gmail.com&id=5b31e85d2b8fc936ea1cbff9&from=Mon%20Oct%2001%202018%2000:00:00%20GMT+0530%20(India%20Standard%20Time)&to=Mon%20Oct%2001%202018%2016:55:56%20GMT+0530%20(India%20Standard%20Time)&s=10&l=10
        return this.http.get("http://51.38.175.41/gps/getGpsReport?email=" + email + '&id=' + _id + '&from=' + from + '&to=' + to + '&s=' + skip + '&l=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        return this.http.post(this.link + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        return this.http.post('http://51.38.175.41/geofencing/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        return this.http.post('http://51.38.175.41/users/user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        return this.http.post('http://51.38.175.41/users/editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        return this.http.post('http://51.38.175.41/groups/addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        return this.http.post('http://51.38.175.41/devices/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        return this.http.get("http://51.38.175.41/geofencing/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        return this.http.get(this.link + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        return this.http.get(this.link + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.link + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post(this.link + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dashboardcall = function (email, from, to, _id) {
        return this.http.get('http://51.38.175.41/gps/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        return this.http.get('http://51.38.175.41/devices/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        return this.http.get("http://51.38.175.41/devices/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        return this.http.post("http://51.38.175.41/devices/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get('http://51.38.175.41/gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get('http://51.38.175.41/stoppage/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get('http://51.38.175.41/gps?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        return this.http.get("http://51.38.175.41/users/getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        return this.http.get("http://51.38.175.41/summary?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateInCall = function (devicedetail) {
        return this.http.post("http://51.38.175.41/devices/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        return this.http.get("http://51.38.175.41/devices/deleteDevice?did=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        return this.http.get("http://51.38.175.41/devices/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post("http://51.38.175.41/users/PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post("http://51.38.175.41/users/PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        return this.http.get("http://51.38.175.41/groups/deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addcustomerCall = function (devicedetails) {
        return this.http.post('http://51.38.175.41/users/signUp', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        return this.http.post("https://www.oneqlik.in/share/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post("https://www.oneqlik.in/share", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["LoadingController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 502:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddGeofencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddGeofencePage = /** @class */ (function () {
    function AddGeofencePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AddGeofencePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddGeofencePage');
    };
    AddGeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-geofence',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-geofence\add-geofence.html"*/'<!--\n  Generated template for the AddGeofencePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>AddGeofence</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\add-geofence\add-geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AddGeofencePage);
    return AddGeofencePage;
}());

//# sourceMappingURL=add-geofence.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(navCtrl, navParams, formBuilder, api, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.contact_data = {};
        this.contactusForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            // mobile: ['', Validators.required],
            // title: ['', Validators.required],
            note: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    ContactUsPage_1 = ContactUsPage;
    ContactUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactUsPage');
    };
    ContactUsPage.prototype.contactUs = function () {
        var _this = this;
        this.submitAttempt = true;
        console.log(this.contactusForm.value);
        if (this.contactusForm.valid) {
            this.contact_data = {
                "email": this.contactusForm.value.mail,
                // "title": this.contactusForm.value.title,
                "msg": this.contactusForm.value.note,
                // "phone": this.contactusForm.value.mobile,
                "dealerid": "rudragpstrack@gmail.com",
                "dealerName": "Sashi Prakash"
            };
            this.api.startLoading().present();
            this.api.contactusApi(this.contact_data)
                .subscribe(function (data) {
                _this.api.stopLoading();
                console.log(data.message);
                var toast = _this.toastCtrl.create({
                    message: 'Your message has been sent, we will get back to you soon..',
                    position: 'bottom',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.navCtrl.setRoot(ContactUsPage_1);
                });
                toast.present();
            }, function (error) {
                _this.api.stopLoading();
                console.log(error);
            });
        }
    };
    ContactUsPage = ContactUsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-us',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\contact-us\contact-us.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contact Us</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <form class="form" [formGroup]="contactusForm">\n    <p type="Name:">\n      <input formControlName="name" type="text" placeholder="Write your name here.." />\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.name.valid && (contactusForm.controls.name.dirty || submitAttempt)">Name is required and should be in valid format!</span>\n    <p type="Email:">\n      <input formControlName="mail" type="email" placeholder="Let us know how to contact you back.." />\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.mail.valid && (contactusForm.controls.mail.dirty || submitAttempt)">Email id is required and should be in valid format!</span>\n    <p type="Message:">\n      <textarea rows="4" cols="50" formControlName="note" placeholder="What would you like to tell us.."></textarea>\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.note.valid && (contactusForm.controls.note.dirty || submitAttempt)">please write your message!</span>\n    <button (tap)="contactUs()">Send Message</button>\n    <div class="div">\n      <!-- <p style="padding:0px">Support Details</p> -->\n      <ion-icon name="call"></ion-icon> +91-9430420078\n      <ion-icon name="mail"></ion-icon> sales@digiconsecure.com\n      <!-- <ion-icon name="call"></ion-icon><a href="tel:+91-9028045389" style="color: white">+91-9028045389</a> \n        <ion-icon name="mail"></ion-icon><a href="mailto:poonam.g@processfactory.in" style="color: white">poonam.g@processfactory.in</a>  -->\n    </div>\n  </form>\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\contact-us\contact-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ContactUsPage);
    return ContactUsPage;
    var ContactUsPage_1;
}());

//# sourceMappingURL=contact-us.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modals_add_device_modal__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modals_update_cust_update_cust__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modals_add_customer_modal_add_customer_modal__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as io from 'socket.io-client';






// import { CustomersPageModule } from './customers.module';
// declare var angular;
var CustomersPage = /** @class */ (function () {
    function CustomersPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.CustomerArraySearch = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
    }
    CustomersPage_1 = CustomersPage;
    CustomersPage.prototype.ngOnInit = function () {
        this.getcustomer();
    };
    CustomersPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getcustomer();
        refresher.complete();
        // setTimeout(() => {
        //   console.log('Async operation has ended');
        //   refresher.complete();
        // }, 2000);
    };
    CustomersPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.CustomerArray);
        var val = ev.target.value.trim();
        this.CustomerArraySearch = this.CustomerArray.filter(function (item) {
            return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.CustomerArraySearch);
    };
    CustomersPage.prototype.getcustomer = function () {
        var _this = this;
        console.log("getcustomer");
        var baseURLp = 'http://51.38.175.41/users/getCust?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.customerslist = data.reverse();
            _this.CustomerData = data;
            console.log("CustomerData=> " + JSON.stringify(_this.CustomerData));
            // console.log("customerlist=> ", this.customerslist)
            _this.CustomerArray = [];
            for (var i = 0; i < _this.CustomerData.length; i++) {
                _this.CratedeOn = JSON.stringify(_this.CustomerData[i].created_on).split('"')[1].split('T')[0];
                var gmtDateTime = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(_this.CustomerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
                var gmtDate = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(_this.CustomerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");
                _this.time = gmtDateTime.local().format(' h:mm:ss a');
                _this.date = gmtDate.format('DD/MM/YYYY');
                _this.CustomerArray.push({ '_id': _this.CustomerData[i]._id, 'first_name': _this.CustomerData[i].first_name, 'last_name': _this.CustomerData[i].last_name, 'email': _this.CustomerData[i].email, 'phone': _this.CustomerData[i].phone, 'date': _this.date, 'status': _this.CustomerData[i].status, 'pass': _this.CustomerData[i].pass, 'total_vehicle': _this.CustomerData[i].total_vehicle, 'userid': _this.CustomerData[i].userid, 'address': _this.CustomerData[i].address, 'dealer_firstname': _this.CustomerData[i].dealer_firstname });
                // this.CustomerArraySearch.push({ '_id': this.CustomerData[i]._id, 'first_name': this.CustomerData[i].first_name, 'last_name': this.CustomerData[i].last_name, 'email': this.CustomerData[i].email, 'phone': this.CustomerData[i].phone, 'date': this.date, 'status': this.CustomerData[i].status, 'pass': this.CustomerData[i].pass, 'total_vehicle': this.CustomerData[i].total_vehicle, 'userid': this.CustomerData[i].userid, 'address': this.CustomerData[i].address, 'dealer_firstname': this.CustomerData[i].dealer_firstname });
            }
            _this.CustomerArraySearch = _this.CustomerArray;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error found=> " + err);
        });
    };
    CustomersPage.prototype.CustomerStatus = function (Customersdeta) {
        var _this = this;
        console.log("status=> " + Customersdeta.status);
        console.log(Customersdeta._id);
        console.log(this.islogin._id);
        var msg;
        if (Customersdeta.status) {
            msg = 'Do you want to Deactivate this customer ?';
        }
        else {
            msg = 'Do you want to Activate this customer ?';
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        _this.user_status(Customersdeta);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        _this.getcustomer();
                    }
                }]
        });
        alert.present();
    };
    CustomersPage.prototype.user_status = function (Customersdeta) {
        var _this = this;
        var stat;
        if (Customersdeta.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var data = {
            "uId": Customersdeta._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            console.log("DeletedDevice=> " + _this.DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Customer was updated successfully',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getcustomer();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alerCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    CustomersPage.prototype.openupdateCustomersModal = function (Customersdetails) {
        var _this = this;
        console.log('Opening Modal openAdddeviceModal');
        this.customer = Customersdetails;
        // console.log(this.customer)
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modals_update_cust_update_cust__["a" /* UpdateCustModalPage */], {
            param: this.customer
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            // this.getcustomer();
            _this.navCtrl.setRoot(CustomersPage_1);
        });
        modal.present();
    };
    CustomersPage.prototype.openAdddeviceModal = function (Customersdetails) {
        var _this = this;
        console.log('Opening Modal openAdddeviceModal');
        this.customer = Customersdetails;
        console.log("customers=> ", this.customer);
        console.log(this.customer._id);
        console.log(this.customer.email);
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modals_add_device_modal__["a" /* AddDeviceModalPage */], { custDet: this.customer });
        profileModal.onDidDismiss(function (data) {
            console.log("vehData=> " + JSON.stringify(data));
            _this.getcustomer();
        });
        profileModal.present();
    };
    CustomersPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CustomersPage.prototype.openAddCustomerModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modals_add_customer_modal_add_customer_modal__["a" /* AddCustomerModal */]);
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getcustomer();
        });
        modal.present();
    };
    CustomersPage.prototype.switchUser = function (cust_id) {
        var _this = this;
        //debugger;
        console.log(cust_id);
        localStorage.setItem('isDealervalue', 'true');
        // $rootScope.dealer = $rootScope.islogin;
        localStorage.setItem('dealer', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');
        this.apiCall.getcustToken(cust_id)
            .subscribe(function (res) {
            console.log('UserChangeObj=>', res);
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            // console.log('token=>', logindata);
            var details = JSON.parse(userDetails);
            // console.log(details.isDealer);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            console.log("temp=> ", temp);
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
        }, function (err) {
            console.log(err);
        });
    };
    CustomersPage = CustomersPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\customers.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Customers</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openAddCustomerModal()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar placeholder="Search..." (ionInput)="getItems($event)"></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <div *ngFor="let item of CustomerArraySearch">\n\n\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n        </ion-thumbnail>\n\n        <div (tap)="switchUser(item._id)">\n\n          <p>\n\n            <span ion-text color="dark">Name: </span> {{item.first_name}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Email: </span> {{item.email}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Phone: </span>{{item.phone}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Password: </span>{{item.pass}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Created On: </span>{{item.date}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Total Vehicles: </span>\n\n            <span ion-text color="danger">{{item.total_vehicle}}</span>\n\n          </p>\n\n        </div>\n\n\n\n\n\n        <p>\n\n          <button ion-button small (click)="openupdateCustomersModal(item)">Edit</button>\n\n          <button ion-button small (click)="openAdddeviceModal(item)">Add Vehicle</button>\n\n          <button ion-button small (click)="CustomerStatus(item)" *ngIf="item.status == true">Active</button>\n\n          <button ion-button small color="danger" (click)="CustomerStatus(item)" *ngIf="item.status != true">InActive</button>\n\n        </p>\n\n      </ion-item>\n\n\n\n    </div>\n\n  </ion-list>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\customers\customers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], CustomersPage);
    return CustomersPage;
    var CustomersPage_1;
}());

//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 553:
/***/ (function(module, exports) {

/**
 * [chartjs-plugin-labels]{@link https://github.com/emn178/chartjs-plugin-labels}
 *
 * @version 1.0.0
 * @author Chen, Yi-Cyuan [emn178@gmail.com]
 * @copyright Chen, Yi-Cyuan 2017-2018
 * @license MIT
 */
(function(){function f(){this.renderToDataset=this.renderToDataset.bind(this)}"undefined"===typeof Chart?console.error("Can not find Chart object."):(f.prototype.setup=function(a,c,b){this.chart=a;this.elements=c.meta.data;this.ctx=a.ctx;c=a.config.options;this.options=Object.assign({position:"default",precision:0,fontSize:c.defaultFontSize,fontColor:c.defaultFontColor,fontStyle:c.defaultFontStyle,fontFamily:c.defaultFontFamily,shadowOffsetX:3,shadowOffsetY:3,shadowColor:"rgba(0,0,0,0.3)",shadowBlur:6,
images:[],outsidePadding:2,textMargin:2,overlap:!0},b);"outside"===this.options.position&&(b=1.5*this.options.fontSize+this.options.outsidePadding,a.chartArea.top+=b,a.chartArea.bottom-=b)},f.prototype.render=function(){this.labelBounds=[];this.chart.data.datasets.forEach(this.renderToDataset)},f.prototype.renderToDataset=function(a){this.totalPercentage=0;this.total=null;this.elements.forEach(function(c,b){this.renderToElement(a,c,b)}.bind(this))},f.prototype.renderToElement=function(a,c,b){if(this.shouldRenderToElement(c._view)){this.value=
null;var d=this.getLabel(a,c,b);if(d){var e=this.getRenderInfo(c,d);if(this.drawable(c,d,e)){var g=this.ctx;g.save();g.beginPath();g.font=Chart.helpers.fontString(this.options.fontSize,this.options.fontStyle,this.options.fontFamily);g.fillStyle=this.getFontColor(a,c,b);this.renderLabel(d,e);g.restore()}}}},f.prototype.renderLabel=function(a,c){return this.options.arc?this.renderArcLabel(a,c):this.renderBaseLabel(a,c)},f.prototype.renderBaseLabel=function(a,c){var b=this.ctx;if("object"===typeof a)b.drawImage(a,
c.x-a.width/2,c.y-a.height/2,a.width,a.height);else{b.save();b.textBaseline="top";b.textAlign="center";this.options.textShadow&&(b.shadowOffsetX=this.options.shadowOffsetX,b.shadowOffsetY=this.options.shadowOffsetY,b.shadowColor=this.options.shadowColor,b.shadowBlur=this.options.shadowBlur);for(var d=a.split("\n"),e=0;e<d.length;e++)b.fillText(d[e],c.x,c.y-this.options.fontSize/2*d.length+this.options.fontSize*e);b.restore()}},f.prototype.renderArcLabel=function(a,c){var b=this.ctx,d=c.radius,e=c.view;
b.save();b.translate(e.x,e.y);if("string"===typeof a){b.rotate(c.startAngle);b.textBaseline="middle";b.textAlign="left";e=a.split("\n");var g=0,f=[],k=0;"border"===this.options.position&&(k=(e.length-1)*this.options.fontSize/2);for(var h=0;h<e.length;++h){var l=b.measureText(e[h]);l.width>g&&(g=l.width);f.push(l.width)}for(h=0;h<e.length;++h){var m=e[h],q=(e.length-1-h)*-this.options.fontSize+k;b.save();b.rotate((g-f[h])/2/d);for(var n=0;n<m.length;n++){var p=m.charAt(n);l=b.measureText(p);b.save();
b.translate(0,-1*d);b.fillText(p,0,q);b.restore();b.rotate(l.width/d)}b.restore()}}else b.rotate((e.startAngle+Math.PI/2+c.endAngle)/2),b.translate(0,-1*d),this.renderLabel(a,{x:0,y:0});b.restore()},f.prototype.shouldRenderToElement=function(a){return this.options.showZero||"polarArea"===this.chart.config.type?0!==a.outerRadius:0!==a.circumference},f.prototype.getLabel=function(a,c,b){if("function"===typeof this.options.render)a=this.options.render({label:this.chart.config.data.labels[b],value:a.data[b],
value:this.getPercentage(a,c,b),dataset:a,index:b});else switch(this.options.render){case "value":a=a.data[b];break;case "label":a=this.chart.config.data.labels[b];break;case "image":a=this.options.images[b]?this.loadImage(this.options.images[b]):"";break;default:a=this.getPercentage(a,c,b)+"%"}"object"===typeof a?a=this.loadImage(a):null!==a&&void 0!==a&&(a=a.toString());return a},f.prototype.getFontColor=function(a,c,b){var d=this.options.fontColor;"function"===typeof d?d=d({label:this.chart.config.data.labels[b],
value:a.data[b],value:this.getPercentage(a,c,b),backgroundColor:a.backgroundColor[b],dataset:a,index:b}):"string"!==typeof d&&(d=d[b]||this.chart.config.options.defaultFontColor);return d},f.prototype.getPercentage=function(a,c,b){if(null!==this.value)return this.value;if("polarArea"===this.chart.config.type){if(null===this.total)for(c=this.total=0;c<a.data.length;++c)this.total+=a.data[c];a=a.data[b]/this.total*100}else a=c._view.circumference/this.chart.config.options.circumference*
100;a=parseFloat(a.toFixed(this.options.precision));this.options.showActualPercentages||(this.totalPercentage+=a,100<this.totalPercentage&&(a-=this.totalPercentage-100,a=parseFloat(a.toFixed(this.options.precision))));return this.value=a},f.prototype.getRenderInfo=function(a,c){return this.options.arc?this.getArcRenderInfo(a,c):this.getBaseRenderInfo(a,c)},f.prototype.getBaseRenderInfo=function(a,c){if("outside"===this.options.position||"border"===this.options.position){var b,d=a._view,e=d.startAngle+
(d.endAngle-d.startAngle)/2,g=d.outerRadius/2;"border"===this.options.position?b=(d.outerRadius-g)/2+g:"outside"===this.options.position&&(b=d.outerRadius-g+g+this.options.textMargin);b={x:d.x+Math.cos(e)*b,y:d.y+Math.sin(e)*b};"outside"===this.options.position&&(e=this.options.textMargin+this.measureLabel(c).width/2,b.x+=b.x<d.x?-e:e);return b}return a.tooltipPosition()},f.prototype.getArcRenderInfo=function(a,c){var b=a._view;var d="outside"===this.options.position?b.outerRadius+this.options.fontSize+
this.options.textMargin:"border"===this.options.position?(b.outerRadius/2+b.outerRadius)/2:(b.innerRadius+b.outerRadius)/2;var e=b.startAngle,g=b.endAngle,f=g-e;e+=Math.PI/2;g+=Math.PI/2;var k=this.measureLabel(c);e+=(g-(k.width/d+e))/2;return{radius:d,startAngle:e,endAngle:g,totalAngle:f,view:b}},f.prototype.drawable=function(a,c,b){if(this.options.overlap)return!0;if(this.options.arc)return b.endAngle-b.startAngle<=b.totalAngle;var d=this.measureLabel(c);c=b.x-d.width/2;var e=b.x+d.width/2,g=b.y-
d.height/2;b=b.y+d.height/2;return"outside"===this.options.renderInfo?this.outsideInRange(c,e,g,b):a.inRange(c,g)&&a.inRange(c,b)&&a.inRange(e,g)&&a.inRange(e,b)},f.prototype.outsideInRange=function(a,c,b,d){for(var e=this.labelBounds,g=0;g<e.length;++g){for(var f=e[g],k=[[a,b],[a,d],[c,b],[c,d]],h=0;h<k.length;++h){var l=k[h][0],m=k[h][1];if(l>=f.left&&l<=f.right&&m>=f.top&&m<=f.bottom)return!1}k=[[f.left,f.top],[f.left,f.bottom],[f.right,f.top],[f.right,f.bottom]];for(h=0;h<k.length;++h)if(l=k[h][0],
m=k[h][1],l>=a&&l<=c&&m>=b&&m<=d)return!1}e.push({left:a,right:c,top:b,bottom:d});return!0},f.prototype.measureLabel=function(a){if("object"===typeof a)return{width:a.width,height:a.height};var c=0;a=a.split("\n");for(var b=0;b<a.length;++b){var d=this.ctx.measureText(a[b]);d.width>c&&(c=d.width)}return{width:c,height:this.options.fontSize*a.length}},f.prototype.loadImage=function(a){var c=new Image;c.src=a.src;c.width=a.width;c.height=a.height;return c},Chart.plugins.register({id:"labels",afterDatasetUpdate:function(a,
c,b){Array.isArray(b)||(b=[b]);var d=b.length;a._labels&&d===a._labels.length||(a._labels=b.map(function(){return new f}));for(var e=0;e<d;++e)a._labels[e].setup(a,c,b[e])},afterDatasetsDraw:function(a){a._labels.forEach(function(a){a.render()})}}))})();


/***/ }),

/***/ 554:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DailyReportPage = /** @class */ (function () {
    function DailyReportPage(navCtrl, navParams, apicalldaily) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaily = apicalldaily;
        this.deviceReportSearch = [];
        this.page = 0;
        this.limit = 6;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin.email);
        console.log("id => " + this.islogin._id);
        // this.datetime = new Date().setHours(0, 0, 0, 0)
        // console.log(this.datetime);
        // this.from = new Date(new Date().setHours(0,0,0,0)).toISOString();
        //  console.log(this.from );
        // // this.todaytime = this.todaydate.toLocaleString();
        // // console.log(this.todaytime.toLocaleString());
        // this.to = new Date().toISOString()
        // console.log("from=> " +this.from);
        // var temp = new Date();
        // // this.datetimeStart = temp.toISOString();
        // var settime = temp.getTime();
        // // this.datetime=new Date(settime).setMinutes(0);
        // this.datetime = new Date(settime).setHours(5, 30, 0);
        // this.from = new Date(this.datetime).toISOString();
        // var a = new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes() + 30);
        // this.to = new Date(a).toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        //var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        //var datetime = new Date(settime).setHours(5, 30, 0);
        this.from = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.from);
        // var a = new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes() + 30);
        this.to = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('start date', this.to);
    }
    DailyReportPage.prototype.ngOnInit = function () {
        this.getDailyReportData();
    };
    DailyReportPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.deviceReport);
        var val = ev.target.value.trim();
        this.deviceReportSearch = this.deviceReport.filter(function (item) {
            return (item.device.Device_ID.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.deviceReportSearch);
    };
    DailyReportPage.prototype.getDailyReportData = function () {
        var _this = this;
        this.page = 0;
        //  console.log("chengeddate=> ", chengeddate)
        // var chengeddate = this.from;
        console.log("from date=> ", this.from);
        console.log("to date=> ", this.to);
        this.apicalldaily.startLoading().present();
        this.apicalldaily.getDailyReport(this.islogin.email, this.islogin._id, new Date(this.from).toISOString(), new Date(this.to).toISOString(), this.page, this.limit)
            .subscribe(function (data) {
            _this.apicalldaily.stopLoading();
            // this.deviceReport = data.data;
            _this.deviceReport = data;
            _this.deviceReportSearch = data;
            console.log("daily report data=> " + JSON.stringify(_this.deviceReport.data));
        }, function (error) {
            _this.apicalldaily.stopLoading();
            console.log("error in service=> " + error);
        });
    };
    DailyReportPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            // that.apicalldaily.startLoading().present();
            that.apicalldaily.getDailyReport(that.islogin.email, that.islogin._id, that.from, that.to, that.page, that.limit)
                .subscribe(function (res) {
                // that.apicalldaily.stopLoading();
                // for (let i = 0; i < res.data.length; i++) {
                //   that.deviceReport.push(res.data[i]);
                //   that.deviceReportSearch.push(res.data[i]);
                // }
                for (var i = 0; i < res.length; i++) {
                    that.deviceReport.push(res[i]);
                    // that.deviceReportSearch.push(res[i]);
                }
                that.deviceReportSearch = that.deviceReport;
            }, function (error) {
                // that.apicalldaily.stopLoading();
                console.log(error);
            });
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    DailyReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-daily-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\daily-report\daily-report.html"*/'<ion-header color="gpsc">\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Daily Report</ion-title>\n\n\n\n        <ion-buttons end>\n\n            <ion-datetime class="dateStyle" displayFormat="DD-MM-YYYY " pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="from" (ionChange)="getDailyReportData()"></ion-datetime>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-searchbar placeholder="Search..." (ionInput)="getItems($event)"></ion-searchbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-card *ngFor="let item of deviceReportSearch">\n\n\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-avatar item-start>\n\n                <!-- <img src="assets/imgs/car_img1.png" > -->\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-avatar>\n\n\n\n            <ion-avatar item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-avatar>\n\n\n\n            <ion-avatar item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n\n\n            </ion-avatar>\n\n            <p style="color:black; font-size:16px; padding-left: 4px;">{{item.device.Device_Name }}</p>\n\n            <ion-row style="margin-top:6%;">\n\n\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;">Running</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.tripCount">{{item.tripCount}}</span>\n\n                        <span *ngIf="!item.tripCount">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Trips</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="margin-top: 2%;">\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Distance(Km)</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.avgSpeed">{{item.avgSpeed | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.avgSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;    margin-left:0%;font-size: 11px;font-weight: 350;">Avg.Speed</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.maxSpeed">{{item.maxSpeed}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.maxSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Max Speed</p>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n        </ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\daily-report\daily-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DailyReportPage);
    return DailyReportPage;
}());

//# sourceMappingURL=daily-report.js.map

/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSummaryRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DeviceSummaryRepoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DeviceSummaryRepoPage = /** @class */ (function () {
    function DeviceSummaryRepoPage(navCtrl, navParams, apicallsummary) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallsummary = apicallsummary;
        this.summaryReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        // var temp = new Date();
        // // this.datetimeStart = temp.toISOString();
        // var settime= temp.getTime();
        // // this.datetime=new Date(settime).setMinutes(0);
        // this.datetime=new Date(settime).setHours(5,30,0);
        //  this.datetimeStart =new Date(this.datetime).toISOString();
        //  var a= new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes()+30);
        // this.datetimeEnd = new Date(a).toISOString();
        var temp = new Date();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('start date', this.datetimeEnd);
    }
    DeviceSummaryRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DeviceSummaryRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    DeviceSummaryRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    DeviceSummaryRepoPage.prototype.getSummaarydevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.device_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    DeviceSummaryRepoPage.prototype.getdevices = function () {
        var _this = this;
        this.apicallsummary.startLoading().present();
        this.apicallsummary.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallsummary.stopLoading();
            console.log(err);
        });
    };
    DeviceSummaryRepoPage.prototype.getSummaryReport = function () {
        var _this = this;
        this.summaryReportData = [];
        var outerthis = this;
        if (this.device_id == undefined) {
            this.device_id = "";
        }
        console.log("from date=> ", this.datetimeStart);
        console.log("to date=> ", this.datetimeEnd);
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getSummaryReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.summaryReport = data;
            console.log(_this.summaryReport);
            var i = 0, howManyTimes = _this.summaryReport.length;
            function f() {
                outerthis.summaryReportData.push({ 'avgSpeed': outerthis.summaryReport[i].avgSpeed, 'Device_Name': outerthis.summaryReport[i].device.Device_Name, 'routeViolations': outerthis.summaryReport[i].routeViolations, 'overspeeds': outerthis.summaryReport[i].overspeeds, 'ignOn': outerthis.summaryReport[i].ignOn, 'ignOff': outerthis.summaryReport[i].ignOff, 'distance': outerthis.summaryReport[i].distance, 'tripCount': outerthis.summaryReport[i].tripCount });
                // outerthis.summaryReportData.push({ 'distance': outerthis.summaryReport[i].distance, 'Device_Name': outerthis.summaryReport[i].device.Device_Name });
                if (outerthis.summaryReport[i].endPoint != null && outerthis.summaryReport[i].startPoint != null) {
                    var latEnd = outerthis.summaryReport[i].endPoint[0];
                    var lngEnd = outerthis.summaryReport[i].endPoint[1];
                    var latlng = new google.maps.LatLng(latEnd, lngEnd);
                    var latStart = outerthis.summaryReport[i].startPoint[0];
                    var lngStart = outerthis.summaryReport[i].startPoint[1];
                    var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                    var geocoder = new google.maps.Geocoder();
                    var request = {
                        latLng: latlng
                    };
                    var request1 = {
                        latLng: lngStart1
                    };
                    geocoder.geocode(request, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("End address is: " + data[1].formatted_address);
                                outerthis.locationEndAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                    });
                    geocoder.geocode(request1, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("Start address is: " + data[1].formatted_address);
                                outerthis.locationAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = outerthis.locationAddress;
                    });
                }
                console.log(outerthis.summaryReportData);
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 5000);
                }
            }
            f();
        }, function (error) {
            _this.apicallsummary.stopLoading();
            console.log(error);
        });
    };
    DeviceSummaryRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-summary-repo',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\device-summary-repo\device-summary-repo.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    <ion-title>Summary Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n      <ion-label>Select Vehicle</ion-label>\n\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n        [canSearch]="true" (onChange)="getSummaarydevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n      </select-searchable>\n\n    </ion-item>\n\n\n\n    <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n        <ion-col width-20>\n\n          <ion-label ><span style="font-size: 13px">From Date</span>\n\n            <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n              style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n          </ion-label>\n\n        </ion-col>\n\n    \n\n        <ion-col width-20>\n\n          <ion-label ><span style="font-size: 13px">To Date</span>\n\n            <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n              style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n          </ion-label>\n\n        </ion-col>\n\n    \n\n        <ion-col width-20>\n\n          <div style="margin-top: 9px; float: right">\n\n            <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSummaryReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n    <!-- <ion-card *ngFor="let item of summaryReportData">\n\n\n\n        <ion-row>\n\n            <ion-col col-8>\n\n           <p style="font-size:16px;">{{item.Device_Name}}</p>\n\n           </ion-col>\n\n  \n\n            <ion-col col-4>\n\n           <p style="text-align:left;font-size: 14px;">\n\n            <span style="color:gray;">Trips</span>&nbsp;&nbsp;<span style="color: #fd39e3;">{{item.tripCount}}</span></p>\n\n         </ion-col>\n\n        \n\n         \n\n            </ion-row>\n\n        <br/>\n\n        <ion-row>\n\n            <ion-col style="margin-top:-5%;">\n\n           \n\n    \n\n     \n\n        <ion-row style="padding:0px!important;" style="padding:0px!important;" >\n\n           <p style="text-align:left;font-size: 14px;"><span style="color:#009688;  font-size: 16px;font-weight: 450;">\n\n              \n\n              <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:15px;"></ion-icon>\n\n              &nbsp;</span>&nbsp;&nbsp;<span *ngIf="item.StartLocation"></span>\n\n              <span *ngIf="!item.StartLocation">&nbsp;&nbsp;&nbsp;&nbsp;N/A</span\n\n                ><span style="color:gray;font-size:12px">{{item.StartLocation}}</span></p>\n\n        </ion-row >\n\n  \n\n        <ion-row style="padding:0px!important;" style="padding:0px!important;" >\n\n            <p style="text-align:left;font-size: 14px;"><span style="color:#009688;  font-size: 16px;font-weight: 450;">\n\n                <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n                &nbsp;</span>&nbsp;&nbsp;<span *ngIf="item.EndLocation"></span>\n\n                <span *ngIf="!item.EndLocation">&nbsp;&nbsp;&nbsp;&nbsp;N/A</span>\n\n                <span style="color:gray;font-size:12px">{{item.EndLocation}}</span></p>\n\n        </ion-row>\n\n        \n\n       </ion-col>\n\n       \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                    <span *ngIf="!item.ignOn">0.0</span>&nbsp;</p>\n\n                <p style="color:#53ab53;margin-left:12%;font-size:11px;font-weight: 450;">Total Running Time</p>\n\n            </ion-col>\n\n            <ion-col>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                    <span *ngIf="!item.ignOff">0.0</span>&nbsp;</p>\n\n                <p style="color:#5dd17f;text-align:center;font-size: 11px;color:red;font-weight: 450;">Total Idle/Stop Time</p>\n\n            </ion-col>\n\n            <ion-col center text-center>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.distance">{{item.distance  | number : \'1.0-2\'}}</span>\n\n                    <span *ngIf="!item.distance">0.0</span>&nbsp;</p>\n\n                <p style="text-align:center;font-size: 11px;color:#11c1f3;font-weight: 450;">Total KM</p>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.overspeeds">{{item.overspeeds}}</span>\n\n                    <span *ngIf="!item.overspeeds">0.0</span>&nbsp;</p>\n\n                <p style="color:#009688;margin-left:14%;font-size: 11px;font-weight: 450;">Overspeeding</p>\n\n            </ion-col>\n\n            <ion-col>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.routeViolations">{{item.routeViolations }}</span>\n\n                    <span *ngIf="!item.routeViolations">0.0</span>&nbsp;</p>\n\n                <p style="color:#009688;    margin-left:14%;font-size: 11px;font-weight: 450;">Route Voilation</p>\n\n            </ion-col>\n\n            <ion-col center text-center>\n\n                <p style="color:gray;font-size:11px; margin-top: -9px; text-align:center;font-weight: 400;">\n\n                    <span *ngIf="item.avgSpeed">{{item.avgSpeed  | number : \'1.0-2\'}}</span>\n\n                    <span *ngIf="!item.avgSpeed">0.0</span>&nbsp;</p>\n\n                <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 450;">Avg Speed</p>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-card> -->\n\n\n\n    <ion-list>\n\n            <ion-card *ngFor="let item of summaryReportData">\n\n    \n\n                <ion-item style="border-bottom: 2px solid #dedede;">\n\n                        <ion-thumbnail item-start>\n\n                     <img src="assets/imgs/car_red_icon.png" >\n\n                     </ion-thumbnail>\n\n                    <!-- <ion-thumbnail item-start>\n\n                       \n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                                    \n\n                        <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                    </ion-thumbnail>\n\n    \n\n                    <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                    </ion-thumbnail>\n\n    \n\n                    <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n    \n\n                    </ion-thumbnail> -->\n\n                    <p style="margin-left: 6px;color:black;font-size:16px;">{{item.Device_Name }}</p>\n\n                    \n\n                    <ion-row style="margin-top:6%;">\n\n    \n\n                        <ion-col>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                                <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                                <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                            <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;"> Running </p>\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                                <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                                <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                            <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                        </ion-col>\n\n                        <ion-col center text-center>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                                <span *ngIf="item.distance">{{item.distance  | number : \'1.0-2\'}}</span>\n\n                                <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                            <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Total KM</p>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <ion-row style="margin-top: 2%;">\n\n                        <ion-col>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                                <span *ngIf="item.overspeeds">{{item.overspeeds}}</span>\n\n                                <span *ngIf="!item.overspeeds">00.00</span>&nbsp;</p>\n\n                            <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Overspeeding</p>\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                                <span *ngIf="item.routeViolations">{{item.routeViolations}}&nbsp;Km/h</span>\n\n                                <span *ngIf="!item.routeViolations">00.00</span>&nbsp;</p>\n\n                            <p style="color:#009688;    margin-left:0%;font-size: 11px;font-weight: 350;">Route Voilation</p>\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                                <span *ngIf="item.avgSpeed">{{item.avgSpeed  | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                                <span *ngIf="!item.avgSpeed" >00.00</span>&nbsp;</p>\n\n                            <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Avg Speed</p>\n\n                        </ion-col>\n\n                    </ion-row>\n\n\n\n                    <ion-row>\n\n      \n\n                            <p style="font-size: 14px;">\n\n                              <span >\n\n                                <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:17px;margin-left:5px;\n\n                                "></ion-icon>&nbsp;</span>\n\n                              <span *ngIf="item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n                              <span *ngIf="!item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n                              <span style="color:gray;font-size:11px;font-weight: 400;"  text-nowrap >{{item.StartLocation}}</span>\n\n                            </p>\n\n            \n\n                        </ion-row>\n\n                    <ion-row>\n\n      \n\n                            <p style="font-size: 14px;">\n\n                              <span >\n\n                                <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:17px;margin-left:5px;\n\n                                "></ion-icon>&nbsp;</span>\n\n                              <span *ngIf="item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n                              <span *ngIf="!item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n                              <span style="color:gray;font-size:11px;font-weight: 400;"  text-nowrap >{{item.EndLocation}}</span>\n\n                            </p>\n\n            \n\n                        </ion-row>\n\n    \n\n                </ion-item>\n\n    \n\n            </ion-card>\n\n        </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\device-summary-repo\device-summary-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DeviceSummaryRepoPage);
    return DeviceSummaryRepoPage;
}());

//# sourceMappingURL=device-summary-repo.js.map

/***/ }),

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistanceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DistanceReportPage = /** @class */ (function () {
    function DistanceReportPage(navCtrl, navParams, apicallDistance) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallDistance = apicallDistance;
        this.distanceReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        // var temp = new Date();
        // // this.datetimeStart = temp.toISOString();
        // var settime= temp.getTime();
        // // this.datetime=new Date(settime).setMinutes(0);
        // this.datetime=new Date(settime).setHours(5,30,0);
        //  this.datetimeStart =new Date(this.datetime).toISOString();
        //  var a= new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes()+30);
        // this.datetimeEnd = new Date(a).toISOString();
        var temp = new Date();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('start date', this.datetimeEnd);
    }
    DistanceReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DistanceReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    DistanceReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    DistanceReportPage.prototype.getdevices = function () {
        var _this = this;
        this.apicallDistance.startLoading().present();
        this.apicallDistance.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallDistance.stopLoading();
            console.log(err);
        });
    };
    DistanceReportPage.prototype.getdistancedevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    DistanceReportPage.prototype.getDistanceReport = function (starttime, endtime) {
        var _this = this;
        var outerthis = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicallDistance.startLoading().present();
        this.apicallDistance.getDistanceReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.islogin._id, this.Ignitiondevice_id)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.distanceReport = data;
            console.log(_this.distanceReport);
            var i = 0, howManyTimes = _this.distanceReport.length;
            function f() {
                outerthis.distanceReportData.push({ 'distance': outerthis.distanceReport[i].distance, 'Device_Name': outerthis.distanceReport[i].device.Device_Name });
                if (outerthis.distanceReport[i].endPoint != null && outerthis.distanceReport[i].startPoint != null) {
                    var latEnd = outerthis.distanceReport[i].endPoint[0];
                    var lngEnd = outerthis.distanceReport[i].endPoint[1];
                    var latlng = new google.maps.LatLng(latEnd, lngEnd);
                    var latStart = outerthis.distanceReport[i].startPoint[0];
                    var lngStart = outerthis.distanceReport[i].startPoint[1];
                    var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                    var geocoder = new google.maps.Geocoder();
                    var request = {
                        latLng: latlng
                    };
                    var request1 = {
                        latLng: lngStart1
                    };
                    geocoder.geocode(request, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("End address is: " + data[1].formatted_address);
                                outerthis.locationEndAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                    });
                    geocoder.geocode(request1, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("Start address is: " + data[1].formatted_address);
                                outerthis.locationAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = outerthis.locationAddress;
                    });
                }
                console.log(outerthis.distanceReportData);
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 100);
                }
            }
            f();
        }, function (error) {
            _this.apicallDistance.stopLoading();
            console.log(error);
        });
    };
    DistanceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-distance-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\distance-report\distance-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Distance Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getdistancedevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right;">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getDistanceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card *ngFor="let item of distanceReportData">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n      </ion-thumbnail>\n\n\n\n      <ion-row style="padding-left:4px">\n\n        <ion-col col-8>\n\n          <p style="color:black;font-size:16px;">{{item.Device_Name }}</p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <p style="font-size: 12px;color:#e14444;" ion-text text-right>&nbsp;&nbsp; {{item.distance}}&nbsp;KM\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <br/>\n\n      <ion-row>\n\n        <p>\n\n          <span>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:17px;margin-left:5px;"></ion-icon>&nbsp;</span>\n\n          <span *ngIf="item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n          <span *ngIf="!item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n          <span style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.StartLocation}}</span>\n\n        </p>\n\n\n\n      </ion-row>\n\n      <ion-row>\n\n\n\n        <p style="font-size: 14px;">\n\n          <span>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:17px;margin-left:5px;\n\n                          "></ion-icon>&nbsp;</span>\n\n          <span *ngIf="item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n          <span *ngIf="!item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n          <span style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.EndLocation}}</span>\n\n        </p>\n\n\n\n      </ion-row>\n\n\n\n    </ion-item>\n\n\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\distance-report\distance-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DistanceReportPage);
    return DistanceReportPage;
}());

//# sourceMappingURL=distance-report.js.map

/***/ }),

/***/ 557:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FeedbackPage = /** @class */ (function () {
    function FeedbackPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FeedbackPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedbackPage');
    };
    FeedbackPage.prototype.rateusSubmit = function (e, rate) {
        console.log(e);
        console.log(rate);
    };
    FeedbackPage.prototype.submitRate = function () {
    };
    FeedbackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-feedback',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\feedback\feedback.html"*/'<ion-header>\n\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Rate Us</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<!-- <div class="modaldiv"> -->\n    <h6 ion-text color="gpsc" style="padding-left:12px;">Rating for the application</h6>\n    <br/>\n    <rating [(ngModel)]="rate" readOnly="false" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star"\n        nullable="false" (ngModelChange)="rateusSubmit($event, rate)">\n    </rating>\n    <ion-item style="padding-left: 0px">\n        <ion-textarea [(ngModel)]="note" name="note" autocomplete="on" autocorrect="on" placeholder="Add Reviews..."></ion-textarea>\n    </ion-item>\n\n    <button ion-button block color="gpsc" (tap)="submitRate()">SUBMIT REVIEW</button>\n<!-- </div> -->\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\feedback\feedback.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FeedbackPage);
    return FeedbackPage;
}());

//# sourceMappingURL=feedback.js.map

/***/ }),

/***/ 560:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FuelReportPage = /** @class */ (function () {
    function FuelReportPage(navCtrl, navParams, apicallFuel, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallFuel = apicallFuel;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    FuelReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    FuelReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    FuelReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelReportPage.prototype.getdevices = function () {
        var _this = this;
        this.apicallFuel.startLoading().present();
        this.apicallFuel.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicallFuel.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallFuel.stopLoading();
            console.log(err);
        });
    };
    FuelReportPage.prototype.getFueldevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    FuelReportPage.prototype.getFuelReport = function (starttime, endtime) {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicallFuel.startLoading().present();
        this.apicallFuel.getFuelApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallFuel.stopLoading();
            _this.fuelReport = data;
            console.log(_this.fuelReport);
            if (_this.fuelReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallFuel.stopLoading();
            console.log(error);
        });
    };
    FuelReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\fuel-report\fuel-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Fuel Report</ion-title>\n\n  </ion-navbar>\n\n\n\n\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getFueldevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getFuelReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <!-- <ion-row padding-left color="primary">\n\n    <ion-col width-20>\n\n\n\n      <ion-label style="font-size: 12px">From Date\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label style="font-size: 12px">To Date\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px;">\n\n        <ion-icon ios="ios-search" md="md-search" style="text-align: right; font-size: 20px;margin-top: 7px;" (click)="getFuelReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n  <hr> -->\n\n  <!-- <ion-list>\n\n\n\n    <ion-card *ngFor="let fuelfilldata of   fuelReport">\n\n\n\n      <ion-row>\n\n        <ion-col col-4>\n\n          <div>\n\n            <p style="margin-top:0%;">\n\n              <span style="text-align:left;color:gray;font-size:16px;">{{fuelfilldata.vehicleName}}</span>\n\n\n\n             \n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n          <div>\n\n            <p style="margin-top:0%;">\n\n              <span>\n\n                <ion-icon ios="ios-time" md="md-time"></ion-icon>&nbsp;</span>\n\n              <span style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">{{fuelfilldata.timestamp | date: \'short\'}}</span>\n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <div style="margin-top:0%;">\n\n\n\n            <div class="row" style="margin-top:-11px;text-align:center;">\n\n              <img src="assets/imgs/fuel.png" style="height: 19px;width: 17px;margin-top:9px;">&nbsp;\n\n              <span *ngIf="fuelfilldata.litres"></span>\n\n              <span *ngIf="!fuelfilldata.litres" style="font-size:11px;">&nbsp;N/A</span>\n\n              <span style="font-size:11px;color:#d40e0e;margin-top:12px;white-space:normal;">{{fuelfilldata.litres}}&nbsp;L </span>\n\n            </div>\n\n\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-card-header style="padding:0px;">\n\n            <ion-row>\n\n              <ion-col>\n\n                <p style="text-align:left;font-size: 14px;">\n\n                  <span style="color:#009688;  font-size: 16px;font-weight: 450;">\n\n                    <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444"></ion-icon>&nbsp;</span>&nbsp;&nbsp;\n\n                  <span *ngIf="fuelfilldata.address"></span>\n\n                  <span *ngIf="!fuelfilldata.address">&nbsp;&nbsp;&nbsp;&nbsp;N/A</span>\n\n                  <span style="color:gray;font-size:11px; text-align:left;font-weight: 400;">{{fuelfilldata.address}}</span>\n\n                </p>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-card-header>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </ion-card>\n\n  </ion-list> -->\n\n\n\n  <ion-list>\n\n    <ion-card *ngFor="let fuelfilldata of   fuelReport">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n          <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n        </ion-thumbnail>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="margin-left:3px;color:gray;font-size:16px;">{{fuelfilldata.vehicleName}}</p>\n\n          </ion-col>\n\n\n\n          <ion-col col-4>\n\n\n\n            <div class="row" style="margin-top:-11px;text-align:center;">\n\n              <img src="assets/imgs/fuel.png" style="height: 19px;width: 17px;margin-top:9px;">&nbsp;\n\n              <span *ngIf="fuelfilldata.litres"></span>\n\n              <span *ngIf="!fuelfilldata.litres" style="font-size:11px;">&nbsp;N/A</span>\n\n              <span style="font-size:11px;color:#d40e0e;margin-top:12px;white-space:normal;">{{fuelfilldata.litres}}&nbsp;L </span>\n\n            </div>\n\n          </ion-col>\n\n\n\n        </ion-row>\n\n\n\n\n\n        <ion-row>\n\n\n\n          <p style="margin-top:0%;">\n\n            <span>\n\n              <ion-icon ios="ios-time" md="md-time" style="margin-left: 6px;font-size:15px;"></ion-icon>&nbsp;</span>\n\n            <span style="font-size:11px;">{{fuelfilldata.timestamp | date: \'medium\'}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n\n\n          <p style="text-align:left;font-size: 14px;">\n\n            <span>\n\n              <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;margin-left: 8px;\n\n                    "></ion-icon>&nbsp;</span>\n\n            <span *ngIf="fuelfilldata.address"></span>\n\n            <span *ngIf="!fuelfilldata.address">&nbsp;&nbsp;N/A</span>\n\n            <span style="color:gray;font-size:11px;font-weight: 400;" ion-text text-wrap>{{fuelfilldata.address}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n        <!--<ion-row style="margin-top:6%;">\n\n\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;">Running</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.tripCount">{{item.tripCount}}</span>\n\n                        <span *ngIf="!item.tripCount">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Trips</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="margin-top: 2%;">\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Distance(Km)</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.avgSpeed">{{item.avgSpeed | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.avgSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;    margin-left:0%;font-size: 11px;font-weight: 350;">Avg.Speed</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.maxSpeed">{{item.maxSpeed}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.maxSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Max Speed</p>\n\n                </ion-col>\n\n            </ion-row> -->\n\n\n\n      </ion-item>\n\n\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\fuel-report\fuel-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], FuelReportPage);
    return FuelReportPage;
}());

//# sourceMappingURL=fuel-report.js.map

/***/ }),

/***/ 561:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as moment from 'moment';
var GeofenceReportPage = /** @class */ (function () {
    function GeofenceReportPage(navCtrl, navParams, apicallGeofenceReport, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallGeofenceReport = apicallGeofenceReport;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        //   var temp = new Date();
        //  var temp = new Date();
        //   this.datetimeStart = temp.toISOString();
        //   // $rootScope.datetimeStart.setHours(0,0,0,0);
        //   this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    GeofenceReportPage.prototype.ngOnInit = function () {
        this.getgeofence1();
    };
    GeofenceReportPage.prototype.getgeofence1 = function () {
        var _this = this;
        console.log("getgeofence shape");
        var baseURLp = 'http://51.38.175.41/geofencing/getallgeofence?uid=' + this.islogin._id;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getallgeofenceCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.devices1243 = [];
            _this.geofencelist = data;
            console.log("geofencelist=> ", _this.geofencelist);
        }, function (err) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(err);
        });
    };
    GeofenceReportPage.prototype.getGeofencedata = function (from, to, geofence) {
        console.log("selectedVehicle=> ", geofence);
        this.Ignitiondevice_id = geofence._id;
        this.getGeofenceReport(from, to);
    };
    GeofenceReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    GeofenceReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    GeofenceReportPage.prototype.getGeofenceReport = function (starttime, endtime) {
        var _this = this;
        // console.log("this.Ignitiondevice_id=> "+ this.Ignitiondevice_id)
        // if (this.Ignitiondevice_id == undefined) {
        //   this.Ignitiondevice_id = "";
        // }
        console.log(starttime);
        console.log(endtime);
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getGeogenceReportApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofenceRepoert = data;
            console.log(_this.geofenceRepoert);
            if (_this.geofenceRepoert.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
            // this.devicesReport = data;
            // console.log(this.devicesReport);
            // this.geofencedata = [];
            // if (this.devicesReport.length == 0) {
            //   var alertPopup = this.alertCtrl.create({
            //     message: 'No Data Found',
            //     buttons: ['OK']
            //   });
            //   alertPopup.present();
            // } else {
            //   for (var i = 0; i < this.devicesReport.length; i++) {
            //     this.StartTime = JSON.stringify(this.devicesReport[i].timestamp).split('"')[1].split('T')[0];
            //     var gmtDateTime = moment.utc(JSON.stringify(this.devicesReport[i].timestamp).split('T')[1].split('.')[0], "HH:mm:ss");
            //     var gmtDate = moment.utc(JSON.stringify(this.devicesReport[i].timestamp).slice(0, -1).split('T'), "YYYY-MM-DD");
            //     this.Startetime = gmtDateTime.local().format(' h:mm a');
            //     this.Startdate = gmtDate.format('ll');
            //     this.geofencedata.push({ 'vehicleName': this.devicesReport[i].vehicleName, 'time': this.Startetime, 'date': this.Startdate, 'direction': this.devicesReport[i].direction, 'address': this.devicesReport[i].address });
            //   }
            //   console.log(this.geofencedata);
            // }
        }, function (error) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(error);
            // let alert = this.alertCtrl.create({
            //   message: "Please Select Geofencing",
            //   buttons: ['OK']
            // });
            // alert.present();
            // let alert = this.alertCtrl.create({
            //   message: 'No data found!',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    GeofenceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence-report\geofence-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Geofence Report</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Geofence</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedGeofence" [items]="geofencelist" itemValueField="geoname" itemTextField="geoname"\n\n      [canSearch]="true" (onChange)="getGeofencedata(datetimeStart, datetimeEnd, selectedGeofence)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  \n\n<ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n  <ion-col width-20>\n\n\n\n    <ion-label ><span style="font-size: 13px">From Date</span>\n\n      <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n        style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n    </ion-label>\n\n  </ion-col>\n\n\n\n  <ion-col width-20>\n\n    <ion-label ><span style="font-size: 13px">To Date</span>\n\n      <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n        style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n    </ion-label>\n\n  </ion-col>\n\n\n\n  <ion-col width-20>\n\n    <div style="margin-top: 9px; float: right">\n\n      <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getGeofenceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n    </div>\n\n  </ion-col>\n\n</ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  \n\n  <ion-list>\n\n    <ion-card *ngFor="let item of geofenceRepoert">\n\n\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-thumbnail item-start  >\n\n                <!-- <img src="assets/imgs/car_red_icon.png" > -->\n\n                 <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <!-- <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))"> -->\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                           <!--  </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n            </ion-thumbnail>\n\n            <ion-row>\n\n              <ion-col col-8>\n\n                <p style="margin-left:3px;color:gray;font-size:16px;">{{item.vehicleName}}</p>\n\n              </ion-col>\n\n\n\n              <ion-col col-4>\n\n                \n\n            <p style="margin-top:0%;text-align:center;" *ngIf="item.direction==\'In\'">\n\n              <span style="font-size:12px;">{{item.direction}}&nbsp;</span>\n\n              <span>\n\n                  <ion-icon ios="ios-arrow-round-down" md="md-arrow-round-down" style="color:#ee7272"></ion-icon>&nbsp;</span>\n\n            </p>\n\n            <p style="margin-top:0%;text-align:center;" *ngIf="item.direction==\'Out\'">\n\n              <span style="font-size:12px;">{{item.direction}}&nbsp;</span>\n\n              <span>\n\n                  <ion-icon ios="ios-arrow-round-up" md="md-arrow-round-up" style="color:#5edb82"></ion-icon>&nbsp;</span>\n\n            </p>\n\n\n\n              </ion-col>\n\n             \n\n           </ion-row>\n\n          \n\n\n\n             <ion-row>\n\n              \n\n                <p style="margin-top:0%;">\n\n                  <span>\n\n                    <ion-icon ios="ios-time" md="md-time" style="margin-left: 6px;font-size:15px;"></ion-icon>&nbsp;</span>\n\n                  <span style="font-size:11px;">{{item.timestamp | date: \'medium\'}}</span>\n\n                </p>\n\n             \n\n             </ion-row>\n\n\n\n             <ion-row>\n\n\n\n                <p style="text-align:left;font-size: 14px;">\n\n                  <span >\n\n                    <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;margin-left: 8px;\n\n                    "></ion-icon>&nbsp;</span>\n\n                  <span *ngIf="item.address"></span>\n\n                  <span *ngIf="!item.address">&nbsp;&nbsp;N/A</span>\n\n                  <span style="color:gray;font-size:11px;font-weight: 400;text-align:justify;" >{{item.address}}</span>\n\n                </p>\n\n\n\n            </ion-row>\n\n            <!--<ion-row style="margin-top:6%;">\n\n\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;">Running</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.tripCount">{{item.tripCount}}</span>\n\n                        <span *ngIf="!item.tripCount">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Trips</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="margin-top: 2%;">\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Distance(Km)</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.avgSpeed">{{item.avgSpeed | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.avgSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;    margin-left:0%;font-size: 11px;font-weight: 350;">Avg.Speed</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.maxSpeed">{{item.maxSpeed}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.maxSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Max Speed</p>\n\n                </ion-col>\n\n            </ion-row> -->\n\n\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n</ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\geofence-report\geofence-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], GeofenceReportPage);
    return GeofenceReportPage;
}());

//# sourceMappingURL=geofence-report.js.map

/***/ }),

/***/ 562:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__customers_modals_group_modal_group_modal__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__update_group_update_group__ = __webpack_require__(358);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { GroupModel } from './group-model/group-model';


var GroupsPage = /** @class */ (function () {
    function GroupsPage(navCtrl, navParams, apigroupcall, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupcall = apigroupcall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
    }
    GroupsPage_1 = GroupsPage;
    GroupsPage.prototype.ngOnInit = function () {
        this.getgroup();
    };
    GroupsPage.prototype.getgroup = function () {
        var _this = this;
        console.log("getgroup");
        var baseURLp = 'http://51.38.175.41/groups/getGroups_list?uid=' + this.islogin._id;
        this.apigroupcall.startLoading().present();
        this.apigroupcall.getGroupCall(baseURLp)
            .subscribe(function (data) {
            _this.apigroupcall.stopLoading();
            _this.groupData = data;
            _this.allGroup = data["group details"];
            console.log("GroupData=> " + JSON.stringify(_this.allGroup));
            // console.log("customerlist=> ", this.customerslist)
            _this.GroupArray = [];
            for (var i = 0; i < _this.allGroup.length; i++) {
                _this.allGroupName = _this.allGroup[i].name;
                _this.datetime = _this.allGroup[i].last_modified;
                _this.Groupdevice = _this.allGroup[i].devices.length;
                _this.status = _this.allGroup[i].status;
                _this.groupId = _this.allGroup[i]._id;
                _this.GroupArray.push({ 'groupname': _this.allGroupName, 'status': _this.status, 'vehicle': _this.Groupdevice, '_id': _this.groupId, 'datetime': _this.datetime });
                console.log(_this.GroupArray);
            }
        }, function (err) {
            _this.apigroupcall.stopLoading();
            console.log("error found=> " + err);
        });
    };
    GroupsPage.prototype.deleteItem = function (item) {
        var that = this;
        console.log("delete");
        console.log(item);
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(item._id);
                        that.deleteDevice(item._id);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    GroupsPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apigroupcall.startLoading().present();
        this.apigroupcall.deleteGroupCall(d_id)
            .subscribe(function (data) {
            _this.apigroupcall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.push(GroupsPage_1);
            });
            toast.present();
        }, function (err) {
            _this.apigroupcall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GroupsPage.prototype.openAddGroupModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__customers_modals_group_modal_group_modal__["a" /* GroupModalPage */]);
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getgroup();
        });
        modal.present();
    };
    GroupsPage.prototype.openUpdateGroupModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__update_group_update_group__["a" /* UpdateGroup */]);
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getgroup();
        });
        modal.present();
    };
    GroupsPage = GroupsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-groups',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\groups.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Groups</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openAddGroupModal()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-list >\n\n    <ion-item *ngFor="let item of GroupArray">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n      </ion-thumbnail>\n\n      <h2>{{item.groupname}}</h2>\n\n      <p>{{item.datetime | date: \'short\'}}</p>\n\n      <p><span style="color:rgb(255,64,129);">{{item.vehicle}}</span>&nbsp;&nbsp;Vehicles</p>\n\n      <button ion-button clear item-end *ngIf="item.status">Active</button>\n\n      <button ion-button clear item-end *ngIf="!item.status">InActive</button>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <!-- <ion-list>\n\n    <div *ngFor="let item of GroupArray">\n\n     \n\n\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n        </ion-thumbnail>\n\n\n\n        <ion-row>\n\n          <ion-col col-8>\n\n\n\n            <p>\n\n              <span ion-text color="dark" style="text-align:left;"></span> {{item.groupname}}</p>\n\n\n\n          </ion-col>\n\n          <ion-col col-4>\n\n\n\n            <p style="text-align:left;font-size: 14px;">&nbsp;&nbsp;\n\n              <span style="color:#23c797;" *ngIf="item.status == true">Active&nbsp;</span>\n\n            </p>\n\n            <p style="text-align:left;font-size: 14px;">&nbsp;&nbsp;\n\n              <span style="color:#c74423;" *ngIf="item.status != true">InActive&nbsp;</span>\n\n            </p>\n\n\n\n          </ion-col>\n\n\n\n        </ion-row>\n\n\n\n\n\n        <p style="font-size:12px;">{{item.datetime | date: \'medium\'}}</p>\n\n\n\n        <p>\n\n          <span style="color:rgb(255,64,129);">{{item.vehicle}}</span>&nbsp;&nbsp;Vehicles</p>\n\n     \n\n        <p>\n\n          <button ion-button small (click)="openUpdateGroupModal(item)">Edit</button>\n\n          <button ion-button small (click)="deleteItem(item)">Delete</button>\n\n        </p>\n\n      </ion-item>\n\n  \n\n    </div>\n\n  </ion-list> -->\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\groups.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], GroupsPage);
    return GroupsPage;
    var GroupsPage_1;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 563:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IgnitionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IgnitionReportPage = /** @class */ (function () {
    function IgnitionReportPage(navCtrl, navParams, apicalligi, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    IgnitionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    IgnitionReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    IgnitionReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    IgnitionReportPage.prototype.getdevices = function () {
        var _this = this;
        this.apicalligi.startLoading().present();
        this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.isdevice = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    IgnitionReportPage.prototype.getIgnitiondevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle.Device_Name;
        // this.getIgnitiondeviceReport(from, to);
    };
    IgnitionReportPage.prototype.getIgnitiondeviceReport = function (starttime, endtime) {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicalligi.startLoading().present();
        this.apicalligi.getIgiApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.igiReport = data;
            console.log(_this.igiReport);
            if (_this.igiReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicalligi.stopLoading();
            _this.apicalligi.stopLoading();
            // console.log(error);
            // let alert = this.alertCtrl.create({
            //   message: 'No data found!',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    IgnitionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ignition-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\ignition-report\ignition-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Ignition Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getIgnitiondevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getIgnitiondeviceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n\n\n  <!-- <ion-list>\n\n    <ion-item *ngFor="let ignitationdata of igiReport">\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <h2>{{ignitationdata.vehicleName}}</h2>\n\n        </ion-col>\n\n        <ion-col width-50 style="text-align: right; font-size: 13px">\n\n          <ion-icon style="font-size: 18px;" name="switch" color="secondary" *ngIf="ignitationdata.switch==\'ON\'"></ion-icon>\n\n          <ion-icon style="font-size: 18px;" name="switch" color="danger" *ngIf="ignitationdata.switch==\'OFF\'"></ion-icon>\n\n          &nbsp;&nbsp;{{ignitationdata.switch}}\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <br/>\n\n      <p>\n\n        <ion-icon style="font-size:15px;" ios="ios-time" md="md-time"></ion-icon>&nbsp; {{ignitationdata.timestamp | date: \'medium\'}}\n\n      </p>\n\n      <p *ngIf="ignitationdata.address" ion-text text-wrap>\n\n        <ion-icon name="pin" color="danger"></ion-icon>&nbsp; {{ignitationdata.address}}\n\n      </p>\n\n      <p *ngIf="!ignitationdata.address">\n\n        <ion-icon name="pin" color="danger"></ion-icon>&nbsp; N/A\n\n      </p>\n\n\n\n\n\n    </ion-item>\n\n  </ion-list> -->\n\n  <!-- <ion-list>\n\n\n\n    <ion-card *ngFor="let ignitationdata of igiReport">\n\n\n\n      <ion-row>\n\n        <ion-col col-5>\n\n          <div>\n\n            <p style="margin-top:0%;">\n\n              <span></span>\n\n              <span style="color:gray;font-size:16px; margin-top: -9px;margin-left: 6px;font-weight: 400;">{{ignitationdata.vehicleName}}</span>\n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-5>\n\n          <div>\n\n            <p style="margin-top:0%;">\n\n              <span>\n\n                <ion-icon ios="ios-time" md="md-time"></ion-icon>&nbsp;</span>\n\n              <span style="font-size:11px;">{{ignitationdata.timestamp | date: \'medium\'}}</span>\n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <div style="margin-top:0%;">\n\n\n\n            <p style="margin-top:0%;text-align:center;" *ngIf="ignitationdata.switch==\'OFF\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#ee7272"></ion-icon>&nbsp;</span>\n\n            </p>\n\n            <p style="margin-top:0%;text-align:center;" *ngIf="ignitationdata.switch==\'ON\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#5edb82"></ion-icon>&nbsp;</span>\n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-card-header style="padding:0px;">\n\n            <ion-row>\n\n              <ion-col>\n\n                <p style="text-align:left;font-size: 14px;">\n\n                  <span >\n\n                    <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>&nbsp;</span>&nbsp;&nbsp;\n\n                  <span *ngIf="ignitationdata.address"></span>\n\n                  <span *ngIf="!ignitationdata.address">&nbsp;&nbsp;&nbsp;&nbsp;N/A</span>\n\n                  <span style="color:gray;font-size:11px; text-align:left;font-weight: 400;">{{ignitationdata.address}}</span>\n\n                </p>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-card-header>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </ion-card>\n\n  </ion-list> -->\n\n  <ion-list>\n\n    <ion-card *ngFor="let ignitationdata of igiReport">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n          <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n        </ion-thumbnail>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="margin-left:3px;color:gray;font-size:16px;">{{ignitationdata.vehicleName}}</p>\n\n          </ion-col>\n\n\n\n          <ion-col col-4>\n\n            <p style="margin-top:0%;text-align:center;" *ngIf="ignitationdata.switch==\'OFF\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#ee7272"></ion-icon>&nbsp;</span>\n\n            </p>\n\n            <p style="margin-top:0%;text-align:center;" *ngIf="ignitationdata.switch==\'ON\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#5edb82"></ion-icon>&nbsp;</span>\n\n            </p>\n\n          </ion-col>\n\n\n\n        </ion-row>\n\n\n\n\n\n        <ion-row>\n\n\n\n          <p style="margin-top:0%;">\n\n            <span>\n\n              <ion-icon ios="ios-time" md="md-time" style="margin-left: 6px;font-size:15px;"></ion-icon>&nbsp;</span>\n\n            <span style="font-size:11px;">{{ignitationdata.timestamp | date: \'medium\'}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n\n\n          <p style="text-align:left;font-size: 14px;">\n\n            <span>\n\n              <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;margin-left: 8px;\n\n                    "></ion-icon>&nbsp;</span>\n\n            <span *ngIf="ignitationdata.address"></span>\n\n            <span *ngIf="!ignitationdata.address">&nbsp;&nbsp;N/A</span>\n\n            <span style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ignitationdata.address}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n        <!--<ion-row style="margin-top:6%;">\n\n\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;">Running</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.tripCount">{{item.tripCount}}</span>\n\n                        <span *ngIf="!item.tripCount">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Trips</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="margin-top: 2%;">\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Distance(Km)</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.avgSpeed">{{item.avgSpeed | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.avgSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;    margin-left:0%;font-size: 11px;font-weight: 350;">Avg.Speed</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.maxSpeed">{{item.maxSpeed}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.maxSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Max Speed</p>\n\n                </ion-col>\n\n            </ion-row> -->\n\n\n\n      </ion-item>\n\n\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\ignition-report\ignition-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], IgnitionReportPage);
    return IgnitionReportPage;
}());

//# sourceMappingURL=ignition-report.js.map

/***/ }),

/***/ 564:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverSpeedRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OverSpeedRepoPage = /** @class */ (function () {
    function OverSpeedRepoPage(navCtrl, navParams, apicalloverspeed, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalloverspeed = apicalloverspeed;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    OverSpeedRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    OverSpeedRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    OverSpeedRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    OverSpeedRepoPage.prototype.getOverspeeddevice = function (selectedVehicle) {
        // console.log("selectedVehicle=> ", selectedVehicle)
        this.overSpeeddevice_id = selectedVehicle._id;
        // console.log("selected vehicle=> " + this.overSpeeddevice_id)
    };
    OverSpeedRepoPage.prototype.getdevices = function () {
        var _this = this;
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.isdevice = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicalloverspeed.stopLoading();
            console.log(err);
        });
    };
    OverSpeedRepoPage.prototype.getOverSpeedReport = function (starttime, endtime) {
        var _this = this;
        var baseUrl;
        if (this.overSpeeddevice_id == undefined) {
            baseUrl = "http://51.38.175.41/notifs/overSpeedReport?from_date=" + starttime + '&to_date=' + endtime + '&_u=' + this.islogin._id;
        }
        else {
            baseUrl = "http://51.38.175.41/notifs/overSpeedReport?from_date=" + starttime + '&to_date=' + endtime + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id;
        }
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.getOverSpeedApi(baseUrl)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.overspeedReport = data;
            console.log(_this.overspeedReport);
            if (_this.overspeedReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicalloverspeed.stopLoading();
            console.log(error);
        });
    };
    OverSpeedRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-over-speed-repo',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\over-speed-repo\over-speed-repo.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Over Speed Report</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getOverspeeddevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getOverSpeedReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card *ngFor="let overdata of overspeedReport">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n        <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n      </ion-avatar>\n\n      <ion-row>\n\n        <ion-col col-8>\n\n          <p style="margin-left:3px;color:gray;font-size:16px;">{{overdata.vehicleName}}</p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <div style="margin-top:0%;">\n\n            <p style="margin-top:0%;text-align:center;">\n\n              <span>\n\n                <ion-icon ios="ios-speedometer" md="md-speedometer" style="color:#e14444;"></ion-icon>&nbsp;\n\n              </span>\n\n              <span style="font-size:12px;">{{overdata.overSpeed}}&nbsp;</span>\n\n            </p>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <br/>\n\n      <ion-row>\n\n        <div class="overme" *ngIf="overdata.address">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>&nbsp;&nbsp;{{overdata.address}}\n\n        </div>\n\n        <div class="overme" *ngIf="!overdata.address">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>&nbsp;&nbsp;N/A\n\n        </div>\n\n      </ion-row>\n\n    </ion-item>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\over-speed-repo\over-speed-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], OverSpeedRepoPage);
    return OverSpeedRepoPage;
}());

//# sourceMappingURL=over-speed-repo.js.map

/***/ }),

/***/ 565:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouteVoilationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RouteVoilationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RouteVoilationsPage = /** @class */ (function () {
    function RouteVoilationsPage(navCtrl, navParams, apicallroute, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallroute = apicallroute;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    RouteVoilationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RouteVoilationsPage');
    };
    RouteVoilationsPage.prototype.ngOnInit = function () {
        this.getRoute();
    };
    RouteVoilationsPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    RouteVoilationsPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    RouteVoilationsPage.prototype.getRoute = function () {
        var _this = this;
        var baseURLp = 'http://51.38.175.41/trackRoute/user/' + this.islogin._id;
        this.apicallroute.startLoading().present();
        this.apicallroute.getallrouteCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallroute.stopLoading();
            _this.devices1243 = [];
            _this.routelist = data;
            console.log("Routelist=> ", _this.routelist);
        }, function (err) {
            _this.apicallroute.stopLoading();
            console.log(err);
        });
    };
    RouteVoilationsPage.prototype.getRouteName = function (from, to, selectedroute) {
        console.log("selectedVehicle=> ", selectedroute);
        this.routename_id = selectedroute.Device_Name;
    };
    RouteVoilationsPage.prototype.getroutevoilation = function (starttime, endtime) {
        var _this = this;
        var baseURLp = 'http://51.38.175.41/notifs/RouteVoilationReprot?from_date=' + starttime + '&to_date=' + endtime + '&_u=' + this.islogin._id;
        this.apicallroute.startLoading().present();
        this.apicallroute.getallrouteCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallroute.stopLoading();
            _this.routevolitionReport = data;
            console.log(_this.routevolitionReport);
            if (_this.routevolitionReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallroute.stopLoading();
            console.log(error);
        });
    };
    RouteVoilationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-route-voilations',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\route-voilations\route-voilations.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Route Violation Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Route</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedRoute" [items]="routelist" itemValueField="name" itemTextField="name"\n\n      [canSearch]="true" (onChange)="getRouteName(datetimeStart, datetimeEnd, selectedRoute)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getroutevoilation(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n<ion-content>\n\n\n\n  <ion-list>\n\n    <ion-card *ngFor="let routedata of routevolitionReport">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n          <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                                  \n\n                      <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                  </ion-thumbnail>\n\n      \n\n                  <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                      <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                  </ion-thumbnail>\n\n      \n\n                  <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                      <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n      \n\n                      <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                      <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                      <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n        </ion-avatar>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="margin-left:3px;color:black;font-size:16px;">{{routedata.vehicleName}}</p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n\n\n        <ion-row>\n\n\n\n          <p style="margin-top:0%;">\n\n            <span>\n\n              <ion-icon ios="ios-time" md="md-time" style="margin-left: 6px;font-size:15px;"></ion-icon>&nbsp;</span>\n\n            <span style="font-size:11px;">{{routedata.timestamp | date: \'medium\'}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n\n\n          <p style="font-size: 14px;">\n\n            <span>\n\n              <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:17px;margin-left: 8px;\n\n                          "></ion-icon>&nbsp;</span>\n\n            <span *ngIf="routedata.address"></span>\n\n            <span *ngIf="!routedata.address">&nbsp;&nbsp;N/A</span>\n\n            <span style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{routedata.address}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n      </ion-item>\n\n\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\route-voilations\route-voilations.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], RouteVoilationsPage);
    return RouteVoilationsPage;
}());

//# sourceMappingURL=route-voilations.js.map

/***/ }),

/***/ 566:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__edit_route_details_edit_route_details__ = __webpack_require__(374);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { CreateRoutePage } from './create-route/create-route';
var RoutePage = /** @class */ (function () {
    function RoutePage(navCtrl, navParams, apiCall, modalCtrl, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
    }
    RoutePage.prototype.ngOnInit = function () {
        this.getRoutes();
    };
    RoutePage.prototype.getRoutes = function () {
        var _this = this;
        console.log("getRoutes");
        var baseURLp = 'http://51.38.175.41/trackRoute/user/' + this.islogin._id;
        //console.log(baseURLp);
        this.apiCall.startLoading().present();
        this.apiCall.getRoutesCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.routesdata = data.reverse();
            console.log(_this.routesdata);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    // createRoute() {
    //   this.navCtrl.push(CreateRoutePage)
    // }
    // searchUser(ev: any) {
    //   // Reset items back to all of the items
    //   // this.getcustomer();
    //   // set val to the value of the searchbar
    //   let val = ev.target.value;
    //   // if the value is an empty string don't filter the items
    //   if (val && val.trim() != '') {
    //     this.routesdata = this.routesdata.filter((item) => {
    //       return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //     })
    //   }
    // }
    RoutePage.prototype.openroute_edit = function (routes) {
        console.log('Opening Modal open update deviceModal');
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__edit_route_details_edit_route_details__["a" /* EditRouteDetailsPage */], {
            param: routes
        });
        modal.present();
        // $rootScope.routesdetail = angular.copy(routes1);
        // console.log($rootScope.routesdetail);
    };
    ;
    RoutePage.prototype.deleteFunc = function (_id) {
        var _this = this;
        // var link = 'http://13.126.36.205:3000/trackRoute/' + _id;
        this.apiCall.startLoading().present();
        this.apiCall.trackRouteCall(_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data) {
                // console.log(this.DeletedDevice);
                var toast = _this.toastCtrl.create({
                    message: 'Route was deleted successfully',
                    position: 'bottom',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.getRoutes();
                });
                toast.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("deleteing route error=> " + err);
            var toast = _this.toastCtrl.create({
                message: 'Route was deleted successfully',
                position: 'bottom',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getRoutes();
            });
            toast.present();
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alertCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    RoutePage.prototype.DelateRoute = function (data) {
        var _this = this;
        console.log(data._id);
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this route?',
            buttons: [{
                    text: 'NO'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteFunc(data._id);
                    }
                }]
        });
        alert.present();
    };
    RoutePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-route',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\route\route.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Routes Details</ion-title>\n    <!-- <ion-buttons end>\n      <button ion-button icon-only (click)="createRoute()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n  <!-- <ion-searchbar placeholder="Search..." (ionInput)="searchUser($event)"></ion-searchbar> -->\n</ion-header>\n\n\n<ion-content>\n\n  <ion-list>\n    <ion-item *ngFor="let routes of routesdata">\n      <ion-thumbnail item-start>\n        <img src="assets/imgs/icon_1024.png">\n      </ion-thumbnail>\n      <h2>{{routes.name}}</h2>\n      <!-- <br/> -->\n      <!-- <p *ngIf="routes.source" ion-text text-wrap>\n        <ion-icon name="pin" color="secondary"></ion-icon>&nbsp;&nbsp;{{routes.source}}\n      </p> -->\n      <p *ngIf="routes.source" class="overme">\n        <ion-icon name="pin" color="secondary"></ion-icon>&nbsp;{{routes.source}}\n      </p>\n      <!-- <br/> -->\n      <p *ngIf="routes.destination" class="overme">\n        <ion-icon name="pin" color="danger"></ion-icon>&nbsp;{{routes.destination}}\n      </p>\n      <!-- <p *ngIf="routes.destination" ion-text text-wrap>\n        <ion-icon name="pin" color="danger"></ion-icon>&nbsp;&nbsp;{{routes.destination}}\n      </p> -->\n\n      <p *ngIf="!routes.source">N/A</p>\n      <p *ngIf="!routes.destination">N/A</p>\n      <ion-row item-end>\n        <ion-col width-10>\n          <!-- <div style="margin-top:10%;">\n            <ion-icon name="create" style="margin-left:10%;text-align: right;" (click)="openroute_edit(routes)"></ion-icon>\n          </div> -->\n          <div style="margin-top:100%;">\n            <ion-icon name="trash" color="danger" style="margin-right:15%;text-align: right;" (click)="DelateRoute(routes)"></ion-icon>\n          </div>\n\n\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\route\route.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], RoutePage);
    return RoutePage;
}());

//# sourceMappingURL=route.js.map

/***/ }),

/***/ 567:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowGeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ShowGeofencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShowGeofencePage = /** @class */ (function () {
    function ShowGeofencePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ShowGeofencePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShowGeofencePage');
    };
    ShowGeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-show-geofence',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\show-geofence\show-geofence.html"*/'<!--\n  Generated template for the ShowGeofencePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>showGeofence</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\show-geofence\show-geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ShowGeofencePage);
    return ShowGeofencePage;
}());

//# sourceMappingURL=show-geofence.js.map

/***/ }),

/***/ 568:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeedRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Chart } from 'chart.js';


var SpeedRepoPage = /** @class */ (function () {
    function SpeedRepoPage(navCtrl, navParams, apiCallspeed) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCallspeed = apiCallspeed;
        this.showChartBox = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = new Date().toISOString();
    }
    SpeedRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    SpeedRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    SpeedRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    SpeedRepoPage.prototype.getdevices = function () {
        var _this = this;
        this.apiCallspeed.startLoading().present();
        this.apiCallspeed.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apiCallspeed.stopLoading();
            _this.showChartBox = true;
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apiCallspeed.stopLoading();
            console.log(err);
        });
    };
    SpeedRepoPage.prototype.getspeeddevice = function (selectedVehicle) {
        var _this = this;
        console.log("selectedVehicle=> ", selectedVehicle);
        this.device_id = selectedVehicle.Device_ID;
        // this.getIgnitiondeviceReport(from, to);
        console.log("device_id=> ", this.device_id);
        // https://www.oneqlik.in/gps/getGpsSpeedReport?imei=866968030813480&time=2018-10-01T12:34:13.662Z
        var baseURLp = 'http://51.38.175.41/gps/getGpsSpeedReport?imei=' + this.device_id + '&time=' + this.datetimeStart;
        this.apiCallspeed.startLoading().present();
        this.apiCallspeed.getSpeedReport(baseURLp)
            .subscribe(function (data) {
            _this.apiCallspeed.stopLoading();
            _this.SpeedReport = data;
            console.log(_this.SpeedReport);
            var dataArraySpeed = new Array;
            for (var i = _this.SpeedReport.length - 1; i > 0; i--) {
                dataArraySpeed.push(_this.SpeedReport[i].speed);
            }
            console.log(dataArraySpeed);
            var dataArrayDate = new Array;
            for (var j = _this.SpeedReport.length - 1; j > 0; j--) {
                /*  $scope.datee=($scope.devicesReport[i].time).split('T')[0];*/
                _this.datee = __WEBPACK_IMPORTED_MODULE_3_moment__((_this.SpeedReport[j].time)).local().format('h:mm:s a');
                /* console.log($scope.datee);*/
                dataArrayDate.push(_this.datee);
            }
            console.log("dataArrayDate=> " + dataArrayDate);
            _this.lineChart = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](_this.myChartspeed.nativeElement, {
                type: 'line',
                data: {
                    datasets: [{
                            data: dataArraySpeed,
                            label: "Vehicle Speed(Kmph)",
                            backgroundColor: "rgb(136,172,161)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(232,105,90,0.8)",
                            hoverBorderColor: "orange",
                            scaleStepWidth: 1
                        }],
                    labels: dataArrayDate
                }
            });
        }, function (error) {
            _this.apiCallspeed.stopLoading();
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myChartspeed'),
        __metadata("design:type", Object)
    ], SpeedRepoPage.prototype, "myChartspeed", void 0);
    SpeedRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-speed-repo',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\speed-repo\speed-repo.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    <ion-title>Speed Variation Report</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n      <ion-label>Select Vehicle</ion-label>\n\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n        [canSearch]="true" (onChange)="getspeeddevice(selectedVehicle)">\n\n      </select-searchable>\n\n    </ion-item>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <canvas #myChartspeed width="280" height="280"></canvas>\n\n <!-- <ion-card *ngIf="showChartBox"> \n\n    <ion-card-content>\n\n        <canvas #myChartspeed width="280" height="280"></canvas>\n\n      </ion-card-content>\n\n </ion-card> -->\n\n</ion-content>\n\n'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\speed-repo\speed-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], SpeedRepoPage);
    return SpeedRepoPage;
}());

//# sourceMappingURL=speed-repo.js.map

/***/ }),

/***/ 569:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SplashPage = /** @class */ (function () {
    function SplashPage(viewCtrl, splashScreen) {
        this.viewCtrl = viewCtrl;
        this.splashScreen = splashScreen;
    }
    SplashPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad SplashPage');
        this.splashScreen.hide();
        setTimeout(function () {
            _this.viewCtrl.dismiss();
        }, 4000);
    };
    SplashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-splash',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\splash\splash.html"*/'<ion-content>\n \n  <svg id="bars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63.15 224.35">\n      <defs>\n          <style>.cls-1{fill:#dd238c;}.cls-2{fill:#ef4328;}.cls-3{fill:#7dd0df;}.cls-4{fill:#febf12;}.cls-5{fill:#282828;}</style>\n      </defs>\n      <title>jmlogo</title>\n      <rect class="cls-1" x="27.22" width="20.06" height="163.78"/>\n      <rect class="cls-2" y="4" width="20.06" height="163.78"/>\n      <rect class="cls-3" x="13.9" y="13.1" width="20.06" height="163.78"/>\n      <rect class="cls-4" x="43.1" y="7.45" width="20.06" height="163.78"/>\n      <path class="cls-5" d="M243.5,323a12,12,0,0,1-.5,3.43,8.88,8.88,0,0,1-1.63,3.1,8.24,8.24,0,0,1-3,2.26,10.8,10.8,0,0,1-4.58.86,9.63,9.63,0,0,1-6-1.82,8.48,8.48,0,0,1-3.07-5.47l4-.82a5.64,5.64,0,0,0,1.66,3.19,4.86,4.86,0,0,0,3.43,1.18,5.71,5.71,0,0,0,2.83-.62,4.53,4.53,0,0,0,1.7-1.63,7,7,0,0,0,.84-2.33,15.15,15.15,0,0,0,.24-2.71V297.82h4V323Z" transform="translate(-224.04 -108.31)"/>\n      <path class="cls-5" d="M252,297.82h6l11.52,26.64h0.1l11.62-26.64H287v34h-4V303.29h-0.1L270.72,331.8h-2.45l-12.19-28.51H256V331.8h-4v-34Z" transform="translate(-224.04 -108.31)"/>\n  </svg>\n  \n\n</ion-content>\n'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\splash\splash.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], SplashPage);
    return SplashPage;
}());

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoppagesRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StoppagesRepoPage = /** @class */ (function () {
    function StoppagesRepoPage(navCtrl, navParams, apicallStoppages, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallStoppages = apicallStoppages;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    StoppagesRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    StoppagesRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    StoppagesRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    StoppagesRepoPage.prototype.getdevices = function () {
        var _this = this;
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallStoppages.stopLoading();
            console.log(err);
        });
    };
    StoppagesRepoPage.prototype.getStoppagesdevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    StoppagesRepoPage.prototype.getStoppageReport = function (starttime, endtime) {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        // console.log(starttime);
        // console.log(endtime);
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.getStoppageApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.stoppagesReport = data;
            // console.log(this.stoppagesReport);
            _this.Stoppagesdata = [];
            if (_this.stoppagesReport.length != 0) {
                for (var i = 0; i < _this.stoppagesReport.length; i++) {
                    _this.arrivalTime = new Date(_this.stoppagesReport[i].arrival_time).toLocaleString();
                    _this.departureTime = new Date(_this.stoppagesReport[i].departure_time).toLocaleString();
                    var fd = new Date(_this.arrivalTime).getTime();
                    var td = new Date(_this.departureTime).getTime();
                    var time_difference = td - fd;
                    var total_min = time_difference / 60000;
                    var hours = total_min / 60;
                    var rhours = Math.floor(hours);
                    var minutes = (hours - rhours) * 60;
                    var rminutes = Math.round(minutes);
                    _this.Durations = rhours + ':' + rminutes;
                    _this.Stoppagesdata.push({ 'arrival_time': _this.stoppagesReport[i].arrival_time, 'departure_time': _this.stoppagesReport[i].departure_time, 'Durations': _this.Durations, 'address': _this.stoppagesReport[i].address, 'device': _this.stoppagesReport[i].device.Device_Name });
                    // console.log(this.Stoppagesdata);
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'No data found!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                _this.selectedVehicle = undefined;
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallStoppages.stopLoading();
            console.log(error);
        });
    };
    StoppagesRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-stoppages-repo',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\stoppages-repo\stoppages-repo.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Stoppages Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getStoppagesdevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20 padding-left>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getStoppageReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n<ion-content>\n\n\n\n\n\n  <!-- <ion-list> -->\n\n  <ion-card *ngFor="let stopdata of Stoppagesdata ">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n      </ion-thumbnail>\n\n      <ion-row>\n\n        <ion-col col-8>\n\n          <p style="margin-left:3px;color:black;font-size:16px;">{{stopdata.device}}</p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <p>\n\n            <span style="font-size:12px;color:#d40e0e">{{stopdata.Durations}} Min&nbsp;</span>\n\n            <span>&nbsp;</span>\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <br/>\n\n      <ion-row style="padding-left: 4px">\n\n        <ion-col col-6>\n\n          <p style="font-size: 11px;">\n\n            <ion-icon ios="ios-time" md="md-time" style="color:#5edb82"></ion-icon>\n\n            {{stopdata.arrival_time | date: \'short\'}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n          <p style="font-size: 11px;">\n\n            <ion-icon ios="ios-time" md="md-time" style="color:#ee7272;"></ion-icon>\n\n            {{stopdata.departure_time | date: \'short\'}}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row style="padding-left: 4px; padding-top:4px">\n\n        <p style="font-size: 14px;">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;"></ion-icon>&nbsp;\n\n          <span *ngIf="stopdata.address" style="color:gray;font-size:11px;font-weight: 400;">{{stopdata.address}}</span>\n\n          <span *ngIf="!stopdata.address">&nbsp;&nbsp;N/A</span>\n\n        </p>\n\n      </ion-row>\n\n    </ion-item>\n\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\stoppages-repo\stoppages-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], StoppagesRepoPage);
    return StoppagesRepoPage;
}());

//# sourceMappingURL=stoppages-repo.js.map

/***/ }),

/***/ 571:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SupportPage = /** @class */ (function () {
    function SupportPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SupportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupportPage');
    };
    SupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-support',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\support\support.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Support</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <ion-card-header>\n      Customer Support Contacts\n    </ion-card-header>\n    <ion-card-content>\n      <a href="tel:+91-9028045389">\n        <ion-icon ios="ios-call" md="md-call" style="color:#33cd5f;" (click)="dialSupportNumber()"></ion-icon>\n        <span>+91 9028045389</span>\n      </a>\n      <br/>\n      <a href="mailto:poonam.g@processfactory.in">\n        <ion-icon ios="ios-mail" md="md-mail" style="color:red;" (click)="dialSupportNumber()"></ion-icon>\n        <span>poonam.g@processfactory.in</span>\n      </a>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\support\support.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SupportPage);
    return SupportPage;
}());

//# sourceMappingURL=support.js.map

/***/ }),

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripReportPage = /** @class */ (function () {
    function TripReportPage(navCtrl, navParams, apicalligi, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        var settime = temp.getTime();
        // this.datetime=new Date(settime).setMinutes(0);
        this.datetime = new Date(settime).setHours(5, 30, 0);
        this.datetimeStart = new Date(this.datetime).toISOString();
        var a = new Date();
        a.setHours(a.getHours() + 5);
        a.setMinutes(a.getMinutes() + 30);
        this.datetimeEnd = new Date(a).toISOString();
    }
    TripReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    TripReportPage.prototype.getdevices = function () {
        var _this = this;
        this.apicalligi.startLoading().present();
        this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.isdevice = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    TripReportPage.prototype.getTripdevice = function (from, to, item) {
        console.log(item);
        this.device_id = item._id;
        console.log(this.device_id);
        localStorage.setItem('devices_id', item);
        this.isdeviceTripreport = localStorage.getItem('devices_id');
    };
    TripReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    TripReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    TripReportPage.prototype.getTripReport = function (starttime, endtime) {
        var _this = this;
        this.TripReportData = [];
        console.log("trip Report report");
        console.log(starttime);
        console.log(endtime);
        if (endtime <= starttime && this.device_id) {
            console.log("to time always greater");
            var alert_1 = this.alertCtrl.create({
                message: 'To time always greater than From Time',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            var baseURLp = 'http://51.38.175.41/user_trip/trip_detail?uId=' + this.islogin._id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + this.device_id;
            this.apicalligi.startLoading().present();
            this.apicalligi.trip_detailCall(baseURLp)
                .subscribe(function (data) {
                _this.apicalligi.stopLoading();
                if (data.length == 0) {
                    var alert_2 = _this.alertCtrl.create({
                        message: 'No data found',
                        buttons: ['OK']
                    });
                    alert_2.present();
                }
                else {
                    console.log("response=> " + data);
                    _this.TripsdataAddress = [];
                    for (var i = 0; i < data.length; i++) {
                        _this.deviceId = data[i]._id;
                        _this.distanceBt = data[i].distance / 1000;
                        _this.StartTime = JSON.stringify(data[i].start_time).split('"')[1].split('T')[0];
                        var gmtDateTime = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
                        var gmtDate = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
                        _this.Startetime = gmtDateTime.local().format(' h:mm a');
                        _this.Startdate = gmtDate.format('ll');
                        _this.EndTime = JSON.stringify(data[i].end_time).split('"')[1].split('T')[0];
                        var gmtDateTime1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
                        var gmtDate1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
                        _this.Endtime = gmtDateTime1.local().format(' h:mm a');
                        _this.Enddate = gmtDate1.format('ll');
                        // var arr = [];
                        _this.TripReportData.push({ 'Startetime': _this.Startetime, 'Startdate': _this.Startdate, 'Endtime': _this.Endtime, 'Enddate': _this.Enddate, 'distance': _this.distanceBt, '_id': _this.deviceId, 'startAddress': data[i].startAddress, 'endAddress': data[i].endAddress, 'start_time': data[i].start_time, 'end_time': data[i].end_time });
                    }
                    console.log("TripReportData=> " + _this.TripReportData);
                }
            }, function (err) {
                _this.apicalligi.stopLoading();
                console.log(err);
            });
        }
    };
    TripReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-report',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\trip-report\trip-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Trip Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getTripdevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getTripReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <!-- <ion-row padding-left color="primary">\n\n    <ion-col width-20>\n\n\n\n      <ion-label style="font-size: 12px">From Date\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label style="font-size: 12px">To Date\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px;">\n\n        <ion-icon ios="ios-search" md="md-search" style="text-align: right; font-size: 20px;margin-top: 7px;" (click)="getTripReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row> -->\n\n\n\n  <hr>\n\n  <ion-list>\n\n    <ion-item *ngFor="let tripdata of TripReportData">\n\n      <ion-thumbnail item-end>\n\n        <img src="assets/imgs/37484830-00d91c72-288a-11e8-8a47-2d0d630fb5fb.jpg">\n\n      </ion-thumbnail>\n\n      <h6>\n\n        <ion-icon name="clock" style="color: gray;"></ion-icon>&nbsp;&nbsp;&nbsp;{{tripdata.Startdate}}{{tripdata.Startetime}}&nbsp;-&nbsp;{{tripdata.Endtime}}</h6>\n\n      <br/>\n\n      <ion-badge *ngIf="!tripdata.distance">0KM</ion-badge>\n\n      <ion-badge *ngIf="tripdata.distance">{{tripdata.distance | number : \'1.0-2\'}}KM</ion-badge>\n\n      <p style="font-size: 13px; margin-top: 2%;">\n\n        <ion-icon name="pin" color="secondary" style="font-size: 13px;"></ion-icon>&nbsp;&nbsp;&nbsp;{{tripdata.startAddress}}</p>\n\n      <p style="font-size: 13px;">\n\n        <ion-icon name="pin" color="danger" style="font-size: 13px;"></ion-icon>&nbsp;&nbsp;&nbsp;{{tripdata.startAddress}}</p>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\trip-report\trip-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], TripReportPage);
    return TripReportPage;
}());

//# sourceMappingURL=trip-report.js.map

/***/ }),

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_dashboard_dashboard__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_network_network__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_menu_menu__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_ReplaySubject__ = __webpack_require__(593);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












// import { ArrayObservable } from "rxjs/observable/ArrayObservable";
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, 
        // private network: Network,
        events, networkProvider, menuProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_10_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            // console.log("islogin=> " + JSON.stringify(this.islogin));
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            // let splash = modalCtrl.create(SplashPage);
            // splash.present();
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        console.log("DealerDetails=> " + this.DealerDetails._id);
        this.dealerStatus = this.islogin.isDealer;
        console.log("dealerStatus=> " + this.dealerStatus);
        this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_dashboard_dashboard__["a" /* DashboardPage */];
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        }
    }
    // atFirst() {
    //   this.events.subscribe('user:updated', (udata) => {
    //     this.islogin = udata;
    //     console.log("islogin=> " + JSON.stringify(this.islogin));
    //   });
    // }
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
            }
        });
    };
    MyApp.prototype.backBtnHandler = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            var nav = _this.app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name === "DashboardPage" || activeView.name === "LoginPage") {
                if (nav.canGoBack()) {
                    nav.pop();
                }
                else {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_1.present();
                }
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '14752485561',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (notification.additionalData.foreground) {
                // let yourAlert = that.alertCtrl.create({
                //   title: notification.title,
                //   message: notification.message
                // });
                // yourAlert.present();
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.pushNotify();
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
                alert("Internet is not connected... please make sure the internet connection is working properly.");
            });
            // Online event
            that.events.subscribe('network:online', function () {
                alert('network:online ==> ' + _this.networkProvider.getNetworkType());
            });
            that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // debugger;
        // Load simple menu options analytics
        // ------------------------------------------
        if (this.isDealer == true) {
            this.options.push({
                iconName: 'home',
                displayText: 'Home',
                component: 'DashboardPage',
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Groups',
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: 'Customers',
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: 'Notifications',
                component: 'AllNotificationsPage'
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Reports',
                // iconName: 'clipboard',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: 'Daily Report',
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Summary Report',
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Geofenceing Report',
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Overspeed Report',
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Ignition Report',
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Stoppage Report',
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Fuel Report',
                        component: 'FuelReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Distance Report',
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Trip Report',
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Route Violation Report',
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Speed Variation Report',
                        component: 'SpeedRepoPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Routes',
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: 'Customer Support',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: 'Rate Us',
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: 'Contact Us',
                        component: 'ContactUsPage'
                    },
                ]
            });
            this.options.push({
                displayText: 'Profile',
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            // if (this.isDealer == false) {
            this.options.push({
                iconName: 'home',
                displayText: 'Home',
                component: 'DashboardPage',
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Groups',
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Dealers',
                component: 'DashboardPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: 'Notifications',
                component: 'AllNotificationsPage'
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Reports',
                // iconName: 'clipboard',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: 'Daily Report',
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Summary Report',
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Geofenceing Report',
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Overspeed Report',
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Ignition Report',
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Stoppage Report',
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Fuel Report',
                        component: 'FuelReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Distnace Report',
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Trip Report',
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Route Violation Report',
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Speed Variation Report',
                        component: 'SpeedRepoPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Routes',
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: 'Customer Support',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: 'Rate Us',
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: 'Contact Us',
                        component: 'ContactUsPage'
                    },
                ]
            });
            this.options.push({
                displayText: 'Profile',
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        // }
        // this.events.subscribe('user:updated', data => {
        //   // if(data.isDealer)
        //   debugger;
        //   console.log("check if dealer in component=> ", data.isDealer)
        //   if (data.isDealer === false) {
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   } else {
        //     this.events.subscribe("sidemenu:event", data => {
        //       console.log("sidemenu:event=> ", data);
        //       if (data) {
        //         this.options[2].displayText = 'Dealers';
        //         this.options[2].iconName = 'person';
        //         this.options[2].component = 'DashboardPage';
        //       }
        //     });
        //   }
        // })
        // debugger;
        // this.events.subscribe('user:updated', (udata) => {
        //   this.islogin = udata;
        //   console.log("islogin=> " + JSON.stringify(this.islogin));
        //   console.log("isDealer=> ", udata.isDealer)
        //   if (udata.isDealer === false) {
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   }
        // });
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        var checkCon = localStorage.getItem("isDealervalue");
        if (checkCon != null) {
            if (checkCon == 'true') {
                console.log("console=> ", localStorage.getItem("isDealervalue"));
                this.options[2].displayText = 'Dealers';
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
            }
            else if (checkCon == 'false') {
                this.options[2].displayText = 'Customers';
                this.options[2].iconName = 'contacts';
                this.options[2].component = 'CustomersPage';
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = 'Dealers';
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                // Get the params if any
                var params = option.custom && option.custom.param;
                console.log("side menu click event=> " + option.component);
                // Redirect to the selected page
                if (localStorage.getItem("isDealervalue") && option.component == 'DashboardPage') {
                    localStorage.setItem('details', localStorage.getItem('dealer'));
                    localStorage.setItem('isDealervalue', 'false');
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": this.islogin._id,
            "token": this.token,
            "os": "android"
        };
        ///////////////////////////////
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.menuCtrl.close();
                            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        _this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
        ///////////////////////////////
    };
    MyApp.prototype.chkCondition = function () {
        // debugger;
        var _this = this;
        this.events.subscribe("event_sidemenu", function (data) {
            var sidemenuOption = JSON.parse(data);
            // console.log(data);
            console.log(sidemenuOption);
            _this.islogin = JSON.parse(data);
            console.log("islogin event publish=> " + _this.islogin);
            _this.options[2].displayText = 'Dealers';
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            // this.events.publish("sidemenu:event", this.options)
            _this.initializeOptions();
            console.log("options=> " + JSON.stringify(_this.options[2]));
        });
        // this.events.subscribe('user:updated', (udata) => {
        //   this.islogin = udata;
        //   console.log("islogin=> " + JSON.stringify(this.islogin));
        //   console.log("isDealer=> ", udata.isDealer)
        //   if (udata.isDealer === false) {
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   }
        //   this.initializeOptions();
        //   console.log(JSON.stringify(this.options))
        // });
        this.initializeOptions();
        this.dealerChk = localStorage.getItem('condition_chk');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_9__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_9__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\app\app.html"*/'<ion-menu [content]="content" (ionOpen)=\'chkCondition()\'>\n  <ion-header>\n    <!-- <ion-navbar style="background-image: linear-gradient(to right top, #d80622, #c0002d, #a60033, #8b0935, #6f1133);"> -->\n    <!-- <div class="headProf">\n      <img src="assets/imgs/dummy-user-img.png">\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n      </div>\n    </div> -->\n    <!-- </ion-navbar> -->\n  </ion-header>\n\n  <ion-content id=outerNew>\n      <div class="headProf">\n          <img src="assets/imgs/dummy-user-img.png">\n          <div>\n            <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n            <p style="font-size: 12px">\n              <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n            <p style="font-size: 12px">\n              <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n          </div>\n        </div>\n    <!-- side menu content -->\n    <side-menu-content [settings]="sideMenuSettings" [options]="options" (change)="onOptionSelected($event)"></side-menu-content>\n    <!-- <ion-list no-lines>\n      <ion-item *ngFor="let p of pages;  let i=index" (click)="openPage(p, i);" style="background-color: transparent; border-bottom: 1px solid #717775; color: #f0f0f0">\n\n        <span ion-text>\n          <ion-icon name="{{p.icon}}"> </ion-icon>\n          &nbsp;&nbsp;&nbsp;{{p.title}}\n          <ion-icon [name]="selectedMenu == i? \'arrow-dropdown-circle\' : \'arrow-dropright-circle\'" *ngIf="p.subPages" float-right></ion-icon>\n        </span>\n\n        <ion-list no-lines [hidden]="selectedMenu != i" style="margin: 0px 0 0px; padding:0px !important;">\n          <ion-item no-border *ngFor="let subPage of p.subPages;let i2=index" text-wrap (click)="openPage(subPage)" style="background-color: transparent;border-bottom: 1px solid #b9c2bf; color: #f0f0f0; padding-left: 40px;font-size: 0.8em;">\n            <span ion-text>&nbsp;{{subPage.title}}</span>\n          </ion-item>\n        </ion-list>\n\n      </ion-item>\n    </ion-list> -->\n  </ion-content>\n  <!-- <ion-footer>\n    <ion-toolbar color="light" (tap)="logout()" class="toolbarmd">\n      <ion-title>\n        <ion-icon color="danger" name="log-out"></ion-icon>&nbsp;&nbsp;&nbsp;\n        <span ion-text color="danger">Logout</span>\n      </ion-title>\n    </ion-toolbar>\n  </ion-footer> -->\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_6__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 592:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 603:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GroupModel = /** @class */ (function () {
    function GroupModel() {
    }
    GroupModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-model',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\group-model\group-model.html"*/'<ion-content>\n\n\n\n    \n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\groups\group-model\group-model.html"*/
        })
    ], GroupModel);
    return GroupModel;
}());

//# sourceMappingURL=group-model.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__live_live__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_devices_add_devices__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__history_device_history_device__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__geofence_geofence__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__all_notifications_all_notifications__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_js_chartjs_plugin_labels_min_js__ = __webpack_require__(553);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_js_chartjs_plugin_labels_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_js_chartjs_plugin_labels_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_socket_io_client__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_socket_io_client__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










 // with ES6 import
var DashboardPage = /** @class */ (function () {
    // pushnotify: any;
    // drawerOptions: any;
    function DashboardPage(events, navCtrl, navParams, apiCall) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.cartCount = 0;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        ///////////////////////////////
        this.events.publish('user:updated', this.islogin);
        /////////////////////////////////
        this.token = localStorage.getItem("DEVICE_TOKEN");
        console.log("device token dashboard=> ", this.token);
        // console.log("islogin=> " + JSON.stringify(this.islogin));
        this.uuid = localStorage.getItem('uuid');
        // console.log("uuid=> " + this.uuid);
        this.PushToken = localStorage.getItem('PushToken');
        // console.log("pushtoken=> " + this.PushToken);
        this.dealerStatus = this.islogin.isDealer;
        // console.log("dealer status => " + this.dealerStatus);
        this.to = new Date().toISOString();
        var d = new Date();
        var a = d.setHours(0, 0, 0, 0);
        this.from = new Date(a).toISOString();
        // console.log("form=> " + this.from);
        ///////////////=======Push notifications========////////////////
        this.events.subscribe('cart:updated', function (count) {
            _this.cartCount = count;
        });
        this.socket = __WEBPACK_IMPORTED_MODULE_10_socket_io_client__('https://www.oneqlik.in/notifIO', {
            transports: ['websocket', 'polling']
        });
        this.socket.on('connect', function () {
            console.log('IO Connected tabs');
            console.log("socket connected tabs", _this.socket.connected);
        });
        this.socket.on(this.islogin._id, function (msg) {
            // this.notData.push(msg);
            _this.cartCount += 1;
            // console.log("tab notice data=> " + this.notData)
        });
        ///////////////=======Push notifications========////////////////
    }
    DashboardPage.prototype.ngOnInit = function () {
        this.getDashboarddevices();
        localStorage.removeItem("LiveDevice");
    };
    DashboardPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        var that = this;
        // this.getDashboarddevices();
        that.apiCall.startLoading().present();
        that.apiCall.dashboardcall(that.islogin.email, that.from, that.to, that.islogin._id)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            ///// add push token to server //////////////
            that.addPushNotify();
            /////////////////////////////////////////////
            that.TotalDevice = [];
            that.devices = data;
            // console.log("devices => " + JSON.stringify(this.devices));
            that.Running = that.devices.running_devices;
            that.TotalDevice.push(that.Running);
            that.IDLING = that.devices["Ideal Devices"];
            that.TotalDevice.push(that.IDLING);
            that.Stopped = that.devices["OFF Devices"];
            that.TotalDevice.push(that.Stopped);
            that.Maintance = that.devices["Maintance Device"];
            that.TotalDevice.push(that.Maintance);
            that.OutOffReach = that.devices["OutOfReach"];
            that.TotalDevice.push(that.OutOffReach);
            that.NogpsDevice = that.devices["no_gps_fix_devices"];
            that.TotalDevice.push(that.NogpsDevice);
            // console.log("total devices => " + this.TotalDevice);
            that.totalVechiles = that.Running + that.IDLING + that.Stopped + that.Maintance + that.OutOffReach + that.NogpsDevice;
            // console.log(this.devices.off_ids);
            // this.getChart();
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
        refresher.complete();
        // setTimeout(() => {
        //   console.log('Async operation has ended');
        //   refresher.complete();
        // }, 2000);
    };
    DashboardPage.prototype.seeNotifications = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__all_notifications_all_notifications__["a" /* AllNotificationsPage */]);
    };
    DashboardPage.prototype.getChart = function () {
        var that = this;
        __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"].pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);
                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);
                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;
                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ["RUNNING", "IDLING", "STOPPED", "MAINTENANCE", "OUT OF REACH", "NO GPS"],
                // labels: ["RUNNING", "IDLING", "STOPPED", "MAINTENANCE", "OUT OF REACH", "NO GPS"],
                datasets: [{
                        label: '# of Votes',
                        data: this.TotalDevice,
                        backgroundColor: ['green', '#ffc900', 'red', '#d27d7d', 'blue', 'gray'],
                    }]
            },
            options: {
                plugins: {
                    labels: {
                        render: 'value',
                        fontSize: 16,
                        fontColor: '#fff',
                        images: [
                            {
                                src: 'image.png',
                                width: 16,
                                height: 16
                            }
                        ],
                        outsidePadding: 4,
                        textMargin: 4
                    }
                },
                elements: {
                    center: {
                        text: this.totalVechiles,
                        color: '#d80622',
                        fontStyle: 'Arial',
                        sidePadding: 20 // Defualt is 20 (as a percentage)
                    }
                },
                tooltips: {
                    enabled: false
                },
                pieceLabel: {
                    mode: 'value',
                    fontColor: ['white', 'white', 'white', 'white', 'white']
                },
                responsive: true,
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)',
                        fontSize: 8
                    },
                    position: 'bottom',
                },
                // scale: {
                //   pointLabels: {
                //     fontSize: "10px",
                //   }
                // },
                onClick: function (event, elementsAtEvent) {
                    // alert("alert => "+ event+ " elementsAtEvent=> "+elementsAtEvent)
                    // console.log(event)
                    // console.log(elementsAtEvent);
                    var activePoints = elementsAtEvent;
                    if (activePoints[0]) {
                        var chartData = activePoints[0]['_chart'].config.data;
                        var idx = activePoints[0]['_index'];
                        var label = chartData.labels[idx];
                        var value = chartData.datasets[0].data[idx];
                        var url = "http://example.com/?label=" + label + "&value=" + value;
                        console.log(url);
                        // alert(url);
                        // $state.go('app.addDevices', + label + "&value=" + $scope.devices.off_ids);
                        // store.set('status', label);
                        localStorage.setItem('status', label);
                        that.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_devices_add_devices__["a" /* AddDevicesPage */], {
                            label: label,
                            value: value
                        });
                    }
                }
            }
        });
    };
    DashboardPage.prototype.historyDevice = function () {
        localStorage.setItem("MainHistory", "MainHistory");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__history_device_history_device__["a" /* HistoryDevicePage */]);
    };
    DashboardPage.prototype.opennotify = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__all_notifications_all_notifications__["a" /* AllNotificationsPage */]);
    };
    DashboardPage.prototype.addPushNotify = function () {
        // var pushdata = {
        //   "uid": this.islogin._id,
        //   "token": this.token,
        //   "imei": this.islogin._id,
        //   "os": "android"
        // }
        var that = this;
        that.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": that.islogin._id,
            "token": that.token,
            "os": "android"
        };
        // console.log("pushdata=> " + JSON.stringify(pushdata));
        that.apiCall.pushnotifyCall(pushdata)
            .subscribe(function (data) {
            console.log("push notifications updated " + data.message);
            // this.pushnotify = data.message;
            // console.log("data=> " + JSON.stringify(data));
        }, function (error) {
            console.log(error);
        });
    };
    DashboardPage.prototype.getDashboarddevices = function () {
        var _this = this;
        console.log("getdevices");
        this.apiCall.startLoading().present();
        this.apiCall.dashboardcall(this.islogin.email, this.from, this.to, this.islogin._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            ///// add push token to server //////////////
            _this.addPushNotify();
            /////////////////////////////////////////////
            _this.TotalDevice = [];
            _this.devices = data;
            // console.log("devices => " + JSON.stringify(this.devices));
            _this.Running = _this.devices.running_devices;
            _this.TotalDevice.push(_this.Running);
            _this.IDLING = _this.devices["Ideal Devices"];
            _this.TotalDevice.push(_this.IDLING);
            _this.Stopped = _this.devices["OFF Devices"];
            _this.TotalDevice.push(_this.Stopped);
            _this.Maintance = _this.devices["Maintance Device"];
            _this.TotalDevice.push(_this.Maintance);
            _this.OutOffReach = _this.devices["OutOfReach"];
            _this.TotalDevice.push(_this.OutOffReach);
            _this.NogpsDevice = _this.devices["no_gps_fix_devices"];
            _this.TotalDevice.push(_this.NogpsDevice);
            // console.log("total devices => " + this.TotalDevice);
            _this.totalVechiles = _this.Running + _this.IDLING + _this.Stopped + _this.Maintance + _this.OutOffReach + _this.NogpsDevice;
            // console.log(this.devices.off_ids);
            _this.getChart();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    DashboardPage.prototype.getStopdevices = function () {
        var _this = this;
        console.log("getStopdevices");
        this.apiCall.startLoading().present();
        this.apiCall.stoppedDevices(this.islogin._id, this.islogin.email, this.devices.off_ids)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices = data;
            // console.log("devices 1 => " + this.devices);
            _this.devices1243 = [];
            _this.devices = data.devices.reverse();
            _this.devices1243.push(data);
            // console.log("devices1243=> " + this.devices1243);
            _this.devices = _this.devices;
            // console.log(this.devices);
            localStorage.setItem('devices', _this.devices);
            _this.isdevice = localStorage.getItem('devices');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
            console.log(_this.islogin);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    DashboardPage.prototype.vehiclelist = function () {
        console.log("coming soon");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_devices_add_devices__["a" /* AddDevicesPage */]);
    };
    DashboardPage.prototype.live = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__live_live__["a" /* LivePage */]);
    };
    DashboardPage.prototype.geofence = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__geofence_geofence__["a" /* GeofencePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], DashboardPage.prototype, "doughnutCanvas", void 0);
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dashboard',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\dashboard\dashboard.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Welcome, {{islogin.fn}}!</ion-title>\n    <ion-buttons end>\n      <button id="notification-button" ion-button clear (tap)="seeNotifications()">\n        <ion-icon name="notifications" style="font-size: 20px; color: #fff">\n          <ion-badge id="notifications-badge" color="danger">{{cartCount}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content overflow-scroll="false">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card>\n    <ion-card-content>\n      <canvas #doughnutCanvas height="300" width="300"></canvas>\n    </ion-card-content>\n  </ion-card>\n  <ion-card>\n    <ion-card-content class="cardSt">\n      <ion-row>\n        <ion-col width-50 (tap)="live()" style="border-right: 1px solid #000064; border-bottom: 1px solid #000064;">\n          <div class="divS">\n            <ion-icon class="iconS" name="pin" color="gpsc"></ion-icon>\n            <br/> LIVE TRACKING\n          </div>\n        </ion-col>\n\n        <ion-col width-50 (tap)="vehiclelist()" style="border-bottom: 1px solid #000064">\n          <div class="divS">\n            <ion-icon class="iconS" name="car" color="gpsc"></ion-icon>\n            <br/> VEHICLES\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col width-50 (tap)="historyDevice()" style="border-right: 1px solid #000064;">\n\n          <div class="divS">\n            <ion-icon class="iconS" name="time" color="gpsc"></ion-icon>\n            <br/> HISTORY\n          </div>\n        </ion-col>\n        <ion-col width-50 (tap)="geofence()" class="colStyl">\n          <div class="divS">\n            <ion-icon class="iconS" name="disc" color="gpsc"></ion-icon>\n            <br/> GEOFENCE\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\dashboard\dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__mobile_verify__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    // loading: any;
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.showPassword = false;
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        // if (this.loginForm.valid) {
        //   console.log(this.loginForm.value);
        //   var UserName = isNaN(this.loginForm.value.username);
        //   if (UserName == false) {
        //     this.data = {
        //       "psd": this.loginForm.value.password,
        //       "ph_num": this.loginForm.value.username
        //     };
        //   } else {
        //     this.data = {
        //       "psd": this.loginForm.value.password,
        //       "emailid": this.loginForm.value.username
        //     };
        //   }
        if (this.loginForm.value.username == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.loginForm.value.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.loginForm.value.username);
        var isName = isNaN(this.loginForm.value.username);
        //var isEmail = validateEmail($scope.data.username);
        //var isMobile = validatePhone(parseInt($scope.data.username));
        if (isEmail == false && isName == false) {
            this.data = {
                "psd": this.loginForm.value.password,
                "ph_num": this.loginForm.value.username
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.loginForm.value.password,
                "emailid": this.loginForm.value.username
            };
        }
        else {
            this.data = {
                "psd": this.loginForm.value.password,
                "user_id": this.loginForm.value.username
            };
        }
        this.apiservice.startLoading();
        this.apiservice.loginApi(this.data)
            .subscribe(function (response) {
            _this.logindata = response;
            _this.logindata = JSON.stringify(response);
            var logindetails = JSON.parse(_this.logindata);
            _this.userDetails = window.atob(logindetails.token.split('.')[1]);
            _this.details = JSON.parse(_this.userDetails);
            console.log(_this.details.email);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(_this.details));
            localStorage.setItem('condition_chk', _this.details.isDealer);
            // $rootScope.islogin = store.get('details');
            // console.log( $rootScope.islogin);
            // $scope.init();
            _this.apiservice.stopLoading();
            var toast = _this.toastCtrl.create({
                message: "Welcome! You're logged In successfully.",
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__["a" /* DashboardPage */]);
            });
            toast.present();
            _this.responseMessage = "LoggedIn  successfully";
        }, function (error) {
            // console.log("login error => "+error)
            var body = error._body;
            var msg = JSON.parse(body);
            if (msg.message == "Mobile Phone Not Verified") {
                var confirmPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: 'Ok',
                            handler: function () {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__mobile_verify__["a" /* MobileVerify */]);
                            }
                        }
                    ]
                });
                confirmPopup.present();
            }
            else {
                // Do something on error
                var alertPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alertPopup.present();
            }
            _this.responseMessage = "Something Wrong";
            _this.apiservice.stopLoading();
        });
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.forgotPassFunc = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Forgot Password',
            message: "Enter your Registered  Mobile Number we will send you a confirmation code",
            inputs: [
                {
                    name: 'mobno',
                    placeholder: 'Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'SEND CONFIRMATION CODE',
                    handler: function (data) {
                        // console.log('Saved clicked');
                        // console.log(data.mobno);
                        var forgotdata = {
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassApi(forgotdata)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.otpMess = data;
                            console.log(_this.otpMess);
                            _this.PassWordConfirmPopup();
                            _this.responseMessage = "sent code successfully";
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: "Forgot Password Failed!",
                                message: msg.message,
                                buttons: ['OK']
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.PassWordConfirmPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Reset Password',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'newpass',
                    placeholder: 'Password'
                },
            ],
            buttons: [
                {
                    text: 'Back',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'SAVE',
                    handler: function (data) {
                        console.log('Saved clicked');
                        if (!data.newpass || !data.mobilenum || !data.confCode) {
                            var alertPopup = _this.alertCtrl.create({
                                title: 'Warning!',
                                message: "Fill all mandatory fields!",
                                buttons: ['OK']
                            });
                            alertPopup.present();
                        }
                        else {
                            if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                                if (data.newpass.length < 6 || data.newpass.length > 12) {
                                    var Popup = _this.alertCtrl.create({
                                        title: 'Warning!',
                                        message: "Password length should be 6 - 12",
                                        buttons: ['OK']
                                    });
                                    Popup.present();
                                }
                                else {
                                    var Passwordset = {
                                        "newpwd": data.newpass,
                                        "otp": data.confCode,
                                        "phone": _this.otpMess,
                                        "cred": _this.otpMess
                                    };
                                    _this.apiservice.startLoading();
                                    _this.apiservice.forgotPassMobApi(Passwordset)
                                        .subscribe(function (data) {
                                        _this.apiservice.stopLoading();
                                        _this.presentToast(data.message);
                                        _this.navCtrl.setRoot(LoginPage_1);
                                        _this.responseMessage = "password changed successfully";
                                    }, function (error) {
                                        _this.apiservice.stopLoading();
                                        var body = error._body;
                                        var msg = JSON.parse(body);
                                        var alert = _this.alertCtrl.create({
                                            title: "Forgot Password failed!",
                                            message: msg.message,
                                            buttons: ['OK']
                                        });
                                        alert.present();
                                        _this.responseMessage = "Something Wrong";
                                    });
                                }
                            }
                            else {
                                var alertPopup = _this.alertCtrl.create({
                                    title: 'Warning!',
                                    message: "New Password and Confirm Password Not Matched",
                                    buttons: ['OK']
                                });
                                alertPopup.present();
                            }
                            if (!data.newpass || !data.mobilenum || !data.confCode) {
                                //don't allow the user to close unless he enters model...
                                return false;
                            }
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.gotosignuppage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\IONIC_PROJECTS\MrTrack\src\pages\login\login.html"*/'<ion-content id=outer>\n  <div class="warning">\n    <!-- <div id=inner> -->\n    <div class="logo">\n      <!-- <img src="assets/imgs/icon.png"> -->\n    </div>\n\n    <form [formGroup]="loginForm">\n      <div class="temp">\n        <ion-item class="logitem">\n          <ion-input formControlName="username" type="text" placeholder="Email/Mobile*"></ion-input>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n        <p>username required!</p>\n      </ion-item>\n      <div class="temp">\n        <ion-item class="logitem">\n          <ion-input formControlName="password" *ngIf="!showPassword" type="password" placeholder="Password*"></ion-input>\n          <ion-input formControlName="password" *ngIf="showPassword" type="text" placeholder="Password*"></ion-input>\n\n          <button ion-button clear item-end (click)="toggleShowPassword()">\n            <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n            <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n          </button>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n        <p>password required!</p>\n      </ion-item>\n    </form>\n    <div class="btnDiv">\n      <button ion-button class="btnLog" color="gpsc" (tap)="userlogin()">SIGN IN</button>\n      <ion-row>\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;" (tap)="forgotPassFunc()">Forgot Password?</ion-col>\n        <!-- <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">New user?&nbsp;&nbsp;\n          <span style="color:#14a4ce;" (tap)="gotosignuppage()">SIGNUP</span>\n        </ion-col> -->\n      </ion-row>\n    </div>\n    <!-- </div> -->\n  </div>\n</ion-content>\n\n\n<!-- <ion-content class="mainDiv">\n  <div class="logo">\n    <img src="assets/imgs/icon.png">\n  </div>\n\n  <form [formGroup]="loginForm">\n    <ion-item class="logitem">\n        \n      <ion-input formControlName="username" type="text" placeholder="Email/Mobile*"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n      <p>username required!</p>\n    </ion-item>\n    <ion-item class="logitem">\n        \n      <ion-input formControlName="password" type="password" placeholder="Password*" ></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n      <p>password required!</p>\n    </ion-item>\n  </form>\n  <div class="btnDiv">\n    <button ion-button block color="gpsc" (tap)="userlogin()">SIGN IN</button>\n    <ion-row>\n      <ion-col width-50 ion-text color="danger" style="font-size: 1.1em;" (tap)="forgotPassFunc()">Forgot Password?</ion-col>\n      <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">New user?&nbsp;&nbsp;<span style="color:#14a4ce;" (tap)="gotosignuppage()">SIGNUP</span></ion-col>\n    </ion-row>\n  </div>\n</ion-content> -->'/*ion-inline-end:"D:\IONIC_PROJECTS\MrTrack\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[427]);
//# sourceMappingURL=main.js.map