/**
 * Automatically generated file. DO NOT MODIFY
 */
package mrtrackgps.ionic.starter;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "mrtrackgps.ionic.starter";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 104001;
  public static final String VERSION_NAME = "1.4";
}
