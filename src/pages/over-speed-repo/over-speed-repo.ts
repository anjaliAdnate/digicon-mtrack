import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';


@IonicPage()
@Component({
  selector: 'page-over-speed-repo',
  templateUrl: 'over-speed-repo.html',
})
export class OverSpeedRepoPage implements OnInit {


  overSpeeddevice_id: any;
  overspeedReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  isdevice: string;
  datetime: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalloverspeed: ApiServiceProvider, public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();
    var temp = new Date();

    // this.datetimeStart = temp.toISOString();
    var settime = temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime = new Date(settime).setHours(5, 30, 0);

    this.datetimeStart = new Date(this.datetime).toISOString();

    var a = new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes() + 30);
    this.datetimeEnd = new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getOverspeeddevice(selectedVehicle) {
    // console.log("selectedVehicle=> ", selectedVehicle)
    this.overSpeeddevice_id = selectedVehicle._id;
    // console.log("selected vehicle=> " + this.overSpeeddevice_id)
  }

  getdevices() {
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicalloverspeed.stopLoading();
          console.log(err);
        });
  }


  getOverSpeedReport(starttime, endtime) {
    var baseUrl;
    if (this.overSpeeddevice_id == undefined) {
      baseUrl = "http://51.38.175.41/notifs/overSpeedReport?from_date=" + starttime + '&to_date=' + endtime + '&_u=' + this.islogin._id

    } else {
      baseUrl = "http://51.38.175.41/notifs/overSpeedReport?from_date=" + starttime + '&to_date=' + endtime + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id
    }


    this.apicalloverspeed.startLoading().present();

    this.apicalloverspeed.getOverSpeedApi(baseUrl)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.overspeedReport = data;
        console.log(this.overspeedReport);
        if (this.overspeedReport.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicalloverspeed.stopLoading();
        console.log(error);
      })
  }

}
