import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddGeofencePage } from './add-geofence';

@NgModule({
  declarations: [
    AddGeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(AddGeofencePage),
  ],
})
export class AddGeofencePageModule {}
