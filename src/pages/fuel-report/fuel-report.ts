import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';


@IonicPage()
@Component({
  selector: 'page-fuel-report',
  templateUrl: 'fuel-report.html',
})
export class FuelReportPage implements OnInit{
  

  fuelReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  datetime: number;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicallFuel: ApiServiceProvider,public alertCtrl: AlertController,) {
      this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();

    var temp = new Date();
    
    // this.datetimeStart = temp.toISOString();
    var settime= temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime=new Date(settime).setHours(5,30,0);

     this.datetimeStart =new Date(this.datetime).toISOString();
     
     var a= new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes()+30);
    this.datetimeEnd = new Date(a).toISOString();
  }



  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }


  ngOnInit(){
    this.getdevices();
  }


  getdevices() {
    this.apicallFuel.startLoading().present();
    this.apicallFuel.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicallFuel.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicallFuel.stopLoading();
          console.log(err);
        });
  }

  getFueldevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
    // this.getIgnitiondeviceReport(from, to);
  }

  
  getFuelReport(starttime, endtime) {

    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";

    }
    console.log(starttime);
    console.log(endtime);

    this.apicallFuel.startLoading().present();

    this.apicallFuel.getFuelApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id,)
      .subscribe(data => {
        this.apicallFuel.stopLoading();
        this.fuelReport = data;
        console.log(this.fuelReport);
         
        if(this.fuelReport.length==0){
        let alert = this.alertCtrl.create({
          message: "No Data Found",
          buttons: ['OK']
        });
        alert.present();
      }

      }, error => {
        this.apicallFuel.stopLoading();
        console.log(error);
      })
  }


}
