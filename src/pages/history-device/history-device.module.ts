import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryDevicePage } from './history-device';
import { DatePicker } from '@ionic-native/date-picker';
// import { DatePickerModule } from 'ion-datepicker';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    HistoryDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryDevicePage),
    // DatePickerModule,
    SelectSearchableModule
  ],
  providers: [DatePicker]
})
export class HistoryDevicePageModule {}
