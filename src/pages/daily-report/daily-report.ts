import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})
export class DailyReportPage implements OnInit {

  deviceReport: any;
  deviceReportSearch: any = [];

  to: string;
  from: string;
  islogin: any;
  todaydate: Date;
  todaytime: string;
  datetime: number;

  page: number = 0;
  limit: number = 6;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicalldaily: ApiServiceProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin.email);
    console.log("id => " + this.islogin._id);


    // this.datetime = new Date().setHours(0, 0, 0, 0)
    // console.log(this.datetime);

    // this.from = new Date(new Date().setHours(0,0,0,0)).toISOString();

    //  console.log(this.from );
    // // this.todaytime = this.todaydate.toLocaleString();
    // // console.log(this.todaytime.toLocaleString());

    // this.to = new Date().toISOString()

    // console.log("from=> " +this.from);

    // var temp = new Date();

    // // this.datetimeStart = temp.toISOString();
    // var settime = temp.getTime();
    // // this.datetime=new Date(settime).setMinutes(0);
    // this.datetime = new Date(settime).setHours(5, 30, 0);

    // this.from = new Date(this.datetime).toISOString();

    // var a = new Date()
    // a.setHours(a.getHours() + 5);
    // a.setMinutes(a.getMinutes() + 30);
    // this.to = new Date(a).toISOString();
  
    
    var temp = new Date();

    // this.datetimeStart = temp.toISOString();
    //var settime = temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    //var datetime = new Date(settime).setHours(5, 30, 0);

    this.from = moment({hours:0}).format();

    console.log('start date',this.from)

    // var a = new Date()
    // a.setHours(a.getHours() + 5);
    // a.setMinutes(a.getMinutes() + 30);
    this.to = moment().format();//new Date(a).toISOString();
    console.log('start date',this.to);
  }

  ngOnInit() {
    this.getDailyReportData();
  }

  getItems(ev: any) {
    console.log(ev.target.value, this.deviceReport)
    const val = ev.target.value.trim();
    this.deviceReportSearch = this.deviceReport.filter((item) => {
      return (item.device.Device_ID.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.deviceReportSearch);
  }


  getDailyReportData() {
    this.page = 0;
    //  console.log("chengeddate=> ", chengeddate)
    // var chengeddate = this.from;
    console.log("from date=> ", this.from)
    console.log("to date=> ", this.to)
    this.apicalldaily.startLoading().present();
    this.apicalldaily.getDailyReport(this.islogin.email, this.islogin._id, new Date(this.from).toISOString(),new Date(this.to).toISOString(), this.page, this.limit)
      .subscribe(data => {
        this.apicalldaily.stopLoading();
        // this.deviceReport = data.data;
         this.deviceReport = data;
        this.deviceReportSearch = data;
        console.log("daily report data=> " + JSON.stringify(this.deviceReport.data));

      }, error => {
        this.apicalldaily.stopLoading();
        console.log("error in service=> "+ error);
      })
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      // that.apicalldaily.startLoading().present();
      that.apicalldaily.getDailyReport(that.islogin.email, that.islogin._id, that.from, that.to, that.page, that.limit)
        .subscribe(
          res => {
            // that.apicalldaily.stopLoading();
            // for (let i = 0; i < res.data.length; i++) {
            //   that.deviceReport.push(res.data[i]);
            //   that.deviceReportSearch.push(res.data[i]);
            // }
            for (let i = 0; i < res.length; i++) {
              that.deviceReport.push(res[i]);
             // that.deviceReportSearch.push(res[i]);
            }
            that.deviceReportSearch=that.deviceReport;
          },
          error => {
            // that.apicalldaily.stopLoading();
            console.log(error);
          });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }
}
