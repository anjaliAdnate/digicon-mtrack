import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

/**
 * Generated class for the RouteVoilationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-route-voilations',
  templateUrl: 'route-voilations.html',
})
export class RouteVoilationsPage implements OnInit{
  
  islogin: any;
  datetimeStart: string;
  datetimeEnd: string;
  devices1243: any[];
  routelist: any;
  routevolitionReport: any;
  routename_id: any;
  datetime: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,public apicallroute:ApiServiceProvider,
    public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();
    var temp = new Date();
    
    // this.datetimeStart = temp.toISOString();
    var settime= temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime=new Date(settime).setHours(5,30,0);

     this.datetimeStart =new Date(this.datetime).toISOString();
     
     var a= new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes()+30);
    this.datetimeEnd = new Date(a).toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RouteVoilationsPage');
  }


  ngOnInit() {
    this.getRoute();
    }
  
  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getRoute(){
   
    var baseURLp = 'http://51.38.175.41/trackRoute/user/'+this.islogin._id;
    this.apicallroute.startLoading().present();
    this.apicallroute.getallrouteCall(baseURLp)
      .subscribe(data => {
        this.apicallroute.stopLoading();
        this.devices1243 = [];
        this.routelist = data;
        console.log("Routelist=> ", this.routelist)

      },
        err => {
          this.apicallroute.stopLoading();
          console.log(err)
        });
  }

  getRouteName(from, to, selectedroute){
    console.log("selectedVehicle=> ", selectedroute)
    this.routename_id= selectedroute.Device_Name;
  }


  getroutevoilation(starttime, endtime){
    var baseURLp ='http://51.38.175.41/notifs/RouteVoilationReprot?from_date='+starttime +'&to_date='+endtime +'&_u='+this.islogin._id;

    this.apicallroute.startLoading().present();

    this.apicallroute.getallrouteCall(baseURLp)
      .subscribe(data => {
        this.apicallroute.stopLoading();
        this.routevolitionReport = data;
        console.log(this.routevolitionReport);
        if(this.routevolitionReport.length==0){
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicallroute.stopLoading();
        console.log(error);
      })

  }
}
