import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { LoginPage } from '../login/login';
import { AppVersion } from '@ionic-native/app-version';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  islogin: any;
  token: string;
  aVer: any;
  isDealer: boolean;

  constructor(private appVersion: AppVersion, public apiCall: ApiServiceProvider, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.appVersion.getVersionNumber().then((version) => {
      this.aVer = version;
      console.log("app version=> " + this.aVer);
    });
    this.isDealer = this.islogin.isDealer;
    console.log("isDealer=> " + this.isDealer)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  service() {
    this.navCtrl.push(ServiceProviderPage, {
      param: this.islogin
    })
  }

  password() {
    this.navCtrl.push(UpdatePasswordPage, {
      param: this.islogin
    })
  }

  logout() {
    this.token = localStorage.getItem("DEVICE_TOKEN");
    var pushdata = {
      "uid": this.islogin._id,
      "token": this.token,
      "os": "android"
    }

    let alert = this.alertCtrl.create({
      message: 'Do you want to logout from the application?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          // localStorage.clear();
          // localStorage.setItem('count', null)
          // // this.menuCtrl.close();
          // this.navCtrl.setRoot(LoginPage);
          this.apiCall.startLoading().present();
          this.apiCall.pullnotifyCall(pushdata)
            .subscribe(data => {
              this.apiCall.stopLoading();
              console.log("push notifications updated " + data.message)
              localStorage.clear();
              localStorage.setItem('count', null)
              // this.menuCtrl.close();
              this.navCtrl.setRoot(LoginPage);

            },
              err => {
                this.apiCall.stopLoading();
                console.log(err)
              });
        }
      },
      {
        text: 'No',
        handler: () => {
          // this.menuCtrl.close();
        }
      }]
    });
    alert.present();
  }



}

@Component({
  templateUrl: './service-provider.html',
  selector: 'page-profile'
})

export class ServiceProviderPage {
  uData: any;
  constructor(public navParam: NavParams) {
    this.uData = this.navParam.get("param");
  }
}

@Component({
  selector: 'page-profile',
  templateUrl: './update-password.html'
})

export class UpdatePasswordPage {
  passData: any;
  cnewP: any;
  newP: any;
  oldP: any;
  constructor(public navParam: NavParams, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    this.passData = this.navParam.get("param")
  }

  savePass() {
    if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
      let alert = this.alertCtrl.create({
        message: 'Fields should not be empty!',
        buttons: ['OK']
      });
      alert.present();
    } else {
      if (this.newP != this.cnewP) {
        let alert = this.alertCtrl.create({
          message: 'Password Missmatched!!',
          buttons: ['Try Again']
        });
        alert.present();
      } else {
        const toast = this.toastCtrl.create({
          message: 'Password Updated successfully',
          duration: 2000
        });
        toast.onDidDismiss(() => {
          this.oldP = "";
          this.newP = "";
          this.cnewP = "";
        });
        toast.present();
      }
    }

  }
}
