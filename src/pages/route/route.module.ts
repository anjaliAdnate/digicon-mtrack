import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoutePage } from './route';
// import { CreateRoutePage } from '../create-route/create-route';

@NgModule({
  declarations: [
    RoutePage,
    // CreateRoutePage
  ],
  imports: [
    IonicPageModule.forChild(RoutePage),
  ],
  exports: [
    // CreateRoutePage
  ]
})
export class RoutePageModule {}
