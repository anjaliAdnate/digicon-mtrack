import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import * as io from 'socket.io-client';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular';
import { AddDeviceModalPage } from './modals/add-device-modal';
import { UpdateCustModalPage } from './modals/update-cust/update-cust';
import { AddCustomerModal } from './modals/add-customer-modal/add-customer-modal';
import { DashboardPage } from '../dashboard/dashboard';
// import { CustomersPageModule } from './customers.module';
// declare var angular;

@IonicPage()
@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
})
export class CustomersPage implements OnInit {
  islogin: any;
  setsmsforotp: any;
  isSuperAdminStatus: any;
  customerslist: any;
  CustomerArray: any;
  CustomerArraySearch: any = [];

  CratedeOn: string;
  CustomerData: any;
  time: string;
  date: string;
  customer: any;
  customersignups: any;
  DeletedDevice: any;
  viewCtrl: any;
  isDealer: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    public events: Events) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("_id=> " + this.islogin._id);
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isSuperAdminStatus = this.islogin.isSuperAdmin;
    console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
    this.isDealer = this.islogin.isDealer
    console.log("isDealer=> " + this.isDealer);
  }

  ngOnInit() {
    this.getcustomer();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getcustomer();
    refresher.complete();
    // setTimeout(() => {
    //   console.log('Async operation has ended');
    //   refresher.complete();
    // }, 2000);
  }

  getItems(ev: any) {
    console.log(ev.target.value, this.CustomerArray)
    const val = ev.target.value.trim();
    this.CustomerArraySearch = this.CustomerArray.filter((item) => {
      return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.CustomerArraySearch);
  }

  getcustomer() {
    console.log("getcustomer");
    var baseURLp = 'http://51.38.175.41/users/getCust?uid=' + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getCustomersCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        // this.customerslist = data.reverse();
        this.CustomerData = data;
        console.log("CustomerData=> " + JSON.stringify(this.CustomerData));
        // console.log("customerlist=> ", this.customerslist)
        this.CustomerArray = [];

        for (var i = 0; i < this.CustomerData.length; i++) {
          this.CratedeOn = JSON.stringify(this.CustomerData[i].created_on).split('"')[1].split('T')[0];
          var gmtDateTime = moment.utc(JSON.stringify(this.CustomerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
          var gmtDate = moment.utc(JSON.stringify(this.CustomerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");

          this.time = gmtDateTime.local().format(' h:mm:ss a');
          this.date = gmtDate.format('DD/MM/YYYY');

          this.CustomerArray.push({ '_id': this.CustomerData[i]._id, 'first_name': this.CustomerData[i].first_name, 'last_name': this.CustomerData[i].last_name, 'email': this.CustomerData[i].email, 'phone': this.CustomerData[i].phone, 'date': this.date, 'status': this.CustomerData[i].status, 'pass': this.CustomerData[i].pass, 'total_vehicle': this.CustomerData[i].total_vehicle, 'userid': this.CustomerData[i].userid, 'address': this.CustomerData[i].address, 'dealer_firstname': this.CustomerData[i].dealer_firstname });
          // this.CustomerArraySearch.push({ '_id': this.CustomerData[i]._id, 'first_name': this.CustomerData[i].first_name, 'last_name': this.CustomerData[i].last_name, 'email': this.CustomerData[i].email, 'phone': this.CustomerData[i].phone, 'date': this.date, 'status': this.CustomerData[i].status, 'pass': this.CustomerData[i].pass, 'total_vehicle': this.CustomerData[i].total_vehicle, 'userid': this.CustomerData[i].userid, 'address': this.CustomerData[i].address, 'dealer_firstname': this.CustomerData[i].dealer_firstname });
        }
        this.CustomerArraySearch = this.CustomerArray;

      },
        err => {
          this.apiCall.stopLoading();
          console.log("error found=> " + err);
        });
  }

  CustomerStatus(Customersdeta) {
    console.log("status=> " + Customersdeta.status)
    console.log(Customersdeta._id)
    console.log(this.islogin._id);
    var msg;
    if (Customersdeta.status) {
      msg = 'Do you want to Deactivate this customer ?'
    } else {
      msg = 'Do you want to Activate this customer ?'
    }
    let alert = this.alerCtrl.create({
      message: msg,
      buttons: [{
        text: 'YES',
        handler: () => {
          this.user_status(Customersdeta);
        }
      },
      {
        text: 'NO',
        handler: () => {
          this.getcustomer();
        }
      }]
    });
    alert.present();
  }

  user_status(Customersdeta) {
    var stat;
    if (Customersdeta.status) {
      stat = false;
    } else {
      stat = true;
    }

    var data = {
      "uId": Customersdeta._id,
      "loggedIn_id": this.islogin._id,
      "status": stat
    };
    this.apiCall.startLoading().present();
    this.apiCall.user_statusCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.DeletedDevice = data;
        console.log("DeletedDevice=> " + this.DeletedDevice)
        let toast = this.toastCtrl.create({
          message: 'Customer was updated successfully',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getcustomer();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error => ", err)
          // var body = err._body;
          // var msg = JSON.parse(body);
          // let alert = this.alerCtrl.create({
          //   title: 'Oops!',
          //   message: msg.message,
          //   buttons: ['OK']
          // });
          // alert.present();
        });
  }

  openupdateCustomersModal(Customersdetails) {
    console.log('Opening Modal openAdddeviceModal');
    this.customer = Customersdetails;
    // console.log(this.customer)
    let modal = this.modalCtrl.create(UpdateCustModalPage, {
      param: this.customer
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      // this.getcustomer();
      this.navCtrl.setRoot(CustomersPage)
    });
    modal.present();
  }

  openAdddeviceModal(Customersdetails) {
    console.log('Opening Modal openAdddeviceModal');
    this.customer = Customersdetails;
    console.log("customers=> ", this.customer)
    console.log(this.customer._id)
    console.log(this.customer.email)

    let profileModal = this.modalCtrl.create(AddDeviceModalPage, { custDet: this.customer });
    profileModal.onDidDismiss(data => {
      console.log("vehData=> " + JSON.stringify(data));
      this.getcustomer();
    });
    profileModal.present();

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }


  openAddCustomerModal() {
    let modal = this.modalCtrl.create(AddCustomerModal);
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.getcustomer();
    })
    modal.present();
  }

  switchUser(cust_id) {
    //debugger;
    console.log(cust_id)
    localStorage.setItem('isDealervalue', 'true');
    // $rootScope.dealer = $rootScope.islogin;
    localStorage.setItem('dealer', JSON.stringify(this.islogin));

    localStorage.setItem('custumer_status', 'OFF');
    localStorage.setItem('dealer_status', 'ON');

    this.apiCall.getcustToken(cust_id)
      .subscribe(res => {
        console.log('UserChangeObj=>', res)
        var custToken = res;

        var logindata = JSON.stringify(custToken);
        var logindetails = JSON.parse(logindata);
        var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
        // console.log('token=>', logindata);

        var details = JSON.parse(userDetails);
        // console.log(details.isDealer);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(details));

        var dealerSwitchObj = {
          "logindata": logindata,
          "details": userDetails,
          'condition_chk': details.isDealer
        }

        var temp = localStorage.getItem('isDealervalue');
        console.log("temp=> ", temp);
        this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
        this.events.publish("sidemenu:event", temp);
        this.navCtrl.setRoot(DashboardPage);

      }, err => {
        console.log(err);
      })
  }

}
