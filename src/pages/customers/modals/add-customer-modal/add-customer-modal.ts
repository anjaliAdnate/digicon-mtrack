import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, AlertController, ViewController, ToastController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";

@Component({
    selector: 'page-add-customer-model',
    templateUrl: './add-customer-modal.html'
})
export class AddCustomerModal implements OnInit {

    addcustomerform: FormGroup;
    customerdata: any = {};
    Customeradd: any;
    submitAttempt: boolean;
    selectDealer: any;
    isSuperAdminStatus: boolean;
    islogin: any;
    dealerdata: any;
    // isDealer: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public apicallCustomer: ApiServiceProvider,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin
        console.log("isDealer=> " + this.isSuperAdminStatus);

        this.addcustomerform = formBuilder.group({
            userId: ['', Validators.required],
            Firstname: ['', Validators.required],
            LastName: ['', Validators.required],
            emailid: ['', Validators.required],
            contact_num: ['', Validators.required],
            password: ['', Validators.required],
            confpassword: [''],
            address: ['', Validators.required],
            dealer_firstname: ['']
        })
    }

    ngOnInit() {
        this.getAllDealers();
    }

    dealerOnChnage(dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    }

    addcustomer() {
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {

            this.customerdata = {
                "first_name": this.addcustomerform.value.Firstname,
                "last_name": this.addcustomerform.value.LastName,
                "email": this.addcustomerform.value.emailid,
                "phone": this.addcustomerform.value.contact_num,
                "password": this.addcustomerform.value.password,
                "isDealer": false,
                "custumer": true,
                "status": true,
                "user_id": this.addcustomerform.value.userId,
                "address": this.addcustomerform.value.address,
            }

            if (this.dealerdata != undefined) {
                this.customerdata.Dealer = this.dealerdata.dealer_id;
            } else {
                this.customerdata.Dealer = this.islogin._id;
            }

            this.apicallCustomer.startLoading().present();
            this.apicallCustomer.addcustomerCall(this.customerdata)
                .subscribe(data => {
                    this.apicallCustomer.stopLoading();
                    this.Customeradd = data;

                    console.log("devicesadd=> ", this.Customeradd)
                    let toast = this.toastCtrl.create({
                        message: 'Customer was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss();
                    });

                    toast.present();
                },
                    err => {
                        this.apicallCustomer.stopLoading();
                        var body = err._body;
                        console.log(body)
                        var msg = JSON.parse(body);
                        console.log(msg)
                        var namepass =[];
                        namepass = msg.split(":");
                        var name = namepass[1];
                        
                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: name,
                            buttons: ['OK']
                        });
                        alert.present();
                        console.log(err);
                    });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    getAllDealers() {
        console.log("get dealer");

        var baseURLp = 'http://51.38.175.41/users/getAllDealerVehicles';

        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.selectDealer = data;
                console.log(this.selectDealer);
            },
                error => {
                    console.log(error)
                });
    }


}



