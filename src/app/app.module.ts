import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AboutUsPageModule } from '../pages/about-us/about-us.module';
// import { TripReportShowPageModule } from '../pages/trip-report-show/trip-report-show.module';
import { TripReportPageModule } from '../pages/trip-report/trip-report.module';
// import { TripPageModule } from '../pages/trip/trip.module';
import { SupportPageModule } from '../pages/support/support.module';
import { StoppagesRepoPageModule } from '../pages/stoppages-repo/stoppages-repo.module';
import { SpeedRepoPageModule } from '../pages/speed-repo/speed-repo.module';
// import { ShowTripPageModule } from '../pages/show-trip/show-trip.module';
// import { ShowRoutePageModule } from '../pages/show-route/show-route.module';
// import { ShowMapNearmePageModule } from '../pages/show-map-nearme/show-map-nearme.module';
import { ShowGeofencePageModule } from '../pages/show-geofence/show-geofence.module';
import { RouteVoilationsPageModule } from '../pages/route-voilations/route-voilations.module';
// import { RouteMapShowPageModule } from '../pages/route-map-show/route-map-show.module';
// import { RouteMapPageModule } from '../pages/route-map/route-map.module';
import { OverSpeedRepoPageModule } from '../pages/over-speed-repo/over-speed-repo.module';
// import { OdoPageModule } from '../pages/odo/odo.module';

import { LoginPageModule } from '../pages/login/login.module';
// import { AcReportPageModule } from '../pages/ac-report/ac-report.module';
// import { AddCustomerPageModule } from '../pages/add-customer/add-customer.module';
import { AddGeofencePageModule } from '../pages/add-geofence/add-geofence.module';
import { AllNotificationsPageModule } from '../pages/all-notifications/all-notifications.module';
import { ContactUsPageModule } from '../pages/contact-us/contact-us.module';
// import { CreateRoutePageModule } from '../pages/create-route/create-route.module';
import { CustomersPageModule } from '../pages/customers/customers.module';
import { DailyReportPageModule } from '../pages/daily-report/daily-report.module';
import { DashboardPageModule } from '../pages/dashboard/dashboard.module';
import { DeviceSummaryRepoPageModule } from '../pages/device-summary-repo/device-summary-repo.module';
// import { DistanceRepoPageModule } from '../pages/distance-repo/distance-repo.module';
import { DistanceReportPageModule } from '../pages/distance-report/distance-report.module';
// import { DriversRepPageModule } from '../pages/drivers-rep/drivers-rep.module';
import { FeedbackPageModule } from '../pages/feedback/feedback.module';
import { FuelReportPageModule } from '../pages/fuel-report/fuel-report.module';
import { GeofencePageModule } from '../pages/geofence/geofence.module';
import { GeofenceReportPageModule } from '../pages/geofence-report/geofence-report.module';
import { GroupsPageModule } from '../pages/groups/groups.module';
// import { IgnitionTabsPageModule } from '../pages/ignition-tabs/ignition-tabs.module';
import { IgnitionReportPageModule } from '../pages/ignition-report/ignition-report.module';
// import { IgnitionRepoPageModule } from '../pages/ignition-repo/ignition-repo.module';
// import { IdlingRepoPageModule } from '../pages/idling-repo/idling-repo.module';
// import { IdletimeReportPageModule } from '../pages/idletime-report/idletime-report.module';
import { HistoryDevicePageModule } from '../pages/history-device/history-device.module';

import { LivePageModule } from '../pages/live/live.module';

import { SignupPageModule } from '../pages/signup/signup.module';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { MobileVerify } from '../pages/login/mobile-verify';
import { SignupOtp } from '../pages/signup/signupOtp';
import { AddDevicesPageModule } from '../pages/add-devices/add-devices.module';
import { AddDeviceModalPage } from '../pages/customers/modals/add-device-modal';
import { GroupModalPage } from '../pages/customers/modals/group-modal/group-modal';
import { UpdateCustModalPage } from '../pages/customers/modals/update-cust/update-cust';
import { GeofenceShowPage } from '../pages/geofence/geofence-show/geofence-show';
import { AddGeofencePage } from '../pages/geofence/add-geofence/add-geofence';
import { MenuProvider } from '../providers/menu/menu';
import { RoutePageModule } from '../pages/route/route.module';
// import { CreateRoutePage } from '../pages/route/create-route/create-route';
import { EditRouteDetailsPage } from '../pages/route/edit-route-details/edit-route-details';
// import { SMS } from '@ionic-native/sms';

import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { PopoverPage } from '../pages/add-devices/add-devices';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { UpdateDevicePage } from '../pages/add-devices/update-device/update-device';
import { SplashPageModule } from '../pages/splash/splash.module';
import { Push } from '@ionic-native/push';
import { DatePicker } from '@ionic-native/date-picker';
import { GroupModel } from '../pages/groups/group-model/group-model';
import { AddCustomerModal } from '../pages/customers/modals/add-customer-modal/add-customer-modal';
import { UpdateGroup } from '../pages/groups/update-group/update-group';
import { FilterPage } from '../pages/all-notifications/filter/filter';
// Custom components
import {SideMenuContentComponent} from '../../shared/side-menu-content/side-menu-content.component';
import { LiveSingleDevice } from '../pages/live-single-device/live-single-device';
import {Geolocation} from '@ionic-native/geolocation';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { ServiceProviderPage, UpdatePasswordPage } from '../pages/profile/profile';
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';


// import { ComponentsModule } from '../components/components.module';
// import { ContentDrawerComponent } from '../components/content-drawer/content-drawer';

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    MobileVerify,
    SignupOtp,
    AddDeviceModalPage,
    GroupModalPage,
    UpdateCustModalPage,
    GeofenceShowPage,
    AddGeofencePage,
    // CreateRoutePage,
    EditRouteDetailsPage,
    PopoverPage,
    UpdateDevicePage,
    GroupModel,
    AddCustomerModal,
    UpdateGroup,
    FilterPage,
    LiveSingleDevice,
    ServiceProviderPage,
    UpdatePasswordPage
    // ContentDrawerComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: false
    }),
    // ComponentsModule,
    IonBottomDrawerModule,
    SplashPageModule,
    AboutUsPageModule,
    // AcReportPageModule,
    // AddCustomerPageModule,
    AddGeofencePageModule,
    AllNotificationsPageModule,
    ContactUsPageModule,
    // CreateRoutePageModule,
    CustomersPageModule,
    DailyReportPageModule,
    DashboardPageModule,
    DeviceSummaryRepoPageModule,
    // DistanceRepoPageModule,
    DistanceReportPageModule,
    // DriversRepPageModule,
    FeedbackPageModule,
    FuelReportPageModule,
    GeofencePageModule,
    GeofenceReportPageModule,
    GroupsPageModule,
    HistoryDevicePageModule,
    // IdletimeReportPageModule,
    // IdlingRepoPageModule,
    // IgnitionRepoPageModule,
    IgnitionReportPageModule,
    // IgnitionTabsPageModule,
    LivePageModule,
    LoginPageModule,
    // OdoPageModule,
    OverSpeedRepoPageModule,
    // RouteMapPageModule,
    // RouteMapShowPageModule,
    RouteVoilationsPageModule,
    ShowGeofencePageModule,
    // ShowMapNearmePageModule,
    // ShowRoutePageModule,
    // ShowTripPageModule,
    SpeedRepoPageModule,
    StoppagesRepoPageModule,
    SignupPageModule,
    SupportPageModule,
    // TripPageModule,
    TripReportPageModule,
    // TripReportShowPageModule,
    AddDevicesPageModule,
    SelectSearchableModule,
    RoutePageModule,
    ProfilePageModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MobileVerify,
    SignupOtp,
    AddDeviceModalPage,
    GroupModalPage,
    UpdateCustModalPage,
    GeofenceShowPage,
    AddGeofencePage,
    // CreateRoutePage,
    EditRouteDetailsPage,
    PopoverPage,
    UpdateDevicePage,
    GroupModel,
    AddCustomerModal,
    UpdateGroup,
    FilterPage,
    LiveSingleDevice,
    ServiceProviderPage,
    UpdatePasswordPage
    // ContentDrawerComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    MenuProvider,
    GoogleMaps, 
    Geolocation,
    Spherical,
    Push,
    DatePicker,
    AppVersion ,
    SocialSharing
    // SMS
  ]
})
export class AppModule { }
