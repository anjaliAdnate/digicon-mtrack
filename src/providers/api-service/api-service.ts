import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { LoadingController } from 'ionic-angular';

@Injectable()
export class ApiServiceProvider {
  private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
  link: string = "http://51.38.175.41/users/";
  loading: any;
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController) {
    console.log('Hello ApiServiceProvider Provider');
  }

  ////////////////// LOADING SERVICE /////////////////
  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }
  ////////////////// END LOADING SERVICE /////////////

  getDriverList(_id) {
    return this.http.get("http://51.38.175.41/driver/getDrivers?userid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  filterByDateCall(_id, skip: Number, limit: Number, dates) {
    // console.log("from date => "+ dates.fromDate.toISOString())
    // console.log("new date "+ new Date(dates.fromDate).toISOString())
    var from = new Date(dates.fromDate).toISOString();
    var to = new Date(dates.toDate).toISOString();
    return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  filterByType(_id, skip: Number, limit: Number, key) {
    return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&type=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getFilteredcall(_id, skip: Number, limit: Number, key) {
    return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getDataOnScroll(_id, skip: Number, limit: Number) {
    return this.http.get('http://51.38.175.41/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleListCall(_id, email) {
    return this.http.get('http://51.38.175.41/devices/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }

  trip_detailCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteDataCall(data) {
    return this.http.post("http://51.38.175.41/trackRoute", data, { headers: this.headers })
      .map(res => res.json());
  }

  gettrackRouteCall(_id, data) {
    return this.http.post('http://51.38.175.41/trackRoute/' + _id, data, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteCall(_id) {
    return this.http.delete('http://51.38.175.41/trackRoute/' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getRoutesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getStoppageApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("http://51.38.175.41/stoppage/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getIgiApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("http://51.38.175.41/notifs/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getOverSpeedApi(link) {
    // http://51.38.175.41/notifs/overSpeedReport?from_date=2018-09-30T20:01:26.055Z&to_date=2018-10-01T12:01:26.055Z&_u=5a7009c9031fc508983b458a
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getGeogenceReportApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("http://51.38.175.41/notifs/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getFuelApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("http://51.38.175.41/notifs/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceReportApi(starttime, endtime, _id, Ignitiondevice_id) {
    return this.http.get("http://51.38.175.41/summary/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
      .map(res => res.json());
  }

  getDailyReport(email, _id, from, to, skip: Number, limit: Number) {
    // http://51.38.175.41/gps/getGpsReport?email=sanjay.diwedi@gmail.com&id=5b31e85d2b8fc936ea1cbff9&from=Mon%20Oct%2001%202018%2000:00:00%20GMT+0530%20(India%20Standard%20Time)&to=Mon%20Oct%2001%202018%2016:55:56%20GMT+0530%20(India%20Standard%20Time)&s=10&l=10
    return this.http.get("http://51.38.175.41/gps/getGpsReport?email=" + email + '&id=' + _id + '&from=' + from + '&to=' + to + '&s=' + skip + '&l=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  contactusApi(contactdata) {
    return this.http.post(this.link + "contactous", contactdata, { headers: this.headers })
      .map(res => res.json());
  }

  getAllNotificationCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addgeofenceCall(data) {
    return this.http.post('http://51.38.175.41/geofencing/addgeofence', data, { headers: this.headers })
      .map(res => res);
  }

  getdevicegeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofencestatusCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGeoCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getallgeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  user_statusCall(data) {
    return this.http.post('http://51.38.175.41/users/user_status', data, { headers: this.headers })
      .map(res => res);
  }

  editUserDetailsCall(devicedetails) {
    return this.http.post('http://51.38.175.41/users/editUserDetails', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerVehiclesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addGroupCall(devicedetails) {
    return this.http.post('http://51.38.175.41/groups/addGroup', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleTypesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getAllUsersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceModelCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  groupsCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addDeviceCall(devicedetails) {
    return this.http.post('http://51.38.175.41/devices/addDevice', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getCustomersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofenceCall(_id) {
    return this.http.get("http://51.38.175.41/geofencing/getgeofence?uid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassApi(mobno) {
    return this.http.get(this.link + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassMobApi(Passwordset) {
    return this.http.get(this.link + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
      .map(res => res.json());
  }

  loginApi(userdata) {
    return this.http.post(this.link + "LoginWithOtp", userdata, { headers: this.headers })
      .map(res => res.json());
  }

  signupApi(usersignupdata) {
    return this.http.post(this.link + "signUp", usersignupdata, { headers: this.headers })
      .map(res => res.json());
  }

  dashboardcall(email, from, to, _id) {
    return this.http.get('http://51.38.175.41/gps/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  stoppedDevices(_id, email, off_ids) {
    return this.http.get('http://51.38.175.41/devices/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
      .map(res => res.json());
  }

  livedatacall(_id, email) {
    return this.http.get("http://51.38.175.41/devices/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
      .map(res => res.json());
  }

  getdevicesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  ignitionoffCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateCall(devicedetail) {
    return this.http.post("http://51.38.175.41/devices/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceSpeedCall(device_id, from, to) {
    return this.http.get('http://51.38.175.41/gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  stoppage_detail(_id, from, to, device_id) {
    return this.http.get('http://51.38.175.41/stoppage/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  gpsCall(device_id, from, to) {
    return this.http.get('http://51.38.175.41/gps?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  getcustToken(id) {
    return this.http.get("http://51.38.175.41/users/getCustumerDetail?uid=" + id)
      .map(res => res.json());
  }

  getSummaryReportApi(starttime, endtime, _id, device_id) {
    return this.http.get("http://51.38.175.41/summary?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  getallrouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getSpeedReport(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateInCall(devicedetail) {
    return this.http.post("http://51.38.175.41/devices/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  deleteDeviceCall(d_id) {
    return this.http.get("http://51.38.175.41/devices/deleteDevice?did=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }

  deviceShareCall(data) {
    return this.http.get("http://51.38.175.41/devices/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    return this.http.post("http://51.38.175.41/users/PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  pullnotifyCall(pushdata) {
    return this.http.post("http://51.38.175.41/users/PullNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }


  getGroupCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGroupCall(d_id) {
    return this.http.get("http://51.38.175.41/groups/deleteGroup?_id=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }



  addcustomerCall(devicedetails) {
    return this.http.post('http://51.38.175.41/users/signUp', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }


  getAllDealerCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  sendTokenCall(payLoad) {
    return this.http.post("https://www.oneqlik.in/share/propagate", payLoad, {headers: this.headers})
    .map(res => res.json());
    }
    
    shareLivetrackCall(data) {
    return this.http.post("https://www.oneqlik.in/share", data, {headers: this.headers})
    .map(res => res.json());
    }

}
